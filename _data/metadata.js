module.exports = {
	title: "Missing Number",
	url: "https://missingnumber.fanstuff.garden/",
	language: "fr",
	description: "Découvrez les secrets des premières version de Pokémon",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
