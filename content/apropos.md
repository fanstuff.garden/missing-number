---
layout: layouts/base.njk
eleventyNavigation:
  key: À propos
  parent: Accueil
  order: 0
---

# À propos de Missing Number

Missing Number est un fansite pokémon orienté 1G / 2G, visant à parler des différents aspects de cette époque. Il a pour but à terme de devenir une sorte de mini "guide" de ces jeux, offrant un peu tout ce qu'il y a a savoir aussi. Il est utilise le générateur de site Eleventy pour former des pages HTML simple, et cherche à être légé et rapide à charger.

Ce site à un concept et un sujet similaire au site [Blue Moon Falls](https://bluemoonfalls.com/), mais une approche un peu différente (et Missing Number est en Français là ou BMF est en anglais). Blue Moon Fall offre une approche permettant de découvrir plus les jeux Pokémon 1G/2G et notamment certaines des mécaniques de customisations et "comment obtenir le plus de fun des jeux", là ou Missing Number vise à montrer un tas de chose qui existaient à cette époque, et tout l'aspect "méta-jeu". Si un jour peut-être qu'on aura quelques guides stratégiques pour les pokémons, pour l'instant nous n'en avons pas.

Cependant, je vous conseille de visiter aussi Blue Moon Falls si vous lisez l'anglais, le site à une approche complémentaire très intéressante, et fournis plusieurs outils dont on reparlera dans les pages adéquates.

Nous avons aussi quelques informations en commun avec le site [PRAMA Initiative](https://www.prama-initiative.com/), un fanwiki de glitchologie Pokémon, mais ceux-ci sont bieeeen plus complet sur ce sujet (même s'ils n'ont pas encore de glitchdex).

## Pourquoi ce site ?

Le but de ce site est de fournir un petit site sur un sujet très spécifique (les pokémons des générations 1 et 2) regroupant des liens vers de nombreux autres endroits ou vous aurez plus d'informations. L'idée est d'être moins complet qu'un wiki, mais un peu plus abordable, et traitant de sujets plus divers (mais centré sur la thématique commune), n'hésitant pas à aller dans les domaines moins "officiels", voir moins encyclopédique.

Il se pose en complément des gros sites Pokémon, tout en étant un petit espace ou je peux parler de la partie qui me plaît de ces sujets. L'idée est aussi de fournir un feeling un peu de cette époque, en abordant de nombreux sujets qui revenaient dans la période (ou plus tard) autour de ces jeux.

## Pourquoi la 1G/2G ?

J'ai fait ce site parce que j'ai passé d'excellent moment sur ces générations, et que je voulais aider à les rendre plus accessible aux gens voulant s'y lancer maintenant. Par rapport aux pokémon moderne, les premières générations ont pas mal d'archaïsme, mais elles ont aussi un scope plus petit qui les rends plus accessible sur certains points. N'ayant pas des centaines et centaines de pokémon avec lesquels jongler, cela en fait des jeux plus "abordable" sur certains points, ayant un peu moins de complexité.

Cela ne veut pas dire qu'elles sont toujours facile, ou n'offrent pas de richesse, mais elles ont un caractère plus simple. Mais à côté, il y a tout un univers moins connus, fondée non pas sur les jeux en eux-même mais tout les discours et les phénomènes émergeant qui ont eut lieu.

D'autres "blocs" de générations pourraient être fait avec d'autres sites similaires, tel qu'un site 3-5G, un site 6G/7G, etc.