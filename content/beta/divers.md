---
layout: layouts/base.njk
eleventyNavigation:
  key: Elements divers
  parent: Éléments des bétas
  order: 100
---

# Elements divers

Plusieurs autres éléments intéressant sont contenu des beta, et peuvent être discuté ici. Cette page couvre aussi bien la generation 1 que la generation 2.

## Prof Chen (g1)

Le professeur chen devait être possible à affronter dans pokémon rouge et bleu. Il possède trois équipe de codée, une avec chaque starter, ce qui laisse supposer qu'il devait avoir le starter restant.

<div style="text-align:center;margin:auto;">

![Le prof Chen en noir et blanc](/img/betared/profchen.png)

</div>

L'équipe devait contenir un **Tauros (niv 66)**, un **Noadkoko (niv 67)**, un **Arcanin  (niv 68)**, un **Léviator (niv 70)** et **le starter restant (niv 69)**.

## Type Bird

Le type bird est un type inutilisé qui existe dans les données des deux premières générations. Il n'est pas un type glitch (juste un type qui n'a pas été utilisé, c'est juste qu'il est visible sur MissingNo.). C'est un type physique, et n'a pas de donnée de faiblesses/résistences.

Dans la liste des types interne, il est situé entre le type Roche et Insecte.