---
layout: layouts/base.njk
eleventyNavigation:
  key: Éléments des bétas
  order: 12
---

# Éléments des bétas

Les premiers jeux Pokémon ont eut de nombreux changements avant d'arriver aux projets finaux. En effet, le premier pokémon a été en développement pendant prêt de 6 ans, et le second a été entièrement refait par rapport à son concept original. 

Le but de ces pages est de présenter quelques uns des éléments inutilisés des bétas de Pokémon Rouge et Bleu, ainsi que Or et Argent, pour les curieux voulant en découvrir plus. 

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Crédits

Une grande partie de ces informations ont été reprise et traduite du TCRF, et ont pour but de fournir un point de vue "global". Pour plus d'informations, vous pouvez aller cliquer sur les liens vers les pages TCRF indiquée en crédits.