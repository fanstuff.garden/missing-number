---
layout: layouts/base.njk
eleventyNavigation:
  key: Pokémon (gen 1)
  parent: Éléments des bétas
  order: 1
---

# Pokémon Béta (Gen 1)

Pokémon Rouge et Bleu possède 39 pokémons qui ont été supprimés, résultant en les emplacement vident qu'on appelle "MissingNo" (initialement 40, mais mew a été rajouté après le retrait des outils de débugs). Quelques uns de ses pokémons ont été récupéré, et nous en apprennent sur le développement des premiers jeux de la série Pokémon.

Seuls les backsprites ont été retrouvés de ces pokémons, mais quelques fans ont développé des pokémons supplémentaires. De nombreux de ces pokémons ont été réimplémenté dans Kanto Extension Pack.

## Omega

Omega est le précédant pokémon ayant comme index l'index 21, qui sera réutilisé dans la verison finale pour Mew (les deux pokémon n'ont cependant rien a voir : l'id 21 était le premier libre).

<div style="text-align:center;">

[![Sprite de "Omega", un pokémon ressemblant à un mecha-kaiju](/img/betared/omega.png)](https://www.spriters-resource.com/custom_edited/pokemongeneration1customs/sheet/168064/)

*Vue d'artiste de Pokédieux, par [Freakyplanet](https://www.spriters-resource.com/submitter/Freakyplanet/)*

</div>

Presque aucune information n'est connue sur lui. Le concept d'un pokémon mecha-kaiju reviendra dans le Pokéwood dans Pokemon Blanc 2 / Noir 2.

## Lignée de Carapuce

La lignée de Carapuce est dans le jeu finale la fusion de deux lignées. En effet, deux pokémons qui devaient servir de sous évolution ou d'évolution à des pokémons de cette lignée ont été retrouvé, ce qui séparait la lignée de Carapuce de cette de Tortank.

| | |
|:--:|:-: |
| *Lignée de Carabaffe* <br/> ![](/img/betared/carapuce.png) ![](/img/betared/carabaffe.png) ![](/img/betared/181.png) | *Lignée de Tortank* <br/> ![](/img/betared/174.png) ![](/img/betared/tortank.png) |

Carabaffe devait évoluer au niveau 43 (les niveaux d'évolution dans le prototype de pokémon étant bien plus haut que dans la version finale)

## Troisième "buu"

Les "buu" sont un ensemble de pokémon kaiju-like contenant Magmar et Electek, et auquel est attaché Lipoutou comme étant un troisième membre de type classe (les trois finissant par "buu"). Cependant, un autre pokémon était prévu pour prendre cette place.

Une autre possibilité serait qu'il ai été une évolution de Lipoutou, ressemblant un peu à un lipoutou plus grand (ce qui peut poser la question de s'il en aurait eut d'autre ?).

|   |   |
|:-:|:-:|
| *Trio des buu* <br/> ![](/img/betared/magmar.png) ![](/img/betared/electek.png) ![](/img/betared/52.png) | *Comparaison à Lipoutou* <br/> ![](/img/betared/lipoutou.png) ![](/img/betared/52.png) |

## Sous-évolutions

Plusieurs sous-évolutions ont été considéré à partir du moment ou le concept d'évolution a été ajouté dans le jeu, mais ont été supprimé.

| | | |
|:--:|:-:|:-:|
| *pré-Nosferapti* <br/> ![](/img/betared/69.png) ![](/img/betared/nosferapti.png) ![](/img/betared/nosferalto.png) | *pré-Goupix* <br/> ![](/img/betared/81.png) ![](/img/betared/goupix.png) ![](/img/betared/feunard.png) | *pré-Miaouss* <br/> ![](/img/betared/134.png) ![](/img/betared/miaouss.png) ![](/img/betared/persian.png) |
| *pré-magnéton* <br/>  ![](/img/betared/140.png) ![](/img/betared/magneton.png) | *pré-poissirène*<br /> ![](/img/betared/156.png) ![](/img/betared/poissirène.png) ![](/img/betared/poissoroi.png) | *pré-ponita*<br /> ![](/img/betared/162.png) ![](/img/betared/ponita.png) ![](/img/betared/galopa.png) |

Le pré-magnéton sera remplacé par magnéti, et les pré-goupix, miaouss, poissirène et ponyta seront retenté dans Or et Argent, mais supprimé de nouveau.

Le pré-miaouss ressemble plus à ce que serait une évolution du milieu entre miaouss et persian, mais est décrit généralement comme une pré-évolution par les sites de datamining.

## Évolutions

| | | |
|:--:|:-:|:-:|
| *evo psykokwak* <br/> ![](/img/betared/psykokwak.png) ![](/img/betared/127.png) ![](/img/betared/akwakwak.png) | *evo Ossatueur* <br/> ![](/img/betared/osselait.png) ![](/img/betared/ossatueur.png) ![](/img/betared/146.png) | *Gorochu (evo Raichu)*<br /> ![](/img/betared/pikachu.png) ![](/img/betared/raichu.png) ![](/img/betared/175.png) |


## Lignées perdues

| | | |
|:--:|:-:|:-:|
| *"Balloonda"* <br/> ![](/img/betared/50.png) | *"Deer"* <br/> ![](/img/betared/56.png) | *Pokémon éléphant* <br/> ![](/img/betared/61.png) | 
| *"Crocky"* <br/> ![](/img/betared/62.png) | *Pokémon poulpes* <br/> ![](/img/betared/63.png) ![](/img/betared/112.png) | *"Jagg"* <br/> ![](/img/betared/67.png) |
| *Pokémon poisson* <br/> ![](/img/betared/79.png) ![](/img/betared/80.png) | *Pokémon tigre* <br/> ![](/img/betared/159.png) ![](/img/betared/160.png) ![](/img/betared/161.png) | *Pokémon 86/87* <br/> ![](/img/betared/86.png) ![](/img/betared/87.png) |
| *Pokémon poisson* <br/> ![](/img/betared/79.png) ![](/img/betared/80.png) | *Pokémon dragon* <br/> ![](/img/betared/172.png) ![](/img/betared/94.png) ![](/img/betared/95.png) |  |

Certains de ces pokémons (le pokémon poulpe, le pokémon tigre et "Jagg") seront retenté dans Pokémon Or et Argent, mais supprimé à nouveau. Jagg est potentiellement un "ancêtre" d'une certaine manière de Sharpedo.

## Sources et crédits

- Infos sur les pokémons : [TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Red_and_Blue/Pok%C3%A9mon)
- Données du pokedex : [TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Red_and_Blue/Pok%C3%A9mon_Data)
- Sprites de fronts [par Freaky Planet](https://www.spriters-resource.com/custom_edited/pokemongeneration1customs/sheet/168064/)