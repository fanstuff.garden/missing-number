---
layout: layouts/pokelist.njk
---

# Sprites supplémentaires

En plus des pokémons apparus présent dans les données du jeu de la version beta du Spaceworld 97, d'autres sont présent dans un backup datant de 98 (ainsi qu'un autre de 99) en tant que graphismes en cours de création. Le but de cette page est de les analyse et les étudier un peu.

Le "numéro" indiqué n'est pas le numéro du pokedex, mais l'identifiant du sprite. Cette liste ne contient que les pokémon ayant une différence par rapport à la version finale et au spaceworld 97 et/ou étant absent de ces versions, et donc pas la liste entière de sprites.

<div class="pokedex spritedex">

| Numero | Sprites | Notes |
|:------:|:---------:|:-------:|
| 300 | ![Un pokémon façon Kokopeli](/img/betagold/backup98/300.png) | Certainement une première version de Célébi, qui ressemblait à cette époque plus à un Kokopeli | 
| 301 | ![Un pokémon anguille](/img/betagold/backup98/301.png) | Un pokémon anguille, certainement de type eau | 
| 304 | ![Un pokémon renard avec une queue de feu](/img/betagold/backup98/304.png) | Un pokémon renard de feu | 
| 305 | ![Un pokémon bouboule](/img/betagold/backup98/305.png) | Un pokémon bouboule | 
| 306 | ![Un pokemon lapin-plante](/img/betagold/backup98/306.png) | Un pokémon lapin, dont les oreilles ressemble à des feuilles. Certainement une évolution du 305 | 
| 307 | ![Un pokémon avec un feuillage comme "cheveux"](/img/betagold/backup98/307.png) | Une évolution du 306, ou les oreilles deviennent un feuillage complet | 
| 308 | ![Un pokémon arbre](/img/betagold/backup98/308.png) | Une autre évolution du 306, qui devient une sorte d'arbre | 
| 309 | ![Un pokémon éléphant](/img/betagold/backup98/309.png) | Une éléphant ressemblant un peu à un pokémon abandonné de Rouge/Bleu index 61 | 
| 311 | ![Un pokémon paon ressemblant aussi un peu à Natu/Xatu](/img/betagold/backup98/311.png) | Une mid-évolution de Natu/Xatu. | 
| 313 | ![Un pokémon kiwi](/img/betagold/backup98/313.png) | Un pokémon kiwi. | 
| 314 | ![Un pokémon scorpion](/img/betagold/backup98/314.png) | Un pokémon scorpion, différent de scorplane. | 
| 315 | ![Un pokémon oiseau-oeuf](/img/betagold/backup98/315.png) | Un pokémon oiseau ayant un peu une apparence d'oeuf. | 
| 316 | ![Un oiseau clef de sol](/img/betagold/backup98/316.png) | Un oiseau en forme de clef de sol, sans doute faisant pas mal d'attaques basé sur le son. |
| 317 | ![Un pokémon poisson à piquant](/img/betagold/backup98/317.png) | Très certainement une version alternative de l'évolution de Qwilfish. | 
| 319 | ![Un pokémon sanglier avec des bois](/img/betagold/backup98/319.png) | Un sanglier ayant des bois de cerfs, sans doute pour représenter un pokémon de foret. Sans doute prévu comme de type normal. | 
| 325 | ![Un chien un peu cartoon](/img/betagold/backup98/325.png) | Sans doute une version alternative de la pré-évolution de Caninos. | 
| 331 | ![Un spectre](/img/betagold/backup98/331.png) | Un pokémon spectre basé sur la représentation des ames qui quittent le corps au japon (également utilisées à fin humoristique dans les manga et l'animation japonaise). | 
| 344 | ![Un dino-drakkar](/img/betagold/backup98/344.png) | Un pokémon dinosaur et drakkar à la fois. Certainement un type eau. | 
| 349 | ![Un gros chien](/img/betagold/backup98/349.png) | Une sorte d'immense chien. | 
| 350 | ![Un lapin cartoon](/img/betagold/backup98/350.png) | Un drole de lapin étrange | 
| 351 | ![Un petit serpent](/img/betagold/backup98/351.png) | Une sorte de petit serpent, sans doute de type sol ? | 
| 352 | ![Un oiseau épouventail](/img/betagold/backup98/352.png) | Un oiseau avec un chapeau, ressemblant un peu à un épouventail | 
| 353 | ![Une gargouille](/img/betagold/backup98/353.png) | Un pokémon gargouille, surment de type roche-vol comme ptera | 
| 355 | ![Un poisson](/img/betagold/backup98/355.png) | Un pokémon poisson, évolution originale de Mambo | 
| 356 | ![Un poisson](/img/betagold/backup98/356.png) | Un pokémon poisson, évolution finale originale de Mambo | 
| 360 | ![Un écureil ninja](/img/betagold/backup98/360.png) | Un écureuil avec de trait de ninja. Sans doute de type ténèbre, combat ou normal. | 
| 364 | ![Un hérisson avec des pics "solide"](/img/betagold/backup98/356.png) | Une version originelle d'héricendre, ou à la place des flammes ce sont des piquants solides. Sans doute de type glace, sol, roche ou acier. | 
| 377 | ![Un renard "longiligne"](/img/betagold/backup98/377.png) | Une sorte de renard très longiligne, à potentiellement été remanié en fouinard. | 
| 378 | ![Une cigogne avec un bébé oiseau dans le bec](/img/betagold/backup98/378.png) | Une cigogne portant un bébé oiseau. | 
| 380 | ![Une poulpe](/img/betagold/backup98/380.png) | Un pouple, sans doute basé sur la lignée de pokémon pouple prévu initialement dans la G1. | 
| 382 | ![Une ver de sac](/img/betagold/backup98/382.png) | Un pokemon ver de sac, faisant penser à ce que sera plus tard Cheniti dans la G4. | 
| 383 | ![Une mite](/img/betagold/backup98/383.png) | Un pokémon mite, évolution du précédant. | 
| 386 | ![Un koala](/img/betagold/backup98/386.png) | Un koala sur une branche. La présence de la branche laisse penser qu'il serait plante, et l'aspect mamifère peut le faire être normal. | 
| 387 | ![Un tanuki](/img/betagold/backup98/387.png) | Un tanuki avec un sac à dos en feu. Sans doute de type feu, du coup. | 
| 392 | ![Un oiseau avec un bec étrange](/img/betagold/backup98/392.png) | Un oiseau avec un bec en forme de trompette, sans doute lié au même thème que l'oiseau en clef de sol (316). | 
| 397 | ![Une grenouille avec une grande langue](/img/betagold/backup98/397.png) | Une grenouille avec une grande langue. Certainement de type eau. Un lien potentiel avec tarpau ? (potentiel fusionne de lui et sa version originel pour donner celui qu'on connait ?) | 
| 400 | ![Un hypopothame-putois](/img/betagold/backup98/400.png) | Une sorte d'hybride entre un hypopothame et un putois. Potentiellement poison avec le motif du putois ? | 
| 401 | ![Un squelette](/img/betagold/backup98/401.png) | Une sorte de squelette dinosauresque. Un fossile revenu à la vie ? Sans doute de type spectre, et peut-être avec un style sol (irait avec osselait et co), ou ténèbre |
| 402 | ![Un lapin a grosse oreille ronde](/img/betagold/backup98/402.png) | Une sorte de lapin ou souris à grosse oreille ronde. |
| 403 | ![Un gros moustique](/img/betagold/backup98/403.png) | Une pokémon moustique. Certainement insecte/vol ou insecte/poison. |
| 404 | ![Une plante pas commode](/img/betagold/backup98/404.png) | Une plante pas commode. |
| 405 | ![Un insecte volant](/img/betagold/backup98/405.png) | Un insect volant. |
| 406 | ![Un dinosaure avec un oeuf sur la tête](/img/betagold/backup98/406.png) | Un dinosaure avec un oeuf sur la tête, sans doute inspiration de Cranidos durant la 4G. Était sans doute roche et/ou sol. |
| 407 | ![Une fleur de cerisier](/img/betagold/backup98/407.png) | Une fleur de cerisier. |
| 412 | ![Un pokemon serpentesque](/img/betagold/backup98/412.png) | Un pokémon un peu serpentesque, sans doute une version initiale d'Insolourdo. |
| 415 | ![Un dinosaure en armure](/img/betagold/backup98/415.png) | Un autre dinosaure, cette fois avec un côté armure. Peut-être roche/sol et eau ? |
| 416 | ![Un poisson-oiseau](/img/betagold/backup98/416.png) | Un poisson-oiseau, certainement de type eau-vol. |

</div>

Vous pouvez trouver [ici](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Sprites/980506) la liste complete des sprites (avec ceux comportant des designs se répétant avec les autres beta et la version finale).


## Design beta 99

La beta de 99 était quasiment finalisée en terme de pokémon, mais comportait quelques designs différents que nous allons regarder ici. A noter que cette liste ne concerne que les designs différents, pas les différences de sprite.

<div class="pokedex spritedex">

| Pokemon | Sprites | Notes |
|:------:|:---------:|:-------:|
| Pichu | ![Un pichu plus blobesque](/img/betagold/beta99/172.png) | A un design plus blob-esque, plus pikachu-esque |
| Axoloto | ![Un axoloto quadrupède](/img/betagold/beta99/195.png) | A un design quadrupède |
| Caratroc | ![Un caratroc plus fin](/img/betagold/beta99/205.png) | A un design plus "fin" pour ses tentacules, et un pot plus petits. |
| Insolourdo | ![Un insolourdo plat](/img/betagold/beta99/206.png) | A un design plus plat et serpentesque (plus simple également). |
| Limagma | ![Un limagma plus limace](/img/betagold/beta99/222.png) | A un design plus de "limace" et a moins de bulles. À un côté plus slime que "magma". |
| Celebi | ![Un celebi plus kokopeli](/img/betagold/beta99/251.png) | A un design plus kokopeli et utilise la palette marron. |

</div>

Vous pouvez trouver [ici](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Sprites/980506) la liste complete des pokémon.

## Divers

Quelques autres sprites ont eut des évolutions intéressantes :

<div class="pokedex spritedex">


| Pokemon | Sprites | Notes |
|:------:|:---------:|:-------:|
| Tsuinzu | ![Deux spectre tournant l'un autour de l'autre](/img/betagold/202-2.png) | Tsuinzu, la sous-évolution de Girafarig, avec un style plus défini, ressemblant à deux spectrum. |

</div>
