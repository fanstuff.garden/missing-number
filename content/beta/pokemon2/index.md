---
layout: layouts/pokelist.njk
eleventyNavigation:
  key: Pokémon (gen 2)
  parent: Éléments des bétas
  order: 2
---

# Pokémon Béta (Gen 2)

La béta de pokémon Or et Argent contenait un pokedex relativement différents, et il a subit de nombreuses évolutions jusqu'à obtenir la version finale qu'on connait. Certains pokémon ont changé d'apparence, d'autres ont été complètement retiré, et certains sont apparu sous d'autres forme dans des générations suivantes.

Cette page se penche sur les pokémon présent dans la démo du Spaceworld 97 de Pokémon Or et Argent.

## Pokédex beta 97

<table class="pokedex">
  <thead>
    <tr>
      <th>Dex</th>
      <th>Nom</th>
      <th>Types</th>
      <th>PV</th>
      <th>Atk</th>
      <th>Def</th>
      <th>Spd</th>
      <th>Spe.Atk</th>
      <th>Spe.Def</th>
    </tr>
  </thead>

  <tbody>
  {%- for pokemon in pokegoldspaceworld -%}
    <tr>
      <td>{{ pokemon.baseStats.dex }} </td>
      <td> <a href="/beta/pokemon2/{{ pokemon.namefr | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ pokemon.namefr }}</a> </td>
      <td> {{ pokemon.baseStats.types[0] }}{%- if pokemon.baseStats.types[0] !== pokemon.baseStats.types[1] -%}, {{ pokemon.baseStats.types[1] }} {%- endif -%} </td>
      <td>{{ pokemon.baseStats.stats.hp }}</td>
      <td>{{ pokemon.baseStats.stats.atk }}</td>
      <td>{{ pokemon.baseStats.stats.def }}</td>
      <td>{{ pokemon.baseStats.stats.spd }}</td>
      <td>{{ pokemon.baseStats.stats.speatk }}</td>
      <td>{{ pokemon.baseStats.stats.spedef }}</td>
    </tr>
  {%- endfor -%}
  <tbody>
</table>

### Analyse 

Une analyse de ce pokédex et de ses pokémons est présente sur [une page dédiée](/beta/pokemon2/analyse/).

## Autres sprites

En plus des pokémons apparus présent dans les données du jeu de la version beta du Spaceworld 97, d'autres sont présent dans un backup datant de 98 (ainsi qu'un autre de 99) en tant que graphismes en cours de création. Vous pouvez en voir plus sur [une page dédiée](/beta/pokemon2/sprites/).

## Crédits

- Les informations sur les pokémons viennent de la décompilation de la démo du spaceworld ainsi que de [ces page de TCRF](https://tcrf.net/Proto:Pok%C3%A9mon_Gold_and_Silver/)
- La liste des sprite de la démo est [en partie traduite de cet article de TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Sprites/980506)
- Les données du pokedex viennent du désassemblement de la rom de pokémon beta