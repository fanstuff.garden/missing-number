---
pagination:
    data: pokegoldspaceworld
    size: 1
    alias: pokemon
permalink: "beta/pokemon2/{{ pokemon.namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}/"
layout: layouts/pokedex2b.njk
---

<div style="display:flex;justify-content:space-between;">
  <div>
    {%- if pokemon.baseStats.dex > 1 -%}<a href="/beta/pokemon2/{{ pokegoldspaceworld[pokemon.baseStats.dex - 2].namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}">← {{ pokegoldspaceworld[pokemon.baseStats.dex - 2].namefr }}</a> {%- endif -%}
  </div>

  <div>
    {%- if pokemon.baseStats.dex < 251 -%}<a href="/beta/pokemon2/{{ pokegoldspaceworld[pokemon.baseStats.dex].namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}">{{ pokegoldspaceworld[pokemon.baseStats.dex].namefr }} →</a> {%- endif -%} 
  </div>
</div>

# {{ pokemon.namefr }} 

{{ pokemon.namefr }}** ( {{ pokemon.nameint }}* en anglais ; {{ pokemon.name }} en japonais ) est le pokémon n°{{ pokemon.baseStats.dex }} du pokedex de de la beta spaceworld de Pokémon Or & Argent, de type {{ pokemon.baseStats.types[0] }}{% if pokemon.baseStats.types[0] !== pokemon.baseStats.types[1] %} et {{ pokemon.baseStats.types[1] }} {%- endif -%}.

( Note : les donnée étant récupéré d'une version béta, elle ne reflette sans doute pas ce qu'auraient été les données finales et sont souvent incomplète / temporaires. )

## Généralité

### Statistiques

|   | PV | ATK | DEF | SPD | SPE.ATK | SPE.DEF | 
|:-:|:--:|:---:|:---:|:---:|:-------:|:-------:|
| **Base :** | {{ pokemon.baseStats.stats.hp }} |  {{ pokemon.baseStats.stats.atk }} |  {{ pokemon.baseStats.stats.def }} |  {{ pokemon.baseStats.stats.spd }} |  {{ pokemon.baseStats.stats.speatk }} | {{ pokemon.baseStats.stats.spedef }} |

### Évolutions

<table>
  <thead>
    <tr>
      <th>Évolue en</th>
      <th>via</th>
    </tr>
  </thead>

  <tbody>
    {%- for evolution in pokemon.evoMoves.evolutionItems -%}
        <tr>
            <td><a href="/beta/pokemon2/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td>Avec un(e) {{ evolution.item }} </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionLevel -%}
        <tr>
            <td><a href="/beta/pokemon2/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Au niveau {{ evolution.level }} </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionTrade -%}
        <tr>
            <td><a href="/beta/pokemon2/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Par échange {% if evolution.item !== "-1" -%} ( Avec un(e) {{ evolution.item }} ) {%- endif -%} </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionHappiness -%}
        <tr>
            <td><a href="/beta/pokemon2/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Par bohneur ( {{ evolution.time }} ) </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionStat -%}
        <tr>
            <td><a href="/beta/pokemon2/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Au niveau {{ evolution.niveau }} ( {{ evolution.stat }} ) </td>
        </tr>
    {%- endfor -%}
    {%- if (pokemon.evoMoves.nbrEvolution <= 0 ) -%}
        <tr>
            <td colspan="2"> <em>Stade Final</em> </td>
        </tr>
    {%- endif -%}
  </tbody>
</table>

## Emplacements

<table class="pokedex">
  <thead>
    <tr>
      <th></th>
      <th>Emplacement</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <th>Or </th>
      <td>
        {%- for encounterList in pokemon.encounters.gold -%}
          {% if encounterList.prefix !== "" %}<strong>{{ encounterList.prefix }}</strong> : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
    <tr>
      <th>Argent </th>
      <td>
        {%- for encounterList in pokemon.encounters.silver -%}
          {% if encounterList.prefix !== "" %}<strong>{{ encounterList.prefix }}</strong> : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
    <tr>
      <th>Cristal </th>
      <td>
        {%- for encounterList in pokemon.encounters.crystal -%}
          {% if encounterList.prefix !== "" %}<strong>{{ encounterList.prefix }}</strong> : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
  </tbody>
</table>

## Attaques

<div class="grid" style="display:flex;">
    <div>

    <table>
        <thead>
            <tr>
            <th>Niv</th>
            <th>Attaque</th>
            </tr>
        </thead>

        <tbody>
            {%- for move in pokemon.evoMoves.moves -%}
                <tr>
                    <td> {{ move.level }} </td>
                    <td>{{ move.move }} </td>
                </tr>
            {%- endfor -%}
            {%- if pokemon.evoMoves.nbrMoves == 0 -%}
                <tr>
                    <td colspan="2"> <em>Aucune attaque apprise par niveau</em> </td>
                </tr>
            {%- endif -%}
        </tbody>
    </table>

    *(Note : certaines attaques peuvent se retrouver en double dans les tableaux.)*

    </div>
    <div>

    <table>
        <thead>
            <tr>
            <th>Par CT / CS</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td> <em>Données faisant peu sens dans la béta</em> </td>
            </tr>
        </tbody>
    </table>
    </div>
</div>



\* Le nom EN générlaement est une tranlittération du nom japonais pour les nouveau pokémon (à partir de 152)

\*\* Le nom FR est soit repris de l'équivalent final du pokémon, soit adapté de la translittération du japonais.