---
layout: layouts/base.njk
---

# Analyse pokedex beta 97

Les pokémons de la béta 97 sont en partie différent de ceux présent dans la version finale de Pokémon Or et Argent. Le but de cette page est de faire une analyse un peu plus "haut niveau" des lignées d'évolution.

Il est à noter que cette page ne respecte pas leur ordre dans le pokedex et les ranges par catégorie, afin de pouvoir les décrire plus efficacement. De plus, elle ne comporte pas ceux dont on a seulement les sprites.

[Retour au pokedex](/beta/pokemon2/)

## Les starters

Deux des lignées de starters sont complètement différentes dans la béta : celles de héricendre et kaiminus. Ces lignées étaient celle à l'époque de **Kurusu** et **Honooguma**. Ils sont tous monotypes.

|   |   |   |
|:-:|:-:|:-:|
| *Lignée de Germignon* <br/> ![](/img/betagold/152.png) ![](/img/betagold/153.png) ![](/img/betagold/154.png) | *Lignée de Honooguma* <br/> ![](/img/betagold/155.png) ![](/img/betagold/156.png) ![](/img/betagold/157.png) | *Lignée de Kurusu* <br/> ![](/img/betagold/158.png) ![](/img/betagold/159.png) ![](/img/betagold/160.png) |

La lignée de **Germignon** possède une majeure différence : l'équivalement de Macronium à une apparence différente, semblant être une créature qui bourgeonne. Il est difficile de savoir si c'est que la lignée était en train d'être redesignée, ou que c'était l'apparence prévue de la seconde évolution. Il fut décidé dans la final en tout cas d'avoir un Macronium plus "entre" les deux autres évolutions. La lignée de **Honooguma** est une lignée de starter feu basé sur les ours. La lignée de **Kurusu** est une lignée de starter eau basé sur les dinosaures, plus précisément les plesiosaurus. Ils peuvent faire penser aussi au monstre du loch ness. 

## Lignées conservés

Les nouveaux pokémon suivants ont été conservés sous une forme proche ou redesignées, avec parfois quelques changements de types, de niveaux, etc.

|   |   |   |
|:-:|:-:|:-:|
| *Lignée de Hoot-hoot* <br/> ![](/img/betagold/161.png) ![](/img/betagold/162.png) | *Lignée de Watouat* <br/> ![](/img/betagold/163.png) ![](/img/betagold/164.png) ![](/img/betagold/165.png) | *Raimanta* <br/> ![](/img/betagold/169.png) |
|  *Lignée de Natu* <br/> ![](/img/betagold/176.png) ![](/img/betagold/177.png)  |  *Lignée de Mimigale* <br/> ![](/img/betagold/185.png) ![](/img/betagold/186.png)  |  *Airmure* <br/> ![](/img/betagold/187.png)  |
|  *Lignée de Phampi* <br/> ![](/img/betagold/191.png) ![](/img/betagold/192.png)  | *Queulorior* <br /> ![](/img/betagold/195.png) | *Lignée de Coxy* <br/> ![](/img/betagold/202.png) ![](/img/betagold/203.png) |
|  *Zarbi* <br/> ![](/img/betagold/201.png) | *Lignée de Rémoraid* <br/> ![](/img/betagold/209.png) ![](/img/betagold/210.png) | *Lignée de Débugant* <br/> ![](/img/betagold/211.png) ![](/img/betagold/212.png) |
|  *Lignée de Granivol* <br/> ![](/img/betagold/214.png) ![](/img/betagold/215.png)  ![](/img/betagold/216.png) | *Ecremeuh* <br /> ![](/img/betagold/223.png) | *Kadwaso* <br /> ![](/img/betagold/225.png) |
|  *Cornèbre* <br/> ![](/img/betagold/231.png) | *Lignée de Malosse* <br/> ![](/img/betagold/235.png) ![](/img/betagold/236.png) | *Farfuret* <br/> ![](/img/betagold/246.png) |
|  *Capumain* <br/> ![](/img/betagold/250.png) |  |  |

- La lignée de **Hoothoot** était de type vol pur. 

## Sous-évolutions de G1

La version béta de Or et Argent avait de nombreux bébé pokémon en plus. Ils ont sans doute été en partie supprimés pour avoir plus de pokémons différents.

|   |   |   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| *pré-Goupix* <br /> ![](/img/betagold/166.png) | *pré-Saquedeneu* <br /> ![](/img/betagold/167.png) | *pré-Poissirène* <br /> ![](/img/betagold/178.png) | *pré-Paras* <br /> ![](/img/betagold/184.png) | *pré-Doduo* <br /> ![](/img/betagold/189.png) | *pré-Miaouss* <br /> ![](/img/betagold/196.png) | *pré-Ponyta* <br/> ![](/img/betagold/204.png) |
| *pré-Tadmorv* <br /> ![](/img/betagold/208.png) | *pré-Caninos* <br /> ![](/img/betagold/213.png) | *pré-Mr. Mime* <br /> ![](/img/betagold/217.png) |  |  |  |  |
| *Pichu* <br /> ![](/img/betagold/172.png) | *Melopti* <br /> ![](/img/betagold/173.png) | *Toudoudou* <br /> ![](/img/betagold/174.png) | *Lipouti* <br /> ![](/img/betagold/218.png) | *Elekid* <br /> ![](/img/betagold/219.png) | *Magbi* <br /> ![](/img/betagold/220.png)  |   |

Certaines de ces sous-évolutions (celle de Poissirène, Goupix, Ponita, Miaouss) étaient originellement créé pour la génération 1.

La pré-évolution de Mr. Mime existera sous la forme de Mime Jr dans la génération IV.

## Évolutions de G1

|   |   |   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| *Saquedeneu* <br /> ![](/img/betagold/168.png) | *Metamorph* <br /> ![](/img/betagold/188.png) | *Boustiflor* <br /> ![](/img/betagold/222.png) | *Madame* <br /> ![](/img/betagold/228.png) | *Plucks* <br /> ![](/img/betagold/234.png) | *Nameeru* <br /> ![](/img/betagold/240.png) | *Evoli* <br/> ![](/img/betagold/251.png) |
| *Nostenfer* <br /> ![](/img/betagold/183.png) | *Tarpeau* <br /> ![](/img/betagold/199.png) | *Roigada* <br /> ![](/img/betagold/200.png) | *Mentali* <br /> ![](/img/betagold/205.png) | *Noctali* <br /> ![](/img/betagold/206.png) | *Joliflor* <br /> ![](/img/betagold/221.png) |  *Leuphorie* <br /> ![](/img/betagold/232.png)  |
| *Cizayox* <br /> ![](/img/betagold/233.png) | *Porygon2* <br /> ![](/img/betagold/239.png) | *Steelix* <br /> ![](/img/betagold/241.png) | *Hyproi* <br /> ![](/img/betagold/242.png) |   |   |   |

Noctali était de type Poison à l'époque, et utilisait une pierre poison pour évoluer.

Les concepts d'évolutions suivantes seront réutilisé dans la génération IV : L'évolution de Saquedeneu (Bouledeneu), l'évolution de Excelangue (Couedelangue), l'évolition de type plante (Philally, mais qui utilisera une pierre plante).

Madame, l'évolution de Canarticho, aura un "équivalent" lors de la génération 8, avec Palarticho, mais celui-ci n'a rien a voir.

## Lignées modifiées

Sept lignées ont été modifiée entre la version de béta datant de 97 et la version finale.

### Lignée ayant perdu un membre

Deux lignées ont perdu un membre, formant à la place des pokémons sans évolution : la lignée de Qwilfish, et celle de Girafarig.

|   |   |
|:-:|:-:|
| *Lignée de Qwilfish* <br/> ![](/img/betagold/170.png) ![](/img/betagold/171.png) | *Lignée de Girafarig* <br/> ![](/img/betagold/193.png) ![](/img/betagold/194.png)  |

La lignée de Girafarig est un cas complexe et intéressant. Si Twinzu, sa sous-évolution n'est en effet plus sa sous-évolution, il est généralement admis qu'elle a servis de base à Qulbutoke, qui gardera même son icone "fantôme" dans le jeu final.

### Lignée ayant gagné un membre

Cinq pokemon prévu à la base comme des pokémon sans évolutions vont gagner une évolution ou une sous-évolutions.

|   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|
| *Maraiste* <br /> ![](/img/betagold/175.png) | *Marill* <br /> ![](/img/betagold/179.png) | *Heliatronc* <br /> ![](/img/betagold/190.png) | *Togepi* <br /> ![](/img/betagold/248.png) | *Snubull* <br /> ![](/img/betagold/249.png) |

- **Marill**, **Togepi** et **Snubull** gagneront des évolutions dans la version finale.
- **Maraiste** et **Heliatronc** gagneront des sous-évolutions dans la version finale.

## Lignées perdues

Huit lignées de pokémon seront remplacée entre la version béta de 97 et la version finale.

|   |   |   |
|:-:|:-:|:-:|
| *Manboo1 & Ikari* <br/> ![](/img/betagold/180.png) ![](/img/betagold/181.png) | *Gurotesu* <br/> ![](/img/betagold/182.png)  | *Rinrin & Berurun* <br/> ![](/img/betagold/197.png) ![](/img/betagold/198.png) |
| *Turban* <br/> ![](/img/betagold/207.png) | *Bomushikaa* <br /> ![](/img/betagold/224.png) | *Kotora & Raitora* <br /> ![](/img/betagold/226.png) ![](/img/betagold/227.png) |
| *Norowara & Kyonpan* <br/> ![](/img/betagold/229.png) ![](/img/betagold/230.png) | *Wolfman & Warwolf* <br/> ![](/img/betagold/237.png) ![](/img/betagold/238.png) | |

Ikari ressemble pas mal à Sharpedo (Generation III), tandis que Kotora et Raitora étaient originellement créé pour la génération 1.

Bomushikaa est assez unique avec son type eau/feu (concept qui n'existera pas avant la Generation VI).

## Pokémon légendaires

Il n'y avait que quatre pokémon légendaires prévu à l'origine dans le jeu : les trois bêtes légendaires, ainsi que Ho-oh.

|   |   |
|:-:|:-:|
| *Bêtes légendaires* <br/> ![](/img/betagold/243.png) ![](/img/betagold/244.png) ![](/img/betagold/245.png) | *Ho-oh* <br/> ![](/img/betagold/247.png)  |

Les trois bêtes légendaires seront complètement redesignées pour la version finale, gagnant des aspects plus "majestueux" comparé à ceux plus "jeunes" qu'ils avaient à l'origine.