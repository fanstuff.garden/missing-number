---
layout: layouts/base.njk
eleventyNavigation:
  key: Région de Kanto
  parent: Éléments des bétas
  order: 10
---

# La région de Kanto (generation 1)

La région de Kanto à eut quelques changements avant sa version finale. Le but de cette page n'est pas d'explorer toutes les apparences qu'elle a eut (ni de montrer toutes les version inachevées des cartes), mais de montrer quelques informations sur l'état originelle de Kanto

## Style originel

Les premières map de Pokémon ont une apparence très différentes, bien plus rudimentaires. On peut voir cela parmis ces examples de cartes

|   |   |
|:-:|:-:|
| *Bourg Palette* <br/> ![](/img/betared/maps/bourgpalette.png) | *Jadielle* <br/> ![](/img/betared/maps/jadielle.png)  |
| *Argenta* <br/> ![](/img/betared/maps/argenta.png) | *Lavanville* <br/> ![](/img/betared/maps/lavanville.png)  |


Parmis les différences globales que l'on peut constater :
- Toutes les villes ont un aspect plus simple, avec moins de bâtiments
- Les bâtiments prennent plus de place, et sont composé de tuiles de deux types de tuiles de toit prenant une majeure partie de la place, et une ligne de tuiles de "mur" (avec des portes et tout).
- Les falaise ont un style plus "en vue 3/4" n'ayant qu'une variation de mur et plate. Les entrées des grottes sont plus grande
- Des hautes herbes sont présentes dans les villes. Elles ont un style aussi plus proche de celui qu'elles auront à partir de la génération 3 (des sortes de petits buissons)
- Certaines routes ont un style plus de "route"

## La "Ville C"

La "Ville C" est une ville supplémentaire prévue dans les documents de design de Capsule Monster, qui devait se trouver en dessous de Celadopole.

<div style="text-align:center;width:50%;margin:auto;">

![Carte originelle de Kanto, proche de la finale mais avec une ville en plus](/img/betared/maps/map.png)

</div>

Une map pouvant correspondre à la ville a été trouvée dans les sources de Capsule Monster, qui est cependant bien plus petite que ce que la map laisse supposée, et plus "connectée", là ou la ville C semble être une île isolée, accessible que via un moyen autre qu'une route (L'océane ? Une grotte à terme ?)

<div style="text-align:center;width:50%;margin:auto;">

![Une ville composée de 3 bâtiments](/img/betared/maps/c.png)

</div>

On peut noter la présence d'un élément caché de la version finale : le fameux camion où la rumeur disait qu'on pouvait trouver mew.

<div style="text-align:center;width:50%;margin:auto;">

[![Une ville composée de 3 bâtiments, avec un port en bas de la map](/img/betared/maps/c-remake.png)](https://www.spriters-resource.com/custom_edited/pokemongeneration1customs/sheet/190190/)

*Une vision de fan de ce qu'aurait pu être la ville C dans la version finale, par [EpicPixelMan](https://www.spriters-resource.com/submitter/EpicPixelMan/)*

</div>

## Source et crédits

- Cette page est en partie une traduction [de cette page de TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Red_and_Blue/Unused_Maps). Elle contient une vue plus détaillée des cartes
- Le [remake de la ville C](https://www.spriters-resource.com/custom_edited/pokemongeneration1customs/sheet/190190/) est par [EpicPixelMan](https://www.spriters-resource.com/submitter/EpicPixelMan/)
- Un [article plus complet sur l'évolution de Kanto](https://helixchamber.com/2019/07/22/proto-map-analysis/) a été fait par le site Helix Fossil.