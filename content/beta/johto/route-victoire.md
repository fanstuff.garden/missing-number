---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Route Victoire
  parent: Région de Johto
  order: 10
---

# Route Victoire

## Route 27

<div style="text-align:center;width:100%;margin:auto;">

![Route 27](/img/betagold/jotho1/route-27-1.png)

*Route 27<br />06/10/1998*

</div>

La route est bien plus simpliste, manquant l'utilisation de surf, et les chutes tohjo.

## Route 28

<div style="text-align:center;width:100%;margin:auto;">

![Route 28](/img/betagold/jotho1/route-28-1.png)

*Route 28<br />06/10/1998*

</div>

Route plus petite, sans la maison de la célébrité. La route est directement lié à la route 27. On peut imaginer un PNJ qui bloque le joueur après les hautes herbes de la map, au nord.

## Mont Argenté

<div style="text-align:center;width:100%;margin:auto;">

![Entrée du mont argenté](/img/betagold/jotho1/mont-argente-1.png)

*Entrée du mont argenté<br />06/10/1998*

</div>

L'entrée du mot Argentée / Route 28 est radicalement différente, avec une forêt labyrinthique avant d'entrée dans la montagne. On peut imaginer qu'à l'époque, l'entrée de la grotte avait un garde comme pour la grotte azurée / donjon inconnu pour éviter d'y entrer à la ligue, si une telle restriction était déjà présente à l'époque.

## Route 26

<details>
<summary>Cliquez ici pour afficher la Route 26 (image longue)</summary>

<div style="text-align:center;width:80%;margin:auto;">

![Route 26](/img/betagold/jotho1/route-26-1.png)

*Route 26<br />06/10/1998*

</div>

</details>

La route à deux maisons en moins, et à la place un donjon qui a été retiré, la maison hantée.

## Maison hantée

|   |   |
|:-:|:-:|
| ![Maison hantée (RDC)](/img/betagold/jotho1/maison-hantee-1.png)<br />*RDC* | ![Maison hantée (Étage)](/img/betagold/jotho1/maison-hantee-2.png)<br />*Étage* |
| ![Maison hantée (Grenier)](/img/betagold/jotho1/maison-hantee-3.png)<br />*Grenier* | ![Maison hantée (Cave)](/img/betagold/jotho1/maison-hantee-4.png)<br />*Cave* |


Un donjon abandonné représentant une maison hantée, sans doute par la grande diminution de type spectre comparé au pokedex beta.

## Route 23

<div style="text-align:center;width:100%;margin:auto;">

![Route 23](/img/betagold/jotho1/route-23-1.PNG)

*Route 23<br />06/10/1998*

</div>

La Route 23, avec un style intéressante pour la ligue pokémon.