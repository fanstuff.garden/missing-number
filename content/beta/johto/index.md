---
layout: layouts/base-toppage.njk
eleventyNavigation:
  key: Région de Johto
  parent: Éléments des bétas
  order: 12
---

# La région de Johto

La région de Johto à pas mal évoluée entre sa création initiale (lorsque le choix a été remplacé de créer une seconde région puis un retour à Kanto, plutôt qu'un voyage "autour du Pays") et la version que nous avons découvert dans le jeu final.

Le but de cette page est de montrer comment différents lieux ont évolués entre les versions.

## Sous-pages

<ul>
  {%- for post in collections.all | getSubPages(eleventyNavigation.key) -%}
    <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
  {%- endfor -%}
</ul>

## Carte de la région

<div style="text-align:center;width:80%;margin:auto;">

![Map initial de Johto](/img/betagold/jotho1/map.png)

*Map de 1998 reconstruite par les utilisateurs de TCRF*

</div>

Nous pouvons remarquer que parmis les changements "de haut niveau", on peut voir les différences suivantes :
- La ville d'Irisia ainsi que son accès est complètement absente des version démo. Son inclusion est donc assez tardive dans l'histoire du jeu.
- On y retrouve quasiment pas de grotte "obligatoire", mais c'était prévu de les rajouter après : on les retrouve dans des routes intermédiaires. Une est présente à l'est de Bourg-Geon, après une sorte de forêt. Cependant, certaines apparaissent au fur et à mesure des cartes.
- Doublonville et Oliville étaient reliée via Surf.
- Les arènes arrivent beaucoup plus vite dans la première map : arrivé à doublonville, on a traversé 5 villes avec une arène.

## Région de Kanto

<div style="text-align:center;width:80%;margin:auto;">

![Map initial de Kanto](/img/betagold/jotho1/kanto.png)

*Map de 1998 reconstruite par les utilisateurs de TCRF*

</div>

La région de Kanto garde la même idée de base que la map finale (être une version légèrement réduite de Kanto, avec certains donjons retirés), mais à aussi eut de nombreuses modifications :

- Cramois'ile n'était pas détruite
- Certains donjons et lieux étaient toujours accessibles (en plus de ceux cramois'îles) : le musée d'argenta, le parc safari, les îles écumes, la grotte azurée
- Le square sélénite n'était pas encore présent
- La grotte sombre était innaccessible, et à la place la centrale et Lavanville pouvaient être accédé à pied
- On pouvait bypasser le mont sélénite
- De nombreuses villes ont un layout plus proche de celui originel

## Sources et crédits

- Les cartes globales viennent de [cette page de TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Unused_Maps)
- Les informations sur les villes viennent de [cette page](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Unused_Maps/Towns_%26_Cities) sur le sujet de TCRF (images inclues)
- Les informations sur les routes viennent de [cette page](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Unused_Maps/Routes) sur le sujet de TCRF (images inclues)
- Certaines images viennent de [la page sur les maps inutilisée](https://tcrf.net/Pok%C3%A9mon_Gold_and_Silver/Unused_Maps) de la version finale de GSC.