---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Mauville
  parent: Région de Johto
  order: 2
---

# Mauville

Mauville a eut quelques évolutions avant sa version finale, mais garde ses aspects principaux : côté traditionnel, présence d'une tour, etc.

## Route 31

<div style="text-align:center;width:80%;margin:auto;">

![Route 30](/img/betagold/jotho1/route-31-1.png)

*Route 31<br />06/10/1998*

</div>

La Route 31 était assez différente avant la version finale : des hautes herbes facultative, pas de grotte, et une maison (qui est sans doute celle de Mr Pokémon.)

## La ville

| 06/10/1998 | 11/12/1998 | 
|:----------:|:----------:|
| ![Mauville 1](/img/betagold/jotho1/mauville-1.png) | ![Mauville 2](/img/betagold/jotho1/mauville-2.PNG) |

La ville a eut différents layout suivant les versions, notamment pour la rendre moins symétriques. On peut noter que dans la seconde version.

Dans la première version, la tour chetiflor n'était visiblement pas accessible avant d'avoir eut coupe (tandis que dans la version finale, on y obtient la CS Flash), et a été déplacé plusieurs fois avant d'attendre sa version finale.

Les Ruines Alpha ont été ajouté entre les deux vrsions, donc quelque part entre Octobre et Décembre.


## Route 32

La Route 32 garde le principe de base dans sa version finale : C'est une route cotière et un peu forestière. Cependant, divers changements sont visible, et la version originelle est bien plus simple que celle finale.

<details>
<summary>Cliquez ici pour afficher la Route 32 (image longue)</summary>

<div style="text-align:center;width:80%;margin:auto;">

![Route 32](/img/betagold/jotho1/route-32-1.png)

*Route 32<br />06/10/1998*

</div>

</details>


Parmi les différences, on peut remarquer :
- Pas de haute herbe obligatoire
- Pas d'accès au ruines alpha
- Pas de centre pokémon et cave jumelle à la fin
- Pas de ponton dans la partie "mer" de la route