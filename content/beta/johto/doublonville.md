---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Doublonville
  parent: Région de Johto
  order: 4
---

# Doublonville

La plus grande ville de Jotho est reprise des anciennes villes "West" et "High Tech" de la version 97 de Pokémon Or et Argent. Elle y reprend notamment la présence de la tour radio.

## Route 34

La route 34 est très primitive dans la version d'origine.

<div style="text-align:center;width:80%;margin:auto;">

![Route 34](/img/betagold/jotho1/route-34-1.png)

*Route 34, 06/10/1998*

</div>

On peut noter l'absence de la pension, le fait que la route soit bien plus petite et que ses hautes herbes sont obligatoires. Les zones accessibles avec surf ne sont pas encore là à l'époque, ayant juste par un point d'eau dans la version initiale.

## Doublonville

| 06/10/1998 | 02/11/1998 | 
|:----------:|:----------:|
| ![Doublonville](/img/betagold/jotho1/doublonville-1.png) | ![Doublonville](/img/betagold/jotho1/doublonville-2.PNG) |

Ces deux versions initiales sont identiques à quelques détails prêts. Par rapport à la version finale, on remarque la différences suivantes : l'absence totale de bâtiments "spéciaux" à l'exception de la tour radio. Il y a juste un centre pokémon, un magasin (normal) et l'arêne, et une maison ouverte. Cela laisse supposer que tout les bâtiments "bonus" n'étaient pas encore fait et que ça ne contenait que ce qui était essentiel pour avancer dans le jeu.

Cela laisse supposer que la plupars des modifications de la ville ont été faites justement pour embarquer ces bâtiments annexes (en plus de la suppression de l'accès vers la mer).

## Route 35

<div style="text-align:center;width:80%;margin:auto;">

![Route 35](/img/betagold/jotho1/route-35-1.png)

*Route 35, 06/10/1998*

</div>

La route 35 était également bien plus simple :
- Absence du parc naturel
- Absence de la petite route disponible sur le côté
- Absence de la zone accessible uniquement avec surf.

Cela rejoint ce qu'on peut constater de la plupars des autres routes : une route sans floriture, sans encore tout les petits aspects "bonus".

## Route 36

La route 36 est intéressante parce que bien plus "ouverte" que sa version finale

<div style="text-align:center;width:80%;margin:auto;">

![Route 36](/img/betagold/jotho1/route-36-1.png)

*Route 36, 06/10/1998*

</div>

La route qui était plutôt étroite avec un point de passage d'une case entre les trois villes est ici bien plus grande et ouverte, ce qui laisse supposer que soit elle était pas bloquée dès ici, soit que c'était pour l'instant pas encore bloqué pour tester plus facilement.

Une théorie possible était le le joueur pouvait avancer jusqu'à la route 35 mais serait bloqué avant doublonville, le forçant alors à faire le tour.

