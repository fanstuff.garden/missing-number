---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Ecorcia
  parent: Région de Johto
  order: 3
---


# Ecorcia

Ecorcia était particulière parce qu'elle est resté longtemps deux ville séparée par une route, avant d'être fusionnée vers une période inconnue avant la version finale.

Cette fusion a sans doute été faite pour rendre les villes plus intéressantes visuellement.

## Ville 1

La première des deux villes composant ecorcia. À l'origine une petite ville avec une rivière.

| 06/10/1998 | 10/12/1998 | 
|:----------:|:----------:|
| ![Une petite ville avec une rivière](/img/betagold/jotho1/ecorcia1-1.png) | ![Une petite ville avec une rivière](/img/betagold/jotho1/ecorcia1-2.PNG) |

Cette ville devait contenir une arêne.

## Route 33

La route 33, qui se trouve avant Ecorcia dans la version finale, se trouve entre les deux villes dans les versions anciennes.

|   |
|:-:|
| *06/10/1998* <br /> ![Route 33 1](/img/betagold/jotho1/route-33-1.png) |
| *13/10/1998* <br /> ![Route 33 2](/img/betagold/jotho1/route-33-2.png) |

Comme pour la route 29, on peut noter l'ajout d'une grotte pour séparer les deux villes. Cela aurait sans doute servi à éloigner les deux arênes, en ayant un donjon entre les deux.

## Ville 2

La seconde ville qui participera à constituer Ecorcia.

| 06/10/1998 | 10/12/1998 | 
|:----------:|:----------:|
| ![Une petite ville forestière](/img/betagold/jotho1/ecorcia2-1.png) | ![Une petite ville forestière](/img/betagold/jotho1/ecorcia2-2.PNG) |

On peut noter que l'arêne a supprimer entre les deux itération (sans doute pour avoir un rythme plus soutenu, et que juste après c'est Doublonville).

Une maison avec un panneau est absente, peut-être la maison de Fargas.