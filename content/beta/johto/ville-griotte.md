# Ville Griotte

Bravo, vous avez trouvé une page béta dans la section béta :p

## Ville Griotte

Ville Griotte est bien plus grande dans ses versions initiales, une partie de la ville sera remplacée par la route 30 dans la version finale.

| 02/10/1998 | 24/12/1998 | 
|:----------:|:----------:|
| ![Ville Griotte 1](/img/betagold/jotho1/ville-griotte-1.png) | ![Ville Griotte 2](/img/betagold/jotho1/ville-griotte-2.PNG) |

Entre les deux revisions visible, la ville est passé du style "ancien" au style moderne des villes, les bâtiments pagodes ayant été remplacé par des 

Nous pouvons noter également que la première version de Ville Griotte contient une arêne. Cependant, nous ne savons pas s'il était prévu que ce soit la première arêne (ce qui pourrait expliquer son faible niveau), ou s'il était prévu qu'elle devienne accessible plus tard dans le jeu, à la manière de celle de Jadielle dans Pokémon Rouge et Bleu.

 Dans les deux versions, il y a aussi un bâtiment accessible que via coupe (qui peut potentiellement être l'endroit débloquant l'arêne dans la première version ?)

## Route 30

<div style="text-align:center;width:80%;margin:auto;">

![Route 30](/img/betagold/jotho1/route-30-1.png)

*Route 30, 06/10/1998*

</div>

La route 30 était beaucoup plus petite dans cette version, étant un simple petit chemin avec un pont amenant vers Mauville et des hautes herbes facultatives.