---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Rosalia
  parent: Région de Johto
  order: 5
---

# Rosalia

L'ancienne ville de Jotho présente peu de grande différence fondamentale avec la version finale, mais quelques unes sont intéressante à observer. Nous avons des anciennes versions des deux tours de la villes, notamment.

La ville est dérivée de l'ancienne ville de "Old" lorsque le jeu se passant dans la "région" de Nihon.

## Route 37

<div style="text-align:center;width:50%;margin:auto;">

![Route 37](/img/betagold/jotho1/route-37-1.png)

*Route 37<br />06/10/1998*

</div>

La route 37 a juste changé un peu superficiellement dans sa version finale, son concept de base d'être divsé en deux chemin étant déjà présent à cette époque

## Rosalia

Une seule ancienne version de Rosalia existe (outre son prédécesseur qui est la ville de "Old")

<div style="text-align:center;width:50%;margin:auto;">

![Rosalia](/img/betagold/jotho1/rosalia-1.PNG)

*Rosalia<br />06/10/1998*

</div>

Sa structure a été pas mal changé dans la version finale, ayant à cette époque un grand espace presque vide au centre. La tour cendrée et le centre pokémon ont échangé de place, et ce dernier nécessitait coupe pour être accédé. De plus, le train passait par Rosalia à l'époque (malgré la proximité des villes). Avant la création de la ville d'Irisia, c'était cette ville qui contenanit la pharmacie.

## Tour Cendrée

Nous avons quelques maps originelles de la Tour Cendrée. Ces mockups ont été restauré par des utilisateurices TCRF.

| Version prototype | Version plus tardive |
|:-----------------:|:--------------------:|
| ![Tour cendrée rez de chaussé](/img/betagold/jotho1/tour-cendree1-1.png) | ![Tour cendrée rez de chaussé](/img/betagold/jotho1/tour-cendree1-2.png) |

On note outre quelques différences d'architectures principalement la différence du tileset, utilisant des rondins de bois. Il a aussi été retrouvé une autre map, un "second sous-sol" de la tour, utilisant un tileset de grotte et utilise des lettres.

<div style="text-align:center;width:50%;margin:auto;">

![Rosalia](/img/betagold/jotho1/tour-cendree2-1.PNG)

*Tour Cendrée, Second Sous-Sol<br />??/??/1998*

</div>

## Tour Ferraille

Nous avons les maps de la tour-feraille, sur 9 étages :

|  |  |  |
|:-----------------:|:--------------------:| :--------------------:|
| ![Rez de chaussé](/img/betagold/jotho1/tour-ferraille1.png) | ![Etage 1](/img/betagold/jotho1/tour-ferraille2.png) | ![Etage 2](/img/betagold/jotho1/tour-ferraille3.png) |
| ![Etage 3](/img/betagold/jotho1/tour-ferraille4.png) | ![Etage 4](/img/betagold/jotho1/tour-ferraille5.png) | ![Etage 5](/img/betagold/jotho1/tour-ferraille6.png) |
| ![Etage 6](/img/betagold/jotho1/tour-ferraille7.png) | ![Etage 7](/img/betagold/jotho1/tour-ferraille8.png) | ![Etage 8](/img/betagold/jotho1/tour-ferraille9.png) |

Il est à noter que le dernier étage remplace le toit, et était sans doute là ou l'on pouvait capturer Ho-oh.