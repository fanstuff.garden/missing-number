---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Acajou & Lac Colère
  parent: Région de Johto
  order: 8
---

# Acajou & Lac Colère

## Route 42

<div style="text-align:center;width:100%;margin:auto;">

![Route 42](/img/betagold/jotho1/route-42-1.png)

*Route 42, 06/10/1998*

</div>

La route 42 est bien plus simple dans cette version, n'ayant ni passage ou il faut utiliser surf, ni le Mont Creuset. Il s'agit d'une simple route de passage. Nous remarquons que comme souvent, rien n'y entrave vraiment le passage du joueur, ce qui montre le côté "temporaire" de la carte.

## Acajou

| 06/10/1998 | 15/12/1998 | 
|:----------:|:----------:|
| ![Acajou 1](/img/betagold/jotho1/acajou-1.png) | ![Acajou 2](/img/betagold/jotho1/acajou-2.PNG) |

La première version d'Acajou était assez différente, n'ayant ni arène, ni centre pokémon, mais juste à priori deux maisons spéciale (ayant des panneau), une maison "ancienne" et une pagode. Elle prend ensuite une forme relativement proche de la version finale assez vite, à l'exception du magasin qui sera remplacée par la boutique de souvenir.

## Route 43

<details>
<summary>Cliquez ici pour afficher la Route 43 (image longue)</summary>

<div style="text-align:center;width:100%;margin:auto;">

![Route 43](/img/betagold/jotho1/route-43-1.png)

*Route 43, 06/10/1998*

</div>

</details>

La route 43 possède quelques similarité déjà avec la final, étant déjà un ensemble de chemins à travers les arbres. Dans cette version originelle, une maison avec un petit étang est présente, sans doute une version ancienne de la maison du pêcheur.

## Lac Colère

| 06/10/1998 | 15/12/1998 | 
|:----------:|:----------:|
| ![Lac Colère 1](/img/betagold/jotho1/lac-colere-1.png) | ![Lac Colère 2](/img/betagold/jotho1/lac-colere-2.PNG) |

Une zone qui a eut de nombreuses évolutions suivant les versions, puisqu'elle est passé d'un endroit ou se trouvait une tour importante, à une un endroit avec une ville, à sa version finale qui n'a que deux maison. On peut supposer que l'arène devait être soit l'arène désormais d'Irisia, soit celle d'Oliville ou Acajou si les trois arènes ont été mélangées avec le temps.

## Route 44

<div style="text-align:center;margin:auto;">

![Route 44](/img/betagold/jotho1/route-44-1.png)

*Route 44, 06/10/1998*

</div>

La route est très proche de sa version finale, avec les différentes suivantes :
- La route est un sentier plus "forestier" contrairement à la version finale qui est entourée de falaise
- La fin de la route amenait directement vers Ébènelle est non vers une grotte (le chemin de glace) pour y accéder
- La partie centrale avec le haute herbe entourée par l'eau a été un peu remaniée dans la version finale
- Un petit étang a été ajouté dans la version finale au sud-ouest