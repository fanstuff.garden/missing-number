---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Début du jeu
  parent: Région de Johto
  order: 1
---

# Début du jeu

La ville de départ de la région, ou notre personnage à sa maison et l'on rencontre le professeur pokémon. Cette page parle aussi de la ville suivante, Ville Griotte.

## Bourg Geon

| 02/10/1998 | 06/10/1998 | 10/11/1998 |
|:----------:|:----------:|:----------:|
| ![Bourg geon 1](/img/betagold/jotho1/bourg-geon-2.png) | ![Bourg geon 2](/img/betagold/jotho1/bourg-geon-1.png) | ![Bourg geon 3](/img/betagold/jotho1/bourg-geon-3.png) |

On peut voir qu'entre les trois prototypes de la map :
- Une maison a été rajoutée
- L'une des maisons a été à un moment remplacé par un centre pokémon (changement renversé dans la version finale)
- De l'eau a été mise pour bloquer le passage vers la route victoire et le Mont Argenté

Dans la première version, la ville était un passage entre vers la ligue et la première ville, ce qui laisse supposer que soit c'était fait pour permettre de plus facilement accéder au lieu pour tester, soit un PNJ aurait été là pour bloquer le passage si le joueur avait voulu passer.

## Route 29

|   |
|:-:|
| *06/10/1998* <br /> ![Route 29 1](/img/betagold/jotho1/route-29-1.png) |
| *13/10/1998* <br /> ![Route 29 2](/img/betagold/jotho1/route-29-2.png) |
| *14/10/1998* <br /> ![Route 29 3](/img/betagold/jotho1/route-29-3.png) |

La route 29 a eut pas mal d'évolution, sa version initiale étant très simple, une route avec un peu de haute herbe amenant vers la première ville. Vu que la première ville à une arène, cela laisse supposer soit un très bas niveau de l'arène (ce qui pourrait expliquer le niveau de la version finale), soit que cette première map n'avait pas pour plan de rester comme ça.

Les version 2 et 3 du 13 et 14 octobre, indique qu'une grotte devait être présent entre la première et la seconde ville, avec la possibilité par un autre chemin d'aller en haut de la colline. La version du 14 semble incomplète, manquant des hautes herbes et tout. On peut imaginer que ça aurait été un chemin pour accéder à quelque chose de particulier, voir dans la version 3, cela pourrait être un chemin pour accéder à une partie spéciale de la grotte, mais uniquement depuis la route du haut.

## Ville Griotte

Ville Griotte est bien plus grande dans ses versions initiales, une partie de la ville sera remplacée par la route 30 dans la version finale.

| 02/10/1998 | 24/12/1998 | 
|:----------:|:----------:|
| ![Ville Griotte 1](/img/betagold/jotho1/ville-griotte-1.png) | ![Ville Griotte 2](/img/betagold/jotho1/ville-griotte-2.PNG) |

Entre les deux revisions visible, la ville est passé du style "ancien" au style moderne des villes, les bâtiments pagodes ayant été remplacé par des 

Nous pouvons noter également que la première version de Ville Griotte contient une arêne. Cependant, nous ne savons pas s'il était prévu que ce soit la première arêne (ce qui pourrait expliquer son faible niveau), ou s'il était prévu qu'elle devienne accessible plus tard dans le jeu, à la manière de celle de Jadielle dans Pokémon Rouge et Bleu.

 Dans les deux versions, il y a aussi un bâtiment accessible que via coupe (qui peut potentiellement être l'endroit débloquant l'arêne dans la première version ?)

## Route 30

<div style="text-align:center;width:80%;margin:auto;">

![Route 30](/img/betagold/jotho1/route-30-1.png)

*Route 30, 06/10/1998*

</div>

La route 30 était beaucoup plus petite dans cette version, étant un simple petit chemin avec un pont amenant vers Mauville et des hautes herbes facultatives.