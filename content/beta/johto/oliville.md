---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Oliville & Irisia
  parent: Région de Johto
  order: 6
---

# Oliville & Irisia

La partie la plus à l'ouest de Jotho rencontrait une différence majeure dans la version originelle de Johto : l'absence totale d'Irisia, qui n'était pas encore ajouté dans la région. Son ajout sera fait après 98.

## Route 38

<div style="text-align:center;width:100%;margin:auto;">

![Route 38](/img/betagold/jotho1/route-38-1.png)

*Route 38<br />06/10/1998*

</div>

La route qui relie Rosalia à la ferme est plus simpliste, composé juste d'un chemin ondulant et de quelques hautes herbes. Elle gagnera une forme bien plus définie dans sa version finale.

## Route 39

<div style="text-align:center;width:50%;margin:auto;">

![Route 39, une petite route avec une ferme](/img/betagold/jotho1/route-39-1.png)

*Route 39<br />06/10/1998*

</div>

La ferme était bien plus grande, constitué de plus de bâtiment, et n'avait pas de paturage comme dans la version finale. La répartition des hautes herbes est également différentes, avec une grande zone de haute herbe, et une petite mare.

## Oliville

<div style="text-align:center;width:50%;margin:auto;">

![Oliville, une petite ville portuaire](/img/betagold/jotho1/oliville-1.png)

*Oliville<br />06/10/1998*

</div>

La ville possède un certain nombre de différence :
- Pas de route vers l'ouest
- L'arêne se trouve vers le sud
- Une petite zone résidentielle est présente sur le côté, avec deux maisons accessibles

La ville est globalement également aussi plus grande, avec plus de maisons "vide" pour indiquer la taille de la ville. Il n'y a aussi pas d'arbre sur le côté.

## Route 40

<div style="text-align:center;width:50%;margin:auto;">

![Route 40](/img/betagold/jotho1/route-40-1.png)

*Route 40<br />06/10/1998*

</div>

La route 40 n'était à cette époque qu'un grand chenal d'eau, sans plage ni zone terrestre.

## Route 41

<div style="text-align:center;width:100%;margin:auto;">

![Route 41](/img/betagold/jotho1/route-41-1.png)

*Route 41<br />06/10/1998*

</div>

La route 41 est dans cette version reliée à Doublonville et non à Irisia, du à l'absence de cette dernière. Les tourb'îles étaient également absente à cette époque, il ne s'agit que d'une route d'eau pour connecter les deux villes.