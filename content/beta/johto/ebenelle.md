---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Ébènelle
  parent: Région de Johto
  order: 9
---

# Ébènelle

La dernière ville à bien changé entre sa version finale. Elle y est bien moins montagneuse, en partie amplifiée par le fait qu'on y accède plus par une grotte. Les trois grottes liées à la ville sont d'ailleurs absentes.

## Ébènelle

<div style="text-align:center;width:50%;margin:auto;">

![Ébènelle, une petite ville de montagne](/img/betagold/jotho1/ebenelle-1.png)

*Ébènelle<br />06/10/1998*

</div>

La ville à rencontré diverse modifications avant d'arriver à sa version finale :
- Tout d'abord, la zone au nord avec surf et l'antre du dragon était une sorte de labyrinthe avec des arbustres et des pentes. On peut imaginer qu'il y avait des dresseurs à battre.
- Une pagode se trouve à l'endroit ou on rentrera dans la version finale via la route de glace, peut-être l'ancêtre de l'antre du dragon ?
- Une maison en moins est présente
- La ville est plus forestière, avec plus d'herbe, et moins "montagnarde"

## Route 45

<details>
<summary>Cliquez ici pour afficher la Route 43 (image longue)</summary>

<div style="text-align:center;width:100%;margin:auto;">

![Route 45](/img/betagold/jotho1/route-45-1.png)

*Route 45, 06/10/1998*

</div>

</details>

La route 45 est quasiment identique à la version finale, si ce n'est qu'il y a plus d'herbe, pas de haute herbe, et l'absence de l'antre noire.