---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Kanto
  parent: Région de Nihon
  order: 8
---

# Kanto

La zone de Kanto est l'une des plus particulière du jeu, à plusieurs niveaux. Tout d'abord, il y a que c'est une réduction de la région éponyme à juste une seule ville, contenant plusieurs des lieux emblématique des jeux précédants, certaines villes n'étant représentée que par une seule maison parfois.

Cette ville plus précisément regroupe la partie "principale" de Kanto, n'ayant pas Cramois'île, ni toutes les zones liée à la ligue pokémon. L'accès vers la route 23 se trouvant vers là ou est Jadielle on peut imaginer que la ligue pokémon est celle qu'on accède à Prince.

## Route 22

<div style="text-align:center;margin:auto;">

![Une petite route horizontale](/img/betagold/nihon/route22.png)

</div>

La route qui amène à Kanto. C'est une petite route qui utilise le même style de tuile que Kanto, qui est inspiré de celles du jeu précédant. La route n'avait pas de haute herbes dans la démo, la map montrée ici est une version ultérieur qui les rajoutent.

## Kanto

<div style="text-align:center;margin:auto;">

![Une ville reprenant grossièrement la région de kanto](/img/betagold/nihon/kanto.png)

</div>

Kanto est de loin la plus grande ville du jeu, et possède quelques particularité : il a deux arènes (ce qui fait monter le compte à 9 arènes, mais l'une des deux est sans doute non-officielle), deux centre pokémon, un petit magasin et un supermarché (qui sont juste à côté).

La ville contient un certain nombre de lieux importants :
- Le bourg palette y est reconstruit fidèlement
- Jadielle est réduit à une petite maison normale et une porte vers la route suivante
- Argenta est réduit à **une arêne** (celle avec Red) et un centre pokémon
- Azuria est réduite à une **maison étrange**, avec un sous-sol innacessible.
- Lavanville est réduite à la tour pokémon (fermée) et une maison normale
- Une grande école se trouve plus ou moins en dessous de là ou se trouve habituelle Lavanville
- Celadopole est fusionnée à Safrania, des bâtiments de Celadopole se trouvant de manière visible dans la zone de Safrania
- Parmanie est réduite à une maison ordinaire.
- Des bâtiments se trouvent à un endroit normalement non-exploité de Kanto, le Sud-Est. On y trouve **une autre arène**.

D'une manière amusante, cela rend Kanto plus proche du Kanto IRL (son inspiration dans le japon) : Kanto étant majoritairement occupé par la municipalité de Tokyo-Yokohama.

## Bourg Palette

Bourg Palette est reconstruit assez fidèlement sur la map, et contient les lieux suivants :
- La maison de Red
- La maison de Blue
- Le laboratoire Pokémon

C'est la seule ville reconstruite autant à l'identique, et à ne perdre aucun bâtiment. La seule différence est que les maisons de Red et Blue ont été inversée. Juste au nord s'y trouve ce qui reste de Jadielle, une maison et l'accès à la route suivante.

<div style="text-align:center;margin:auto;">

![Un petit laboratoire](/img/betagold/nihon/kantolab.png)

</div>

Le laboratoire du Prof Chen y est plus petit que dans la version finale et les jeux originaux.

## Arène(s) de Kanto

<details>
<summary>Cliquez ici pour en voir plus sur les arènes</summary>

<div style="text-align:center;margin:auto;">

| RDC | Étage |
|:------:|:-----:|
| ![Une arêne ressemblant à celle de Stand : Une arene avec des plantes d'intérieur](/img/betagold/nihon/arene8.png) | ![La même arène, mais avec un homme légèrement chauve à la place de red](/img/betagold/nihon/arene8-alt.png) |

</div>

</details>

Kanto est indiqué comme ayant deux arènes. Il est difficile de savoir si une était supposé être fausse ou non. Cependant, on peut remarquer que l'une d'entre elle utilise Red comme champion d'arène.

## Celadopole & Safrania

Plusieurs bâtiments de Celadopole et Safrania sont visible sur la map et sont accessible :
- Le supermarché
- Le casino et la maison pour récupérer les prix
- La résidence ou se trouve Game Freak
- La Sylph SARL (dont seul le premier étage est accessible, comme dans la version finale)

<details>
<summary>Cliquez ici pour en voir plus sur le casino</summary>

<div style="text-align:center;margin:auto;">

| Casino | Pièce en plus | Prix | ??? |
|:------:|:-----:|:------:|:------:|
| ![Un casino](/img/betagold/nihon/casino1.png) | ![Une pièce secrète avec des tables](/img/betagold/nihon/casino2.png) | ![La salle des prix du casino](/img/betagold/nihon/casino3.png) | ![Une salle avec des tables](/img/betagold/nihon/casino4.png) |

</div>

</details>

Le casino est assez proche de sa version de Kanto, on peut noter la présence d'une salle supplémentaire, qui n'est pas accessible dans la version beta mais apparaitra dans une version ultérieur. Cette salle devait contenir des jeux de memory et de poker. Un autre bâtiment très proche à un style similaire et est juste à côté, mais c'est difficile à savoir exactement si elle était lié ou non.

<details>
<summary>Cliquez ici pour en voir plus sur la résidence Celadon</summary>

<div style="text-align:center;margin:auto;">

| RDC | 1 | 2 | Toit | Maison |
|:------:|:-----:|:------:|:-----:|:-----:|
| ![Un casino](/img/betagold/nihon/residence1.png) | ![Une pièce secrète avec des tables](/img/betagold/nihon/residence2.png) | ![La salle des prix du casino](/img/betagold/nihon/residence3.png) | ![Une pièce secrète avec des tables](/img/betagold/nihon/residence4.png) | ![La salle des prix du casino](/img/betagold/nihon/residence5.png) |

</div>

</details>

La résidence Celadon est très proche de la version finale et de celle originale.

## Autres endroits

<div style="text-align:center;margin:auto;">

![Une petite maison avec un escalier](/img/betagold/nihon/azuria.png)

</div>

À l'emplacement de Azuria se trouve une maison dont l'utilité est difficile à définir. Vu la position à côté de montagne, on peut se demander s'il ne s'agit pas d'un accès vers la grotte azurée. Cependant, l'escalier ne semble mener nul part.

## Route 23

<div style="text-align:center;margin:auto;">

![Une longue route horizontale](/img/betagold/nihon/route23.png)

</div>

La route qui amène vers Silent Town.