---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: NewType et Sugar
  parent: Région de Nihon
  order: 5
---

# NewType et Sugar

La zone de newtype est une zone typée contemporaine, avec un bras d'eau amenant à la petite ville de Sugar. La ville est surement là ou on obtient au moins surf, puisque deux zone après demande surf pour aller plus loin.

## Route 15

<div style="text-align:center;margin:auto;">

![Une petite route avec une forêt](/img/betagold/nihon/route15.png)

</div>

La route devait contenir de manière évidente une forêt, mais celle-ci n'est pas présente dans la beta : les zone de téléportation amène directement à l'autre bout de la forêt.

## NewType

<div style="text-align:center;margin:auto;">

![Une ville technologique séparé par une rivière](/img/betagold/nihon/newtype.png)

</div>

Une grande ville portuaire, dont la structure globale nous rappelle un peu la ville de Joliberges de Sinnoh. Le train y passe, reliant la ville à Kanto et West.

Les points d'intérêt y sont:
- L'arène
- La grande maison avec une pokeball à l'est de la ville est un dojo
- La grnade maison à l'ouest semble être une sorte de bar/café

### Intérieurs de NewType

<details>
<summary>Cliquez ici pour en voir plus sur l'arêne</summary>

<div style="text-align:center;margin:auto;">

![Une arène avec pas mal d'eau](/img/betagold/nihon/arene5.png)

</div>

</details>

L'arène semble être une arène d'eau ou de glace.

<details>
<summary>Cliquez ici pour en voir plus sur le dojo</summary>

<div style="text-align:center;margin:auto;">

![Un dojo](/img/betagold/nihon/dojo.png)

</div>

</details>

Un dojo est disponible dans la ville, et permettait sans doute de combattre des pokémon de type combat, et potentiellement d'obtenir Débugan dans cette version.

<details>
<summary>Cliquez ici pour en voir plus sur le bar</summary>

<div style="text-align:center;margin:auto;">

![Un bar](/img/betagold/nihon/bar.png)

</div>

</details>

Un petit bar/café est aussi présent dans la ville, surement surtout pour l'ambiance.

## Route 15

La route reliant NewType à Sugar.

## Sugar

<div style="text-align:center;margin:auto;">

![Une petite île avec une grotte](/img/betagold/nihon/sugar.png)

</div>

Sugar est une petite ville marittime accessible une fois que le joueur à obtenu siphon. La ville contient une école pokémon et une grotte. La grotte n'était pas encore codée dans le jeu et nous ne pouvons donc pas savoir pour l'instant ce qu'elle aurait du contenir. Suivant quand le joueur obtient Siphon, elle n'était sans doute pas accessible dès avoir fini NewType, mais peut-être plus en fin de jeu.

## Route 17 et 18

<details>
<summary>Cliquez ici pour en voir plus sur les routes 17 et 18</summary>

|  |
|:----------:|
| ![Une route de montagne horizontale](/img/betagold/nihon/route17.png) | 
| ![Une route de montagne verticale remontant vers le nord, avec la centrale dedans](/img/betagold/nihon/route18.png) |

</details>

Les routes 17 et 18 sont deux routes entourée de falaise qui amènent de New Type à la zone de Blue Forest. La particularité de la zone 18 est de posséder la centrale dedans, accessible avec coupe.

## Centrale

<details>
<summary>Cliquez ici pour en voir plus sur la Centrale</summary>

|  |  | 
|:----------:|:----------:|
| ![Une entrée avec un bureau](/img/betagold/nihon/centrale1.png) | ![Un couloir en coude](/img/betagold/nihon/centrale2.png) |
| ![Une grande pièce avec des machines](/img/betagold/nihon/centrale3.png) | ![Une grande pièce avec des machines](/img/betagold/nihon/centrale4.png) |

</details>

La centrale est un donjon avec 2 étages, et une grande entrée, accessible depuis la route 18. On y traouvait surement pas mal de pokémon éléctrique, peut-être Electhor et/ou Rai (prédécesseur de Raikou) en pokémon légendaire.