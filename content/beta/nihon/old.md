---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Old & West
  parent: Région de Nihon
  order: 2
---

# Old et West

Les deux premières villes que l'on rencontre (hors celle de départ) montrent un contraste intéressant. On effet, on commence par traverser une ville ancienne inspirée de Kyoto, puis une ville plus moderne basée sur Osaka. Ce contraste est d'autant plus intéressant que ce sera un des contrastes important qu'aura les version finale d'Or et Argent, alternant entre des villes typée traditionnelle (Ecorcia, Rosalia, Mauville, etc) et des villes plus modernes (Doublonville). 

On retrouve dans ces villes plusieurs des endroits typiques du jeu, tel qu'une Tour rappellant les Tour Cendrée, Chetiflor et Ferraille, la Tour Radio, etc.

( Non je ne les ai pas mis ensemble juste pour la blague du "Old West". Pas *juste* pour ça. )

## Old

La ville de Old est inspirée de Kyoto, et représente une ville ancienne japonaise. Certains de ses aspects seront divisé dans plusieurs villes de la version finale tel que Rosalia, Ecorcia ou Mauville.

<div style="text-align:center;margin:auto;">

![Une ville façon japon ancien](/img/betagold/nihon/old.png)

</div>

On y retrouve plusieurs batiment spéciaux, tels que :

- Une tour proche de celles qu'on aura dans le jeu final (Chetiflor, Cendrée et Feraille)
- Une arêne, qui est intéressante parce qu'elle a un design non standard (c'est le bâtiment en haut à droite)
- Le batiment en dessous du point d'eau est l'école pokémon
- Le bâtiment en bas à gauche est la maison de Fargas (qui est assez proche de la finale)
- Le bâtiment en dessous de l'arêne est la maison de léo
- Le bâtiment en haut à gauche est une sorte de musée ou autel

Le point d'eau n'amène nul part sur la map.

### Intérieurs de Old

<details>
<summary>Cliquez ici pour en voir plus sur l'arêne</summary>

| Version Spaceworld | Version plus ancienne | 
|:----------:|:----------:|
| ![Un intérieur avec des sortes de falaises](/img/betagold/nihon/arene1.png) | ![Un intérieur avec des chemins au dessus du vide](/img/betagold/nihon/arene1-next.png) |

</details>

L'intérieur de l'arène aura deux version, une version la faisant ressembler à des falaise qu'on gravis, et une autre à un chemin au dessus du vide. Elle était certainement de type vol comme l'arêne de Mauville de la version finale.

<details>
<summary>Cliquez ici pour en voir plus sur la tour</summary>

|  |  |   |
|:----------:|:----------:|:----------:|
| ![Une entrée de tour avec deux statues d'Arcanin](/img/betagold/nihon/tourold1.png) | ![Un étage de tour avec une statue de pikachu](/img/betagold/nihon/tourold2.png) | ![Un étage de tour avec deux statues de Tentacruel](/img/betagold/nihon/tourold3.png) |
| ![Un étage de tour avec une statue de Ho-oh](/img/betagold/nihon/tourold4.png) | ![Un étage de tour avec deux statues d'Abra](/img/betagold/nihon/tourold5.png) | |

</details>

La tour comporte à chaque étage des pokémons différents. Le premier étage contient deux statues d'Arcanin, le second contient une de Pikachu, le troisième Tentacruel, le quatrième Ho-oh, et le dernier Abra.

<details>
<summary>Cliquez ici pour en voir plus sur la maison de léo</summary>

|  |  | 
|:----------:|:----------:|
| ![Un intérieur traditionnel avec un ordi](/img/betagold/nihon/leo1.png) | ![Un intérieur moderne](/img/betagold/nihon/leo2.png) |

</details>

La maison de Léo aura deux version : une version avec un intérieur traditionnel, et une avec un plus moderne.

## Route 3

<div style="text-align:center;margin:auto;">

![Une petite route avec une maison au milieu](/img/betagold/nihon/route3.png)

</div>

Old et West ne sont séparé que par une petite route, qui contient un bâtiment (qui sera retiré dans une version future de la map). 

<details>
<summary>Cliquez ici pour en voir plus sur le bâtiment</summary>

<div style="text-align:center;margin:auto;">

![Une salle avec des consoles](/img/betagold/nihon/gameroom.png)

</div>

</details>

Ce bâtiment contient une salle avec des consoles de jeu, dont l'utilité est mal définie.

## West

<div style="text-align:center;margin:auto;">

![Une ville moderne](/img/betagold/nihon/west.png)

</div>

West est une version précédante de Doublonville, avec cependant un port (qui rappellera l'accès à la mer que contient la version béta de Doublonville). La map utilisée est une map ultérieur à celle de la version spaceworld, qui ajoute notamment la station de train.

Il contient les points d'intérêt suivant :

- Une arêne en bas à gauche
- La station de train, qui permet d'accéder à NewType et Kanto
- Un Ferry menant vers la ville de *High Tech*
- Le supermarché en haut à gauche
- La tour radio en haut à droite

Il est à noter que d'autres version des sources semblent indiquer qu'un repaire de la Team Rocket y était prévu à un moment du développement, comme à Font et Stand.

### Intérieurs de West

<details>
<summary>Cliquez ici pour en voir plus sur la tour radio</summary>

|  |  | 
|:----------:|:----------:|
| ![Une entrée de tour avec un accueil et un PC](/img/betagold/nihon/tourradio1.png) | ![Un étage de tour trois bureaux](/img/betagold/nihon/tourradio2.png) | ![Un étage de tour trois bureaux](/img/betagold/nihon/tourradio3.png) | 
| ![Un étage de tour trois bureaux](/img/betagold/nihon/tourradio4.png) | ![Un étage avec un gros bureau en fond](/img/betagold/nihon/tourradio5.png) | |

</details>

La tour radio était plus petites que la version finale, et plus étroite, bien que plus large dans l'overworld.

<details>
<summary>Cliquez ici pour en voir plus sur l'arêne</summary>

<div style="text-align:center;margin:auto;">

![Une arêne contenant de la nature](/img/betagold/nihon/arene2.png)

</div>

</details>

L'arêne contient un intérieur de plante. Elle était surement de type insecte, comme l'arêne d'Ecorcia de la version finale.

## Route 5

Une Route 5 était prévue, reliant West à High Tech, qui a été remplacé par un Ferry ensuite. Cela laisse supposée que le joueur était supposé aller après à High Tech et non à Birdon.