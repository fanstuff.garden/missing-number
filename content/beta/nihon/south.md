---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: South
  parent: Région de Nihon
  order: 9
---

# South

South est une zone tropicale et volcanique qui pouvait être accédé depuis Font ou High-Tech, mais dont l'utilité exacte n'est pas définie. Elle est placée en dernier parce que n'étant pas reliée à un autre lieu en particulier, et n'a pas d'emplacement simple à définir dans le flow du jeu.

Surf était obligatoire pour y accéder, et deux des jeux sont bloqués.

## Route 7

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>

<div style="text-align:center;margin:auto;">

![Une route côtière](/img/betagold/nihon/route7.png)

</div>

</details>

Une route avec des plages, reliant South à la route 6

## South

<div style="text-align:center;margin:auto;">

![Une ville tropicale avec des volcans](/img/betagold/nihon/south.png)

</div>

La ville de South est une ville tropicale, avec des volcans visible sur le côté. La ville ne contient pas d'arène. Les volcans ne sont pas accessible et n'ont pas de donjons qui semble correspondre. On peut imaginer que c'était l'endroit ou Sulfura ou En (prédécesseur de Entei) se trouvait, et un bon endroit pour capturer pas mal de pokémon feu.

## Route 9

![Une route avec de l'herbe](/img/betagold/nihon/route9.png)

Une route menant jusqu'à Font, mais qui est bloqué à la fin.

## Route 24

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>

|   |   |
|:-:|:-:|
| ![Une route maritime](/img/betagold/nihon/route24.png) | ![Une route maritime](/img/betagold/nihon/route24-2.png) |

</details>


Un chenal qui devrait mener jusqu'à la route 10… sauf que celle-ci contient un cul de sac et ne peut pas être accéder.