---
layout: layouts/base-toppage.njk
eleventyNavigation:
  key: Région de Nihon
  parent: Éléments des bétas
  order: 11
---

# La région de Nihon

La "région de Nihon" est la région originelle dans laquelle se passe la béta de pokémon Or et Argent. L'appeller "région" est cependant en partie fausse, vu que visiblement l'idée derrière "Pokémon 2" est qu'on allait explorer tout le pays après avoir exploré la région juste de Kanto. 

Cette région semble avoir été utilisée au moins jusqu'en juin 98, ce qui laisse penser que le passage à Jotho a été commencé quelque part entre Juin 98 et Octobre 98. Des hacks permettent de jouer à des recréations de cette région. Les maps utilisé dans la version seront suivant ce que je trouve des version de juin 98 ou du spaceworld, le but étant d'essayer d'avoir un aperçu de ce qu'aurais pu être la région.

**Note :** *Tout les noms indiqué ici sont des noms prototypes, décrivant plus leurs particularités qu'étant prévu pour être leur nom finaux*

## Sous-pages

<ul>
  {%- for post in collections.all | getSubPages(eleventyNavigation.key) -%}
    <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
  {%- endfor -%}
</ul>

## TODO

- Birdon
- Font
- South et High-Tech
- NewType et Sugar
- Blue Forest & North
- Stand
- Kanto

## Carte complète

Deux version de la carte complète sont connue à ce jour, et sont relativement similaires. Les deux maps reconstruites montrées ici proviennent de The Cutting Room Floor et sont sous licence Creative Common BY (à l'exception des aspects fait par game freak)

| Version de 97 | Version de Juin 98 |
|:-------------:|:------------------:|
| ![Map de Nihon, qui ressemble au japon mais avec des environnements d'un peu partout dans le monde](/img/betagold/nihon/map-1.png) | ![Map très ressemblante avec la précédante](/img/betagold/nihon/map-2.png) |

Parmis les aspects à noter :
- La map comporte beaucoup d'environnement très différent : désert, tropical, neige, ville traditionnelles, villes désertiques, il y a une plus grande diversité dans cette version comparée à celle finale qui serra plus recentré sur quelques aspects.
- Kanto est réduit à une seulle très grande ville, modèle réduit de la région d'origine.
- La station de train était présente dans la version de 98 et reliait Kanto à West (ce qui correspond à ce qu'elle reliera dans la version finale, West étant un ancêtre de Doublonville), mais également à New Type.


## Sources et crédits

- Toutes les informations sont reprise et traduite de TCRF, ou proviennent de l'étude de la décompilation.
- Les informations sur la région de Nihon ont été reprise en partie de [cette page de TCRF](https://tcrf.net/Development:Pok%C3%A9mon_Gold_and_Silver/Unused_Maps).
- Certaines des maps ont été récupérés des roms de la beta avec l'aide de l'outil [G1.5Map](https://github.com/xdanieldzd/G15Map)