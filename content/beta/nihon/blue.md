---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Blue Forest et North
  parent: Région de Nihon
  order: 6
---

# Blue Forest et North

Cette zone est une zone particulière parce qu'elle contient des terrains enneigés, chose qu'on ne reverra pas dans Pokémon avant la quatrième génération.

## Route 19

<div style="text-align:center;margin:auto;">

![Une route marritime de neige](/img/betagold/nihon/route19.png)

</div>

Une petite route amenant à Blue Forest. C'est une route enneigée comme le reste de toute cette zone, avec une petite maison ordinaire dedans. Des siphons sont présent, mais ne barrent pas complètement la route.

## Blue Forest

<div style="text-align:center;margin:auto;">

![Une grande ville forrestière enneigée](/img/betagold/nihon/blueforest.png)

</div>

Une grande ville forestière enneigée. La ville contient les points d'intérêt suivant :

- Une grotte (qui n'est pas codée dans le jeu)
- Une arène

### Intérieurs de Blue Forest

<details>
<summary>Cliquez ici pour en voir plus sur l'arène</summary>


<div style="text-align:center;margin:auto;">

![Une arene pleine de pierre tombale](/img/betagold/nihon/arene6.png)

</div>

</details>

Une arène pleine de pierre tombale, étant une arène de type spectre. Comme les autres arènes vues précédemment, elle est dans la beta à l'étage d'une école.

## Route 20

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>


<div style="text-align:center;margin:auto;">

![Route forrestière avec des panneaux enneigés](/img/betagold/nihon/route20.png)

</div>

</details>

La route séparant Blue Forest à Stand.

## Route 26

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>


<div style="text-align:center;margin:auto;">

![Route marritime avec des siphons](/img/betagold/nihon/route26.png)

</div>

</details>

La route séparant Blue Forest à North. La numérotation et la présence de siphons laissent supposer que c'est une ville qui s'accède plus tard dans le jeux.

## North

<div style="text-align:center;margin:auto;">

![Une petite île enneigée](/img/betagold/nihon/north.png)

</div>

Une petite ville enneignée avec une grotte. La grotte n'est pas codée dans le jeu. On peut imaginer que c'était l'endroit ou Artikodin ou Sui (prédécesseur de Suicune) se trouvait, et un bon endroit pour capturer pas mal de pokémon feu.
