---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Silent & Prince
  parent: Région de Nihon
  order: 1
---

# Silent & Prince

Le début du jeu consiste en une ville : Silent Town avec la route 1 et un donjon nommé les "Silent Hills" (aucun rapport avec la série de jeu). On y trouve aussi *Prince*, la dernière ville du jeu qui est accessible depuis une grotte situé dans Silent Town.

## Silent Town

|       |
|:-----:|
| ![Une petite ville forestière, proche de Bourg Geon](/img/betagold/nihon/silent-town.png) |

La ville de Silent est la première ville du jeu, et est très proche de Bourg Geon dans sa structure et son rôle. Elle a comme la version finale la particularité d'être situé proche de Kanto (même si ici Kanto est une "ville"), et d'amener vers la fin du jeu (via une montagne qui devait surement contenir une grotte amenant vers la ville de Prince, qui contient la ligue pokémon).

Il est à noter que la ville contient un Centre Pokémon.

## Laboratoire Pokémon

| Route 1 | Route 2 | 
|:----------:|:----------:|
| ![Un intérieur de laboratoire](/img/betagold/nihon/lab1.png) | ![La suite du laboratoire](/img/betagold/nihon/lab2.png) |

Le laboratoire du Prof Chen à Silent. Il ressemble beaucoup à celui de la génération 1, mais avec une pièce à l'arrière. Il est à noter que la table contenant les trois pokémon est dans cette nouvelle pièce.

## Route 1 et 2

| Route 1 | Route 2 | 
|:----------:|:----------:|
| ![L'est d'une petite route avec des collines](/img/betagold/nihon/route1.png) | ![L'ouest d'une petite route avec des collines, remonte au nord vers un bâtiment](/img/betagold/nihon/route2.png) |

Les routes 1 et 2 sont les routes qui relient Silent Town à Old Town. On peut voir des falaise entre les deux routes, étant le *premier donjon du jeu*, les Silent Hills. Un concept similaire est présent dans l'une des version prototype de la première route de Johto.

J'ai pris ici les maps de version ultérieures retrouvées dans les sources du jeu, qui contiennent en plus l'utilisation de tuiles d'herbe pour indiquer l'entrée des Silent Hill.

## Silent Hills

|       |
|:-----:|
| ![Des collines forestiaires](/img/betagold/nihon/silent-hills.png) |

Les Silent Hills sont le premier donjon du jeu. Ils s'agit d'un donjon forestier, assez classique, rappellant la Forêt de Jade.

## Prince

|       |
|:-----:|
| ![Une petite ville de montagne](/img/betagold/nihon/prince.png) |

Prince est surement la dernière ville du jeu, supposée contenir la ligue pokémon. La ville est séparée de Silent Town par une grotte, et le grand bâtiment est surement la ligue pokémon, et la cascade ce qui amène au Mt Fuji

## Mt Fuji

|       |
|:-----:|
| ![Un chemin vers une montagne](/img/betagold/nihon/fuji.png) |

Le Mont Fuji est surement un équivalent du Mont Argenté et de la Cave Azurée : un donjon post-game. La map est fortement inachevée.