---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Stand
  parent: Région de Nihon
  order: 7
---

# Stand

Stand est une petite ville de forêt tempérée, située entre Blue Forest et Kanto.

## Route 20

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>


<div style="text-align:center;margin:auto;">

![Une longue route verticale](/img/betagold/nihon/route20.png)

</div>

</details>

## Stand

<div style="text-align:center;margin:auto;">

![Une ville forestière avec un zoo](/img/betagold/nihon/stand.png)

</div>

La ville possède les points d'intérêt suivant :
- Un Zoo, similaire à celui de Safrania de Pokémon Rouge & Bleu
- Une porte venant vers une zone inconnue (indisponible). On peut imaginer qu'il s'agissait du parc safari ou de quelque chose de similaire.
- Le bâtiment vers le centre, en bas à droite de la porte, est un ensemble de bureaux
- Le bâtiment en haut du panneau de la place centrale est la pension
- Le grand bâtiment en bas à gauche est un repaire rocket
- Une arène 

### Intérieurs de Stand

<details>
<summary>Cliquez ici pour en voir plus sur l'arène</summary>


<div style="text-align:center;margin:auto;">

![Une arene avec des plantes d'intérieur](/img/betagold/nihon/arene7.png)

</div>

</details>

L'arène est une arène avec des plante d'intérieur. Comme les autres arènes vues précédemment, elle est dans la beta à l'étage d'une école.

<details>
<summary>Cliquez ici pour en voir plus sur la pension</summary>


<div style="text-align:center;margin:auto;">

![Une petite pièce technologique](/img/betagold/nihon/pension.png)

</div>

</details>

La pension, utilisant un tileset basé sur celui du centre pokémon. La pension semblait être à ce moment quelque chose de plus technologique, et de plus proche des centres pokémons.

<details>
<summary>Cliquez ici pour en voir plus sur les bureaux</summary>


<div style="text-align:center;margin:auto;">

![Des bureaux](/img/betagold/nihon/bureaux.png)

</div>

</details>

Un ensemble de bureau, dans un bâtiment.

<details>
<summary>Cliquez ici pour en voir plus sur le repaire rocket</summary>


| RDC | Étage |
|:---:|:-----:|
| ![Le rez de chaussé d'une base rocket](/img/betagold/nihon/standrocket1.png) | ![L'étage d'une base rocket](/img/betagold/nihon/standrocket2.png) |

</details>

Une base rocket avec un étage.

## Route 21

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>


<div style="text-align:center;margin:auto;">

![Une longue route verticale](/img/betagold/nihon/route21.png)

</div>

</details>

La route 21 est une longue route verticale amenant vers la zone de Kanto.