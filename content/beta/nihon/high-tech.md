---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: High-Tech & Font
  parent: Région de Nihon
  order: 3
---

# High-Tech & Font

High Tech est une ville qu'on devait visiter assez tôt dans le jeu, par un Ferry depuis West. Cela fait une continuité aussi de thématique, nous faisant passez d'une ville technologique à une autre ville technologique.

## High-Tech

<div style="text-align:center;margin:auto;">

![Une ville technologique et portuaire](/img/betagold/nihon/hightech.png)

</div>

La ville de High Tech est une ville technologique à la manière de celle de West, ayant presque un aspect de "continuation". On y accède depuis un voyage en ferry, et on passe à la ville suivante (Font) avec un Ferry aussi.

### Intérieurs de High-Tech

<details>
<summary>Cliquez ici pour en voir plus sur l'arêne</summary>

<div style="text-align:center;margin:auto;">

| RDC | Étage |
|:------:|:-----:|
| ![Ce qui ressemble à une salle de classe, avec un escalier menant à l'arène](/img/betagold/nihon/arene3-rdc.png) | ![Une arène dans un milieu naturel](/img/betagold/nihon/arene3.png) |

</div>

</details>

L'arêne d'High Tech est la première arène à avoir une particularité, parce qu'elle est divisée en deux map, comme quelques autres arênes. En effet, l'entrée du bâtiment de l'arêne arrive vers une map qui est un clone exacte d'une carte d'école, et c'est en passant un escalier au fond qu'on arrive à la même arêne. Il est à noter qu'une version de l'arêne vol de Old arrive par un escalier, ce qui laisse supposer à un moment un échange de map.

( C'est là seul ou on mettra le RDC, ils sont identique pour toutes les autres )

<details>
<summary>Cliquez ici pour en voir plus sur l'aquarium</summary>

| RDC | Étage |
|:------:|:-----:|
| ![Entrée de l'aquarium, avec une statue de locklass](/img/betagold/nihon/aquarium1.png) | ![Etage de l'aquarium, avec un carapuce dans un aquarium](/img/betagold/nihon/aquarium2.png) |

</details>

L'aquarium est un lieu qu'on retrouvera plus tard dans les version Rubis et Saphir

## Route 6

<div style="text-align:center;margin:auto;">

![Une route forestière avec des rochers](/img/betagold/nihon/route6.png)

</div>

La route amène à la zone globale de South (cependant South n'est pas accessible, nécessitant Surf). C'est une route classique, avec quelques rochers.

## Font

<div style="text-align:center;margin:auto;">

![Une petite ville avec des ruines au milieu](/img/betagold/nihon/font.png)

</div>

La ville de Font est une petite ville qui fait la transition entre High-Tech et Birdon. On y accède initialement via un Ferry depuis High-Tech. 

On y trouve les lieux suivants :
- La maison en haut à gauche est un repaire de la Team Rocket
- La maison en bas à droite est un petit laboratoire (sans doute pour étudier les ruines alpha)
- Les ruines alpha

### Intérieurs de Birdon

<details>
<summary>Cliquez ici pour en voir plus sur les ruines alpha</summary>

|  |  | 
|:----------:|:----------:|
| ![L'entrée des ruines alpha](/img/betagold/nihon/alpha1.png) | ![La grande cave des ruines alpha](/img/betagold/nihon/alpha2.png) |

</details>

Les ruines alpha sont composé de deux zone, une avec le puzzle, proche de ceux de la version finale, mais plus grand, et une grande zone ou l'on devrait rencontré les zarbis. L'entrée sera divisée en deux dans la version finale, puisqu'une grotte aménera aux ruines, et plusieurs puzzle débloquerons les zarbis.

<details>
<summary>Cliquez ici pour en voir plus sur le repaire rocket</summary>

<div style="text-align:center;margin:auto;">

![Un repaire avec un poster et une télé](/img/betagold/nihon/fontrocket.png)

</div>

</details>

Le repaire de la team rocket est un petit repaire. Il devait surement y avoir un petit nombre de combat à faire, peut-être pour pouvoir gagner l'accès aux ruines alpha ou à Birdon.

<details>
<summary>Cliquez ici pour en voir plus sur le laboratoire</summary>

<div style="text-align:center;margin:auto;">

![Un petit laboratoire](/img/betagold/nihon/birdonlab.png)

</div>

</details>

## Route 10

<details>
<summary>Cliquez ici pour en voir plus sur la route</summary>


<div style="text-align:center;margin:auto;">

![Petite route avec](/img/betagold/nihon/route10.png)

</div>

</details>

La route 10 est étrangement une route complètement cul de sac, malgré le bras de mer amenant vers South.

## Route 11 et 12

| Route 11 | Route 12 | 
|:----------:|:----------:|
| ![Une route un peu labyrinthique débouchant sur un bras de terre](/img/betagold/nihon/route11.png) | ![Une petite route avec un bras de terre sur l'eau](/img/betagold/nihon/route12.png) |


Ces deux routes amènent vers la ville de Birdon, contenant l'arène suivante.