---
layout: layouts/base-subpage.njk
eleventySubNavigation:
  key: Birdon
  parent: Région de Nihon
  order: 4
---

# Birdon

La ville de Birdon est intéressante parce qu'elle est la première qui change drastiquement par rapport au genre d'environnement qu'on rencontre dans la version finale : toute la zone est désertiques, avec des cactus, des arbres morts, etc.

Le groupement (cette ville est dans le groupe 6 de maps) semble indiquer qu'on arrivait à cette ville après avoir traversé High-Tech puis Font, malgré la présence de la route 4. On peut supposer qu'une partie de la route était bloqué, empêchant l'aspect à Font et faisant faire le détour.

## Route 4

<details>
<summary>Cliquez ici pour en voir plus sur la route 4</summary>

<div style="text-align:center;margin:auto;">

![Une longue route verticale entourée d'arbres morts](/img/betagold/nihon/route4.png)

</div>

</details>

La route 4 relie Birdon à la ville de West.

## Birdon

<div style="text-align:center;margin:auto;">

![Une ville désertique avec un puit au milieu](/img/betagold/nihon/birdon.png)

</div>

La ville de Birdon est une petite ville désertique, utilisant des intérieur de bâtiment à l'ancienne. Une version plus récente de la map inversera le centre pokémon et l'arène, qui comme pour High Tech et les arènes suivantes se trouve à l'étage d'une école.

La ville contient les points d'intérêt suivant :
- L'arène
- Le puits ramoloss
- Une lotterie.

### Intérieurs

<details>
<summary>Cliquez ici pour en voir plus sur le Puit Ramolloss</summary>

|  |  | 
|:----------:|:----------:|
| ![L'entrée du puit](/img/betagold/nihon/puit1.png) | ![La salle principale du puit](/img/betagold/nihon/puit2.png) |

</details>

Le puit ramollos ressemble plus à un puit habituelle, avec une corde pour y accéder, et une grotte au bout. Une entrée similaire sera présente dans HeartGold et SoulSilver. Le puis est bien plus petit.

<details>
<summary>Cliquez ici pour en voir plus sur l'arêne</summary>

<div style="text-align:center;margin:auto;">

![Une arène dans un milieu naturel](/img/betagold/nihon/arene4.png) |

</div>

</details>


Comme l'arène de High-Tech, cette arène se trouve à l'étage d'une école. On peut remarquer des trous dans le sol, mais ils ne reconduisent à priori pas au RDC, à moins que ce n'était tout simplement pas enore codé dans la beta.

<details>
<summary>Cliquez ici pour en voir plus sur la lotterie</summary>

<div style="text-align:center;margin:auto;">

![Une arène dans un milieu naturel](/img/betagold/nihon/birdonshop.png) |

</div>

</details>

Ce vieux magasin en style ancien était la lotterie, qui deviendra le nombre mystère de la Tour Radio.

## Route 13 et 14

<details>
<summary>Cliquez ici pour en voir plus sur les routes 13 et 14</summary>

|  |
|:----------:|
| ![Une route horizontale sans haute herbe avec des arbres morts](/img/betagold/nihon/route13.png) | 
| ![Une route descendant vers le sud](/img/betagold/nihon/route14.png) |

</details>

Les routes 13 et 14 sont deux routes désertique amenant Birdon vers l'Est de la map. Vu leur nom, cela va dans le sens qu'on devait prendre ce chemin après un détour vers High Tech puis Font en passant par des Ferry.

La route 13 n'a aucune haute herbes, et semblait plus être un endroit ou on combattait des dresseurs.