---
layout: layouts/base.njk
eleventyNavigation:
  parent: Autour des jeux
  key: Les challenges
  order: 3
---

# Les challenges Pokémon

Les jeux Pokémon ayant été souvent fait et refait dans tout les sens par de nombreux fans, iels ont inventés de nouvelles manières d'y jouer et de les faire. Des petites règles simple pour pimenter une run à des challenges complets, les choix sont vastes.

Le but de cette page est de présenter un peu les challenges qui existent dans le monde de Pokémon

## Règles simples

Sans être des "challenges" au sens classique du terme, un nombre de règles simple sont souvent utilisées pour rendre le jeu plus difficile, ou retirer certains des aventages qu'on a en tant que joueur⋅euse.

- Devoir jouer en mode set (retire la possibilité de changer de pokémon à chaque KO)
- Limiter le nombre d'utiliser du centre pokémon
- Interdire les objets en combats, voir les objets tenus
- Interdire de dépasser le niveau du maitre d'arêne à venir
- Interdire le partageur d'expérience.
- Interdire les pokémon légendaires
- Interdire la pension

Ces règles peuvent être combiné avec les challenges parfois pour les rendre plus difficile.

## Le Nuzlocke

Le [nuzlocke challenge](https://nuzlockeuniversity.ca/) est le plus connu des challenges pokémon. Le principe le plus connu est simple : si votre pokémon tombe KO, il meurt. Avec un ensemble de règle, le but de ce challenge est de vous mettre dans une situation où vos pokémon sont limités, ce qui rajoute une tension supplémentaire au jeu.

Le challenge se constitue d'un certain nombre de règles principales, de petites clauses optionnelles pour adoucir le challenge, mais aussi de règles pour le pimenter. Une partie des informations présente dans cette page parlant du Nuzlock vien de Nuzlock University

Les règles canoniques sont les suivantes :

- Tout pokémon tombant KO est considéré comme mort, et ne pourra plus jamais être utilisé en combat jusqu'à la fin de la partie
- Le joueur ne peut capturer dans chaque zone (une route, une grotte, etc) que le *premier* pokémon qu'il rencontre. Le pokémon sera perdu si jamais il ne peut pas être capturé pour une raison ou une autre. Une "zone" correspond à un lieu marqué indépendamment sur la carte du jeu.
- Tout vos pokémon doivent avoir un surnom (ce n'est pas utile au challenge, mais ça participe à créer du lien avec les pokémon) 

Pour rajouter de l'aléatoire au premier pokémon, l'identifiant du dresseur est parfois utilisé :
- Si c'est 1-3 le joueur choisis le type plante
- Si c'est 4-6 le joueur choisis le type eau
- Si c'est 7-9 le joueur choisis le type feu
- Si c'est 0 il choisi celui qu'il veut ou tire au hasard

### Clauses optionnelles

A côté des règles principales, des "clauses" sont parfois utilisées en challenge pour adoucir un peu le challenge, et retirer un peu de frustration 

Ces clauses sont les suivantes :

- **La clause des duplicat** : Vous pouvez ignorer un pokémon que vous avez déjà dans votre équipe, si c'est votre première rencontre.
- **La clause des chromatiques** : Vous pouvez capturer un pokémon shiney même si ce n'est pas votre pokémon de zone, et sa rencontre ne comptera pas comme une rencontre de zone. Vous avez eut du bol : enjoy !
- **La cendre sacrée** : Certain⋅e⋅s joueur⋅euse⋅s utilisent la cendre sacrée comme unique moyen de ramener un pokémon mort.

### Variante de Nuzlocke

En plus du nuzlocke de base, un certain nombre de version alternatives existent pour pimenter le nuzlocke, voir créer des expériences totalement différentes. 

- Le **Hardcore Nuzlocke** est une variante qui interdit d'utiliser des objets en combat (hors des pokéballs), et qui interdit de dépasser le niveau du prochain boss, forçant à utiliser beaucoup plus les stratégies disponibles avec ses pokémons.

- Le **Soul Link** est une variante "coopérative", ou chacunes des rencontres de route sont "liée" entre les deux joueur : pour qu'elle soit valide, les deux joueur doivent la capturer. Et si un des pokémon ainsi lié meurt, l'autre aussi. Souvent, quand l'un des deux est mis au PC, l'autre doit l'être aussi.

- Le **Monolocke** est un nuzlocke avec une limite de type : vous ne pouvez capturer que les pokémon d'un même type. Les doubles types comptent, et les pokémons devenant d'un type aussi. *Parfois*, ce challenge adoucis les règles de capture.

Il est aussi possible de combiner le nuzlocke avec différents type de limitation de pokémon : les échanges in-game, les pokémon cadeaux, etc.

[Pleins d'autres nuzlocke](https://nuzlockeuniversity.ca/nuzlocke-variants/) sont disponible sur Nuzlocke University (peut-être qu'un jour je ferais une grosse page de traduction de ces nuzlockes)

## L'Avatar Mode

L'Avatar Mode est un challenge Pokémon basé sur Avatar : Le Dernier Maitre de l'Air, créé par [
funnyman2416](https://www.deviantart.com/funnyman2416) sur deviantArt. Les règles sont assez simple, et ont été fait [originellement](https://www.deviantart.com/funnyman2416/art/Pokemon-Avatar-Mode-337506937) pour les générations datant d'avant le type fée (avec une [version modifiée faite pour avec le type fée](https://www.deviantart.com/funnyman2416/art/Pokemon-Avatar-Mode-XY-404942661)). Tout les crédits vont à funnyman2416 pour ce concept cool, le but ici de cette page est de traduire ce qu'il avait fait, et de le partager. N'hésitez pas à consulter les liens aussi.

Se basant sur Avatar, le dernier maitre de l'air, le principe du challenge est basée sur les quatre nation de la série : la nation de l'eau, du feu, de l'air et de la terre.

Les règles sont les suivantes :

- Choississez l'une des quatre nations.
- Vous n'avez le droit de capturer que des pokémon ayant un des types suivants
  - **Nation de l'air** : Vol, Psy, Spectre, Bug
  - **Nation de l'eau** : Eau, Glace, Plante, Poison
  - **Nation du feu** : Feu, Electrique, Ténèbre, Dragon
  - **Nation de la terre** : Sol, Pierre, Acier, Combat
  - Le type normal n'est assigné à aucune nation
- Vous pouvez capturer un pokémon s'il gagnera un type de la nation (par exemple, un membre de la nation de l'air peut choisir salamèche, qui deviendra un dracofeu), mais vous devrez le faire évoluer, et pouvoir le faire évoluer.
- Cependant, si l'évolution rend un de vos pokémon incompatible avec votre nation, vous ne pouvez plus l'utiliser.
- Les pokémon normaux peuvent être utilisées par toute les nations, mais s'ils ont un double type, ils ne peuvent être utilisé que par les membres de la nation ayant le deuxième type (par exemple, Roucool ne peut être utilsié que par la nation de l'air)

Vous n'y êtes pas l'avatar, et devrez utiliser à votre aventage les limites de ces règles pour finir le challenge. Certaines nations sont plus difficile que d'autres, surtout en génération 1 our les nations feu et terre perdent un type chacune.

### Avec le type fée

Deux choses sont modifiée avec le type fée :
- Les pokémon fée remplace ceux insecte dans la nation de l'air
- Tout les pokémon insecte peuvent être utilisé par toute les nations

### Version ou vous êtes l'avatar

L'utilisateur deviantart [ryuseitheferret](https://ryuseitheferret.deviantart.com/) à proposé une version alternative où le joueur est l'avatar. Le principe est d'utiliser l'ordre de réincarnation des avatar dans la série (Air, Eau, Terre, Feu, Air, Eau, Terre, Feu…). Les règles additionnellent s'ajoutent :
- Commencé avec juste votre nation.
- Après le deuxième badge, rajoutez la nation suivante dans l'ordre
- Après le cinquième badge, rajoutez encore celle suivante
- Après le septième badge, vous pouvez utiliser tout vos pokémon.

Je proposerais une petite variante du coup pour cette version de l'avatar mode, pour le rendre moins "à la fin on peut tout faire" et garde le fait de devoir se débrouiller :
- Reprendre la règle du "1 pokémon par zone"
- Faire que le joueur ne puisse capturer que des pokémon de l'élément récupéré le plus récemment.

## Restriction d'équipe

L'un des challenges Pokémon les plus populaire, notamment popularisé par le youtubeur [MahDryBread](https://invidious.fdn.fr/channel/UCK-QfN_fautoBhHm2C4uECQ) : vous prenez juste un pokémon, et tentez de faire tout le jeu avec. Pour cela, il est possible d'utiliser des randomizeur pour forcer le premier pokémon, et faire ensuite tout le jeu avec.

Pour rendre ce challenge plus difficile, il est possible d'ajouter aussi la règle citée plus haut du "pas d'objet dans les combats", même si pour certain cela rend le challenge impossible (genre Magikarp).

Il est aussi possible (et ça a souvent été fait par des youtubeurs) d'utiliser d'autres types de restrictions pour le challenge, en terme de pokémon obtenu :
- N'utiliser que des pokémon Bébés
- N'utiliser que les pokémons provenant d'échange
- N'utiliser que les pokémon "cadeau" du jeu
- N'utiliser que les pokémon fossile
- N'utiliser que des pokémon lié à une thématique (animaux de compagnie, animaux de ferme, etc)
- Utiliser des hacks qui modifient les pokémon disponibles
- N'utiliser qu'un duo de pokémon (genre les deux Nidoran)
- Utiliser et construire l'équipe d'un PNJ du jeu (rival, maitre d'arêne, etc)
- N'utiliser que des pokémon de la zone autour pour combattre (ou, autre dérivé, devoir mettre toute son équipe au PC après chaque arène vaincue)

## Le Minimal Battle

Le Minimum Battle challenge est un type de challenge souvent fait par la chaine youtube [RBY Challenge](https://www.youtube.com/@RBYPokemonChallenges) qui consiste à faire un challenge pokémon en faisant le moins de combat possible, et donc en se limitant fortement dans l'expérience que l'on peut obtenir.

Le principe du challenge est simple : dès qu'on peut esquiver un combat, on doit l'esquiver :
- S'il y a un moyen légitime d'éviter de faire un combat (passer autour, etc), alors on l'évite
- On ne peut pas combattre les pokémon sauvages

RBY Challenge fait souvent ces challenges en one-pokémon, et essaie d'éviter les CT puissante, voir les CT tout court.

La variante de base est sans glitch ou exploit, mais il est possible pour la première génération de rendre le challenge encore plus difficile (que RBY Challenge appelle la "Super Minimal Battle") en s'autorisant les exploits qui permettent de passer des dresseurs, tel que :
- Echanger un pokémon avec des CT pour éviter d'avoir à faire les 
- Echanger via Pokémon Stadium l'eau minérale pour pouvoir accéder tout de suite à Safrania et donc ne pas avoir de combat dans les grottes
- Utiliser la poképoupée pour pouvoir esquiver tout le premier affrontement de la Team Rocket.

Il est possible de faire des choses similaires dans la seconde génération, mais cela permettra moins de couper des grosses sections du jeu.

## Autres Challenges

D'autres types de challenges ont été fait pour jouer aux jeux de manière différente, mais sans forcément modifier les pokémon :

- N'utiliser qu'un type d'attaque (CT/CS, etc) voir qu'une attaque
- Ne pas autoriser les pokémon à apprendre de nouvelles attaques
- Interdire les centre pokémon et les soins "gratuit"
- Empêcher de gagner de l'expérience (via des hacks)