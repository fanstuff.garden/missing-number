---
layout: layouts/base.njk
eleventyNavigation:
  parent: Autour des jeux
  key: Générations 1 & 2
  order: 0
---

# Générations 1 et 2 de Pokémon

Les deux premières générations de pokémon sont les jeux composé des jeux sortis sur gameboy et gameboy color entre 96 et 2001 (ainsi que quelques jeux sur Nintendo 64, et quelques produits dérivés). Les jeux principaux sont composé de pokémon Rouge/Bleu/Vert/Jaune pour la première et de Pokémon Or/Argent/Crystal pour la seconde. 

Sur de nombreux points, ces deux générations sont très liés, et ce dès le début du projet de la seconde génération. C'est à partir de la troisième que chaque génération deviendra plus séparée, même si souvent quelques générations proches sont liées par des mécaniques ou styles graphiques restant sur plusieurs générations.

## Génération 1

La première génération de Pokémon est la première série de jeu sortie, en 1996 au japon, en 1998 aux USA et en 1999 en Europe. Ces jeux sont composé de Pokémon Vert/Rouge, Bleu et Jaune au japon, et de seulement Pokémon Bleu/Rouge et Jaune en occident. Cette version a eut un temps de développement particulièrement long, et est connu pour la quantité de bugs qu'il reste dans le jeu final. Cela n'a pas empêché les jeux d'être un succès retentissant et de lancer la franchise.

Sa troisième version (occidentale), Jaune, sortira en 1998 au Japon , 1999 aux USA/Australie et 2000 en Europe, et sera très proche de l'anime, ajoutant un Pikachu suivant le héros et Jessie et James dans le scénario.

Cette génération à lancé la Pokémania, et a eut le droit à deux remake :
- Pokémon Rouge Feu / Vert Feuille (réutilisant les couleurs originelles), sortie en 2004
- Pokémon Let's Go Pikachu / Evoli (plus des remakes de Jaune), sortie en 2018

### Version japonaises vs occidentales

Les deux premiers jeux pokémon réalisé sont Pokémon Rouge et Vert au japon. Ces jeux, sorti en 1996, ont eut un franc succès. Une version améliorée/modifiée des jeux, Pokémon Bleu, est sortie la même année (exclusive au magazine CoroCoro Comics) pour fêter le million de vente des jeux originel.

Ce jeu contenait les modifications suivants par rapport aux originaux :
- Correction de bugs, amélioration de sprites, et améliorations sonores
- Modification complète de la carte du donjon inconnu
- Modification des rencontres de pokémon.

Cette version est du coup une révision mineur de Pokémon Rouge et Vert, une partie de son contenu serait un patch correctif aujourd'hui. Le jeu sera ressorti pour fêter les quatre million en 1997, et serait mis en vente en 1999. C'est cette révision qui servit de base niveau code pour créer pokémon Rouge et Bleu tel que nous les connaissons aujourd'hui, mais avec les données de rencontre de Pokémon de Rouge et Vert originaux, faisant de nos version des version "hybrides".

Parmis les version japonaise, "Pokémon Vert" (cru comme étant la version exclusive au japon, même si du coup toute le sont d'une manière), a souvent excité les rumeurs en occidents, et beaucoup le légendent tournent autour.

## Génération 2

Commencé sous la forme de "Pokémon 2", la seconde génération est sortie au coeur de la Pokémania, en 1999 au Japon, 2000 aux USA/Australie et 2001 en Europe. Contrairement aux générations suivantes, elles peuvent être vu plus comme une "mise à jour" de la version précédante, ayant beaucoup de pokémon lié aux génération précédantes. Elle sera l'une des rares générations ajoutant de nouveaux types (ténèbre et acier), et rajoutera toute la mécanique des oeufs et des objets à porter. Graphiquement, elle apportera le support Gameboy Color.

Sa troisième version (occidentale), Crystal, sortira en 2000 au Japon et 2001 en Europe, et apportera une mini-intrigue autour de Suicine ainsi que la Tour de Combat.
