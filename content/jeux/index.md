---
layout: layouts/base.njk
eleventyNavigation:
  key: Autour des jeux
  order: 1
---

# Autour des jeux

Ce site parle principalement des jeux de la série principale de pokémons sortis sur gameboy et gameboy color. Le but de cette catégorie est de parler un peu des sujets "génériques" autour des jeux, ainsi qu'une présentation rapide.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>