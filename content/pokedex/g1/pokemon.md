---
pagination:
    data: pokered
    size: 1
    alias: pokemon
permalink: "pokedex/g1/{{ pokemon.namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}/"
layout: layouts/pokedex.njk
---

<div style="display:flex;justify-content:space-between;">
  <div>
    {%- if pokemon.baseStats.dex > 1 -%}<a href="/pokedex/g1/{{ pokered[pokemon.baseStats.dex - 2].namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}">← {{ pokered[pokemon.baseStats.dex - 2].namefr }}</a> {%- endif -%}
  </div>

  <div>
    {%- if pokemon.baseStats.dex < 151 -%}<a href="/pokedex/g1/{{ pokered[pokemon.baseStats.dex].namefr | replace('♀', 'F') | replace('♂', 'M') | slugify }}">{{ pokered[pokemon.baseStats.dex].namefr }} →</a> {%- endif -%}
  </div>
</div>

# {{ pokemon.namefr }} 

{{ pokemon.namefr }} ( {{ pokemon.nameen }} en anglais ; {{ pokemon.namejp }} en japonais ) est le pokémon n°{{ pokemon.baseStats.dex }} du pokedex de Kanto, de type {{ pokemon.baseStats.types[0] }}{% if pokemon.baseStats.types[0] !== pokemon.baseStats.types[1] %} et {{ pokemon.baseStats.types[1] }} {%- endif -%}. {% if pokemon.baseStats.dex <= 151 -%} Il est apparu dans la  première génération. {%- endif %}

## Généralité

### Statistiques

|   | PV | ATK | DEF | SPD | SPE | 
|:-:|:--:|:---:|:---:|:---:|:---:|
| **Base :** | {{ pokemon.baseStats.stats.hp }} |  {{ pokemon.baseStats.stats.atk }} |  {{ pokemon.baseStats.stats.def }} |  {{ pokemon.baseStats.stats.spd }} |  {{ pokemon.baseStats.stats.spe }} |

### Évolutions

<table>
  <thead>
    <tr>
      <th>Évolue en</th>
      <th>via</th>
    </tr>
  </thead>

  <tbody>
    {%- for evolution in pokemon.evoMoves.evolutionItems -%}
        <tr>
            <td><a href="/pokedex/g1/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td>Avec un(e) {{ evolution.item }} </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionLevel -%}
        <tr>
            <td><a href="/pokedex/g1/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Au niveau {{ evolution.level }} </td>
        </tr>
    {%- endfor -%}
    {%- for evolution in pokemon.evoMoves.evolutionTrade -%}
        <tr>
            <td><a href="/pokedex/g1/{{ evolution.pokemon | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ evolution.pokemon }} </td>
            <td> Par échange </td>
        </tr>
    {%- endfor -%}
    {%- if (pokemon.evoMoves.nbrEvolution <= 0 ) -%}
        <tr>
            <td colspan="2"> <em>Stade Final</em> </td>
        </tr>
    {%- endif -%}
  </tbody>
</table>

## Emplacements

<table class="pokedex">
  <thead>
    <tr>
      <th></th>
      <th>Emplacement</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <th>Rouge </th>
      <td>
        {%- for encounterList in pokemon.encounters.red -%}
          {% if encounterList.prefix !== "" %}{{ encounterList.prefix }} : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
    <tr>
      <th>Bleu </th>
      <td>
        {%- for encounterList in pokemon.encounters.blue -%}
          {% if encounterList.prefix !== "" %}{{ encounterList.prefix }} : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
    <tr>
      <th>Jaune </th>
      <td>
        {%- for encounterList in pokemon.encounters.yellow -%}
          {% if encounterList.prefix !== "" %}{{ encounterList.prefix }} : {% endif -%} {{ encounterList.list | join(" ; ") }}<br />
        {%- endfor -%}
      </td>
    </tr>
  </tbody>
</table>

## Attaques

<div class="grid" style="display:flex;">
    <div>

    <table>
        <thead>
            <tr>
            <th>Attaques de base</th>
            </tr>
        </thead>

        <tbody>
            {%- for move in pokemon.baseStats.base_move -%}
                <tr>
                    <td>{{ move }} </td>
                </tr>
            {%- endfor -%}
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
            <th>Niv</th>
            <th>Attaque</th>
            </tr>
        </thead>

        <tbody>
            {%- for move in pokemon.evoMoves.moves -%}
                <tr>
                    <td> {{ move.level }} </td>
                    <td>{{ move.move }} </td>
                </tr>
            {%- endfor -%}
            {%- if pokemon.evoMoves.nbrMoves == 0 -%}
                <tr>
                    <td colspan="2"> <em>Aucune attaque apprise par niveau</em> </td>
                </tr>
            {%- endif -%}
        </tbody>
    </table>

    *(Note : certaines attaques peuvent se retrouver en double dans les tableaux.)*

    </div>
    <div>

    <table>
        <thead>
            <tr>
            <th>Par CT / CS</th>
            </tr>
        </thead>

        <tbody>
            {%- for move in pokemon.baseStats.tmhm -%}
                <tr>
                    <td>{{ move }} </td>
                </tr>
            {%- endfor -%}
            {%- if pokemon.baseStats.nbrTmHm == 0 -%}
                <tr>
                    <td> <em>Aucune attaque apprise par CT/CS</em> </td>
                </tr>
            {%- endif -%}
        </tbody>
    </table>
    </div>
</div>