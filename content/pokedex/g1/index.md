---
layout: layouts/pokelist.njk
eleventyNavigation:
  parent: Autour des jeux
  key: Pokédex (RBY)
  order: 1
---

# Pokédex Génération 1

Cette page présente les pokémon possible à obtenir dans la première génération de Pokémon (Rouge, Bleu, Jaune). 

Les données de ce pokedex sont récupéré directement des dessamblages pokered et pokeyellow (ainsi que la traduction pokered-fr) et transformée via un script en un format de donnée pour que le site puisse les lires.

Cliquez sur le nom d'un pokémon pour accéder à ses informations.

## Liste des pokémons

<table class="pokedex">
  <thead>
    <tr>
      <th>Dex</th>
      <th>ID</th>
      <th>Nom</th>
      <th>Types</th>
      <th>PV</th>
      <th>Atk</th>
      <th>Def</th>
      <th>Spd</th>
      <th>Spe</th>
    </tr>
  </thead>

  <tbody>
  {%- for pokemon in pokered -%}
    <tr>
      <td>{{ pokemon.baseStats.dex }} </td>
      <td>{{ pokemon.internalId }} </td>
      <td> <a href="/pokedex/g1/{{ pokemon.namefr | replace("♀", "F") | replace("♂", "M") | slugify }}">{{ pokemon.namefr }}</a> </td>
      <td> {{ pokemon.baseStats.types[0] }}{%- if pokemon.baseStats.types[0] !== pokemon.baseStats.types[1] -%}, {{ pokemon.baseStats.types[1] }} {%- endif -%} </td>
      <td>{{ pokemon.baseStats.stats.hp }}</td>
      <td>{{ pokemon.baseStats.stats.atk }}</td>
      <td>{{ pokemon.baseStats.stats.def }}</td>
      <td>{{ pokemon.baseStats.stats.spd }}</td>
      <td>{{ pokemon.baseStats.stats.spe }}</td>
    </tr>
  {%- endfor -%}
  <tbody>
</table>

## Crédits

- Données venant de pokered, pokeyellow
- Traductions récupérés via pokered-fr
- Pokedex inspiré de celui du Monde des Pokémon de Pix@lgo