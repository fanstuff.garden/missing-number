---
layout: layouts/base.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

# Bienvenue sur Missing-Number

Missing-Number est un petit site Pokémon orienté 1G / 2G, qui explore les différents aspects de la série à cette époque, et parle aussi bien des jeux que des rumeurs et légendes urbaines autour. Vous pouvez y trouver quelques sujets niches, ainsi que des pokédex pour les version 1G/2G. Il est proche dans l'esprit de [Blue Moon Falls](https://bluemoonfalls.com/), un autre site parlant du sujet (en anglais).

L'idée de Missing Number est du coup d'être un peu un espace de découverte, où vous pourrez en découvrir plus sur ces deux jeux rétro à votre guise. [En savoir plus](/about).

## Updates

- **2024/04/10** - Ouverture du site

## Todo

- Ajouter les jeux secondaires de ces générations
- Pokédex G2 béta (va compléter la page existante)
- Creepypasta et légendes urbaines du genre
- Infos sur le TGC à l'époque 1G/2G
- Ajouter un GlitchDex des attaques et des pokémons
- Ajouter des trucs et astuces
- Créer "l'AU" Indigo Foundation, qui mélange pleins de ces éléments présentés ici sous forme d'un univers alternatif
- Ajouter un annuaire des sites web

## Sources et crédits

Ce site faisant en grande partie du regroupement et de la traduction d'informations, de nombreuses informations sont reprise de divers wiki pokémon anglophones et traduits. Les sources sont disponibles également de manière plus précise (ainsi que les artistes quand des artworks de fans sont utilisés) dans chaque page du site quand c'est pertinent.

- Les décompilations du [projet pret](https://github.com/pret/) - Les informations internes des jeux, ainsi que de nombreux sprites
- [The Cutting Room Floor](https://tcrf.net/) - Toutes les informations à propos des béta et éléments de développements des jeux (sous licence CC-BY)
- [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page) (en) et [Pokepedia](https://www.pokepedia.fr/) (fr) - Wiki pokémon de références, où j'ai récupéré de nombreuses informations plus génériques
- [Tyradex](https://tyradex.vercel.app/) - API pokedex francophone, j'y ai récupéré quelques informations qui me manquait des décompilations (genre des tailles/poids)