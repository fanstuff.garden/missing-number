---
layout: layouts/base.njk
---

# Welcome to the Indigo Foundation

For years, mysterious event have plagued the region of Kanto and Johto. Disappearence, "surnatural" phenomenon, mysterious creatures. These instance are rare, but common enough to cause a resurgence of superstition and old belief, and to cause real disturbance in the life of hundred of thousand of inhabitant of our regions.

But we don't believe in mere superstition and old tradition. Here at the Indigo Foundation, we're determined to studies the phenomenon, to be able to understand what is happening, and solve the problem once and for all.

We're here to understand, but more importantly, protect our people. But we also understand… what if some had an use ?

## What is this site ?

Welcome to {{ metadata.title }} !

This website is a shrine-inspired olschool Pokemon Website, that aim to create stories and SCP-like pages inspired by the old rumors, glitch and creepypasta from the first and second generation de Pokemon. This is a kind of "alternate reality game" of the two first generation of Pokemon, where different creation and apparition will be described, with some substories.

It'll be *mostly* focused on Pokemon Red/Blue/Yellow and Pokemon Gold/Silver/Crystal, but some reference will be made to other region when it give interesting ideas to handle some of the "mysteries". If I add cosmology stuff, it'll also contain elements from several other later games (certainly up to 4G). But the "core" of the content will be 1G/2G stuffs.

This focus isn't to say that the next generation are bad, or wouldn't be interesting to handle : is that I'm only one person, and that I have already a lot of large project that I shouldn't be doing lol.

## Crédits

- Raticlaw, Sapusaur, Charcolt, Rainer, Nidogod, Nidogodess sprites by [Chanchimi](https://www.deviantart.com/chanchimi).
- Doomsday sprites comes from the early days of internet, seemingly from [Golduck's Pokemart](https://web.archive.org/web/20021006094331/http://www.angelfire.com/ny2/pokemart/pokemart.html)

## Creatures

We all know the 250 pokemon that our pokedex comprise (and some of us - especially at the foundation - even know about the legendary Mew). But some other creatures have appeared during the last few years, and can cause havok, and even sometimes death and desolation. We don't have the exact species they are, and don't think they're all the *same* kind of creatures. We're also really reluctant to call them "Pokemon", even if some forms of hybrids exists.

The corrupted trainer aren't part of this categories.

## Classification

We have for the moment the following kind of creature :

- Pokegods - Overpowered pokemon that were created from the mysterious *Mist Stones* stolen from the *Mysterious Man from Kalos* after his death.
- Undead - "Pokemon" that came back from the dead. They should not be confused with gosth-type pokemon.
- Glitches - Space-time anomalies that seems to be able to cause a lot of dangerous events.

## Origins

The different creatures don't share a single origins. Some of them were man-made, like the Pokegods (even if they seems to be coming from some sort of mysterious natural phenomenon, certainly related to the highly volatile phenotypes and genotypes of pokemon), some of them seems just to… naturally occurs.

Investigation are still present to find a single cause. For the moment, we have established a classification of these creatures to establish their cause of appearence :

- Homonculuses - Creatures that have been made by the man from scratch. Most of them seems to come from a mysterious *Pokemon Factory* handled by Team Rocket.
- Mutants - Creatures that have been muted by the environnement
- Abnomalies - Creatures that seems to have appeared from nowhere.

## Unique being

Some creature have evolved to becoming fearful being due to specific event that created just *one* creature and not some kind of species. These creature have their own page to describe them.