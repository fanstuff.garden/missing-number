---
layout: layouts/base.njk
---

# Evolved Pokegods

> **Note:** All the sprites used (and thus their design) in this page are the creation of [Chanchimi](https://www.deviantart.com/chanchimi). You can click on the sprite to access the source of the sprite.

Pokegods are mysterious being that have been evolved from pokemon and made extremely powerfull. 

It's one of the most-documented and more well-known phenomenon, and seems to derive from a *Mysterious Man* coming from Kalos. He was able to make temporarely extremely powerful creatures due to some shard of rock coming from a set of *powerful deadly ruins* from Kalos he said to had created. He was killed by a thief that wanted all this power for himself, and these most of the shards were lost to time. They were collected several years ago by the dangerous crime syndicate called the *Team Rocket* and are now known as the *Mist Stones*.

The Team Rocket seems to have been able to use the Mist Stone to create *permanent* evolution of sorts, at the mysterious *Pokemon Factory*. Pokegods are forbidden, but we can see some being used in battle sometimes. They're highly dangerous due to their sheer power.

It's important to stress to anybody reading this documents : **Pokégods aren't gods**. They're simply highly powerfull enhanced pokemons. The name have been used to refer to their power, not to any divine nature. Any article done to the general public that doesn't remind that will be severaly punished. The last thing we need is a panic and more sects forming up.

## Raticlaw

Raticlaw is known as the potentiel first-ever created pokegod. It's the mist-stone induced evolution of Raticate, a pretty common pokemon. it's also one of the most common pokegods, and we've already seen some trainer using them.

<div style="text-align:center;">

[![A sprite of Raticlaw, an "dangerous-looking" Raticate](/img/pokegods/raticlaw.png)](https://www.deviantart.com/chanchimi/art/Raticlaw-Pokegod-171498746)

</div>

It's orgin pre-date even the Rocket experimentations on Mist Stone, because he was made in the now-destroyed *Pokemon Mansion* in Cinnabar Island, to test the evolutive ability of the Mist Stone. Raticate was choosen for their relative presence all over Kanto and Johto, and as they thought it was be easy to control its potential evolution.

Sadly, this pokegod inherit the volatile behavior of other pokegods. Raticlaw claws and teeths are stronger than many substence, natural of artifical, and they're highly agile and fast. The created raticlaw escaped the mansions and run away, taking down several scientist in the passage.

## Sapasaur, Charcolt and Rainer

These three advanced evolutions of the Venusaur, Charizard and Blastoise are powerful and dangerous pokemon. They abilities make them strong destructive creature that can take on a large amount of weaker pokemon at the same time. The rarity of the three pokemon used to create these pokemon make them less of a threat, but even a few of them could lead do disastrous consequences, especially with the difficulty to get counter-measure.


<div style="text-align:center;">

[![A sprite of Sapusaur, an "dangerous-looking" Venusaur](/img/pokegods/sapusaur.png)](https://www.deviantart.com/chanchimi/art/Sapusaur-Pokegod-171174672)
[![A sprite of Charcolt, an "dangerous-looking" Charizard](/img/pokegods/charcolt.png)](https://www.deviantart.com/chanchimi/art/Charcolt-Pokegod-171123299)
[![A sprite of Rainer, an "dangerous-looking" Blastoise](/img/pokegods/rainer.png)](https://www.deviantart.com/chanchimi/art/Rainer-Pokegod-167831493)

</div>


**Sapusaur** have an extra-corrosive sap, and is also known for the high toxicity of its polen. We say that only plants and fungus can survive in the forest a specimen escaped into.

**Charcolt** flame can get to extreme heat, enough for simply his presence causing huge wild fire, especially as he can fly to hypersonic speeds. The only known specimen live in the center of a perpetually-erupting volcano now.

**Rainer** is able to create huge storms that can engulf whole cities and destroy everything in their path. One escaped Rainer is said to live in the center of a never-ending cyclone.

Some people have started to rever those three pokegods as some sort of gods of nature and elements. The Foundation recommand extreme care when talking about these creature, to avoid a resurgence of sects build around pokegods.

## Nidogod, Nidogoddess

These pokegods have been created by radiating power of the mist stone into a Nidoking and a Nidoqueen. This have created a certain number of genetical mutation that have affected how they produce toxins and influence other member of their evolution line.

<div style="text-align:center;">

[![A sprite of Nidogod, an "dangerous-looking" Nidoking](/img/pokegods/nidogod.png)](https://www.deviantart.com/chanchimi/art/Nidogod-Pokegod-191047470)
[![A sprite of Nidogodess, an "dangerous-looking" Nidoqueen](/img/pokegods/nidogodess.png)](https://www.deviantart.com/chanchimi/art/Nidogodess-Pokegod-171224375)

</div>

The first effect of the mutation is that they've become even more durable and powerful, but also way more savage and even primal. They're moved entirely by instict, and by a need of protecting other member of their evolution line. It can make them extremely dangerous. Especially combined with how their toxin became even more powerful, being able to drain the life of even poison-type pokemons.

They also have some form of hivemind-type control over their evolution line. It's not based on mind-control or any psychic hability, but on toxins that acts like pheromons on other nidoran-evolved creatures.

## Flareth

Flareth is one the numerous tries to make a evolution of eevee evolve, and one of the only successfull attempt.

<div style="text-align:center;">

[![A sprite of Flareth, an "dangerous-looking" Flareon](/img/pokegods/flareth.png)](https://www.deviantart.com/chanchimi/art/Flareth-Pokegod-171468888)

</div>

The internal temperature of this pokemon is enough to melt rocks and metals, and it's like every pokegod an hostile and wild pokemon. One of the few difference between many other pokegods and this one is that the few specialist of wild pokemon behavior of the foundation seems to believe that flareth is more scared than full of rage.

With proper pokemon trainers, it might be a valuable asset into fighting the experiments of Team Rocket.

## Dreamaster

Dremaster is a Mist-induced evolution of Hypno. The power of the stone have awakened its psychic potential to new never-seen before potential, and created a pokegod with strong extra-sensory powers.

<div style="text-align:center;">

[![A sprite of Dreamaster, an "dangerous-looking" Hypno](/img/pokegods/dreamaster.png)](https://www.deviantart.com/chanchimi/art/Dreamaster-Pokegod-171347123)

</div>

This pokemon have the power to generate dream and nightmares at large scale, depending on its current mood. The only known specimen have trapped an entire city in a nightmare for one weeks before being stopped. Sadly, our teams weren't able to capture it.

A sect have been formed around this pokegod, the sect of the *Dream Eaters*, a group of medium that eriged this pokegod to the rank of a divinity that is the sole source of nightmare and dreams in every human and pokemon alike. Members of these teams have attacked our agent when we stopped the wild Dreamaster.

A special team should be dispatched to handle the situation.

## Spooky

Spooky is a Mist-induced evolution of Gengar. The power of the stone have amplified the frightening extra-sensory power of Gengar, making it a creature of pure fear.

<div style="text-align:center;">

[![A sprite of Spooky, an "dangerous-looking" Gengar](/img/pokegods/spooky.png)](https://www.deviantart.com/chanchimi/art/Spooky-Pokegod-305148215)

</div>

THis pokegod is able to influence the thought of everybody around it, turning them into fears. Spooky can recreate the worst and most debilitating fears of anybody, paralysing them while it devour their life energy slowly. This pokemon also have the ability to amplify the negative thought of gosth around them, and is is one of the few creature that isn't afraid of Lavender Town gosths.