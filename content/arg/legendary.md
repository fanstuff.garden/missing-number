---
layout: layouts/base.njk
---

# Legendary Pokegods

Some other being called "pokegods" are other kind of being that have been created "from scratch" by radiations of the mist stones

## Doomsday

Doomsday is a mysterious gosth-type pokegods that appear in place like cimetaries. It looks like some sort of insects, and feeds on dark thought and memories.

<div style="text-align:center;">

[![A sprite of Doomsday, an weird-looking bug-like pokemon](/img/pokegods/doomsday.png)](https://web.archive.org/web/20100326102302/http://www.blue-reflections.net/ragecandybar/projects/pokegods/)

</div>

Some rumors says that it's a lonely soul, that walked back into the living realm to get revenche on those who created its death. This explaination is very popular to some people that try to see a more "occult" aspect of the pokegods and other phenomenon. The biggest criticism of its theory is that there have been one documented case of "vengeful spirit" in the last few years, the *Marowak Gosth*, but it didn't take any form similar to the one of Doomsday.

In scientific circles, the theory is more that its a concentrate of the emotion of sorrow, grief and rage that can happens in the place where the dead rests.

## Chrona Mew

The *Chrona Mew* is a rumored mist-corrupted form of Mew, the mysterious pokemon. There is for the moment no proof of one existing, but all agent should be ready in case one have been made.

<div style="text-align:center;">

![A sprite of the "Chrona Mew", here just a blue mew](/img/pokegods/chronamew.png)

</div>

This project differed from the Mewtwo project in that its aim was to not create a "newer version of Mewtwo", but to try to create an equivalent of what would be a mist-induced evolution of Mew. Not much is known about what would be the particularities of the Chrona Mew. According to rumors, this mew would have an even bigger capacity of Mew to get powers from other pokémon, being able to change their type and copy other pokemon powers.

## Mewthree 

Mewthree is an advanced and revised version of Mewtwo, handled entirely by the Team Rocket without any input from its original creator, Mr Fuji. The Pokemon aim to be the most powerful pokegod, as an way for Team Rocket to retaliate against powerful trainers that defeated them. Mewthree is the consecration of DNA technology, using the full potential of Berserk Gene to raise even more the power of a mewtwo. He is even less controllable that the original, and his high-power psychic pokemon are destructive enough for it to be a threat to themself too.



