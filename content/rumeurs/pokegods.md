---
layout: layouts/base.njk
eleventyNavigation:
  key: Les pokédieux
  parent: Rumeurs
  order: 1
---

# Les pokédieux

Les pokédieux (*pokégods* en anglais) étaient une rumeur persistante qu'il y aurait plus de pokémon que le pokedex le laisserait entendre. Cette légende provient fortement de Mew, qui était déjà un pokémon mythique légendaire en plus des 150 existant déjà, ainsi que des premières images des pokémons de la deuxième génération, qui étaient déjà en développement lors de l'arrivée de la première génération en occident, et étant apparus dans l'anime parfois très tôt (Ho-Oh et Togepi notamment).

La plupars des pokédieux étaient (logiquement à leur noms) souvent considéré comme quasiment invincible, et étaient des pokémons vu comme "caché" et "bonus". De nombreux codes et "triches" étaient présent dans des sites sur comment les obtenirs, souvent avec des méthodes d'obtentions proche de l'impossibilité, demandant souvent des heures de temps de jeu (vaincre la ligue des centaines de fois avec certains pokémon, etc). Sans doute que le but était aussi de faire en sorte que si le code loupait, on disait que la personne l'avait mal fait.

## Évolution de pokémons

Le plus présent dans les types de pokémon étaient des évolutions encore plus puissante que celles finale, souvent à l'aide d'un objet mystérieux, la **Mist Stone**, et elles ont d'une certaine manière eut une forme d'existence sous la forme des *Méga Évolution* les 6e et 7e générations. Cependant, les Pokédieux étaient supposé être des évoutions permanente, là ou les méga étant juste temporaire.

### Evolutions des starters

**Sapasaur**, **Charcolt** et **Rainer** sont sans doute les plus grande démonstration des cas d'évolution de starts post-finale, puisqu'ils sont celles des trois starters : Florizarre, Dracaufau et Tortank. Ils étaient supposé être encore plus puissant que les évolutions finale, et possible d'avoir qu'avec des techniques secretes. Leur descriptions en faisaient souvent des pokémon aux conséquences cataclysmiques.

<div style="text-align:center;">

[![Un fan-sprite de Sapusaur, un Florizarre avec un air dangereux](/img/pokegods/sapusaur.png)](https://www.deviantart.com/chanchimi/art/Sapusaur-Pokegod-171174672)
[![Un fan-sprite de Charcolt, un Dracaufeu avec un air dangereux](/img/pokegods/charcolt.png)](https://www.deviantart.com/chanchimi/art/Charcolt-Pokegod-171123299)
[![Un fan-sprite de Rainer, un Tortank avec un air dangereux](/img/pokegods/rainer.png)](https://www.deviantart.com/chanchimi/art/Rainer-Pokegod-167831493)

*Vue d'artiste des Pokédieux des starters, par [Chanchimi](https://www.deviantart.com/chanchimi/)*

</div>

Ces starters auront tous des méga évolution dès la 6e génération, Dracaufeu fera même parti des rares pokémon ayant le droit à *deux* méga-évolution. Ces méga (comme tout les méga) rappelle beaucoup l'idée des Pokédieux, étant souvent parmis les créatures les plus puissantes de l'univers de Pokémon, ce qui participera beaucoup à nous les rappeller, en amplifiant les traits et la puissance des starters.

<div style="text-align:center;"> 

![Un mega-florizarre, un Florizarre avec la plante largement augmentée](/img/mega/megaflorizarre.png) ![Un mega-dracaufeu x, un Dracaufeu noir avec une immense flamme bleu](/img/mega/megadracaufeu1.png) ![Un mega-dracaufeu y, un Dracaufeu avec un air plus svelte et rapide](/img/mega/megadracaufeu2.png) ![Un mega-tortank, un Tortank avec des canons beaucoup plus présent](/img/mega/megatortank.png) 

</div>

Ces mega ont tous un ajout de 100 points de statistiques en tout, passant de ~530 à ~630. Certains des starters ont quelques pokémons glitchés de la même espèce d'eux, ce qui participait aussi à leur donner 

### Evolutions autres

De nombreux autres évolutions ont été cru avec d'autres pokémon de la première génération, avec différents méthode pour les avoirs. Souvent, des noms de fans étaient donnés pour ajouter un aspects plus "puissant" aux pokémons originels.

<div style="text-align:center;">

[![A sprite of Nidogod, an "dangerous-looking" Nidoking](/img/pokegods/nidogod.png)](https://www.deviantart.com/chanchimi/art/Nidogod-Pokegod-191047470)
[![A sprite of Nidogodess, an "dangerous-looking" Nidoqueen](/img/pokegods/nidogodess.png)](https://www.deviantart.com/chanchimi/art/Nidogodess-Pokegod-171224375)
[![A sprite of Flareth, an "dangerous-looking" Flareon](/img/pokegods/flareth.png)](https://www.deviantart.com/chanchimi/art/Flareth-Pokegod-171468888)
[![A sprite of Dreamaster, an "dangerous-looking" Hypno](/img/pokegods/dreamaster.png)](https://www.deviantart.com/chanchimi/art/Dreamaster-Pokegod-171347123)
[![A sprite of Spooky, an "dangerous-looking" Gengar](/img/pokegods/spooky.png)](https://www.deviantart.com/chanchimi/art/Spooky-Pokegod-305148215)

*Vue d'artiste de Pokédieux, par [Chanchimi](https://www.deviantart.com/chanchimi/)*

</div>


Les pokedieux étaient assez nombreux, et voici une petite listes de ceux basées sur des évolutions de pokémon "normaux" :

- **Nidogod**, évolution de Nidoking
- **Nidogoddess**, évolution de Nidoqueen
- **Locustod**, évolution de Papillusion
- **Beepin**, évolution de Dardargnan
- **Raticlaw**, évolution de Ratatac
- **Flareth**, évolution de Flareon
- **Sandswipes**, évolution de Sablairau
- **Spooky**, évolution d'Ectoplasma
- **Pearduck**, évolution de Akwakwak
- **Dreamaster**, évolution d'Hypnomade
- **Diamonix**, évolution de Onix
- **Omnamist**, évolution de Amonistar

Certains de ces pokémons auront des évolutions dans les générations suivantes (tel que Stelix), mais d'autres auront le droit à des méga-évolutions dans les générations 6 et 7 :

<div style="text-align:center;"> 

![Mega-dardargnan, un dardargnan véner](/img/mega/megadardargnan.png) ![Mega-ectoplasma, un ectoplasma véner](/img/mega/megaectoplasma.png)

</div>

A noter que la liste de ces pokémon (et leur noms) étaient très variable suivant les sites, les erreurs d'orthographes faites par les utilisateurs, etc. De plus, il est intéressant de noter que certains pokémons de la première génération *avaient des évolutions de prévus*.

### Evolutions de légendaires

Mew et Mewtwo avaient le droit à des pokédieu aussi, avec le *Chroma Mew* (aussi écrit Chrono, Crona ou Corona suivant les personnes) et le célèbre Mewthree. Mewthree était très souvent représenté par le Mewtwo en armure qui apparaissait dans le premier film, Mew contre Mewtwo. Cette variante de Mewtwo apparaitra dans Pokémon Go ainsi que dans les cartes pokémon pour célébrer la sortie du remake du premier film.

<div style="text-align:center;">

![Mewtwo en armure, tel que montré dans le premier film](/img/other/mewtwoarmure.png)

</div>

Si Mew n'aura pas de méga, Mewtwo aura le droit à deux méga-évolutions, Mewtwo-X et Mewtwo-Y.

<div style="text-align:center;"> 

![Mega-mewtwo X, un Mewtwo plus "musclé"](/img/mega/megamewtwox.png) ![Mega-mewtwo Y, un Mewtwo plus svelte, plus aérodynamique](/img/mega/megamewtwoy.png)

</div>

## Générations suivantes

Un second type de Pokégods *très* courant était ceux des générations suivant apparu dans des magazines, des saisons de l'anime ou dans le premier film : Mewtwo contre Mew. Les deux premiers apparaissant très tôt étant Ho-oh et Togépi, apparaissant dès le début de l'anime, et étant certainement des pokémon abandonné de la première génération, réutilisé dans la deuxième.

<div style="text-align:center;"> 

![Ho-oh en sprite](/img/betagold/247.png) ![Togepi en sprite](/img/betagold/248.png)

</div>

Ensuite, petit à petit d'autres pokémons seront découvert venant des autres média, faisant au total la liste suivante :

- **Ho-oh**, apparu dès le première épisode de pokémon
- **Togepi**, apparu assez tôt dans l'anime
- "**Pikablu**" (Marill), cru comme une évolution eau de Raichu
- "**Pikaflare**" (ou **Flarachu**) cru comme une évolution feu de Raichu.
- **Donphan**
- **Roigada**
- **Pharamp**
- **Lunareon**/**Solareon** étant en fait Noctali et Mentali
- **Snubbull** apparaissant dans le premier film
- **Donphan** apparaissant dans le premier film

<div style="text-align:center;"> 

![Ho-oh en sprite](/img/betagold/pokemonorargent.png)

</div>

A noter qu'à part *Flarachu* (qui était en fait Honooguma, starter feu de la seconde génération), tout ces pokémons seront donc vraiment découvert à la seconde génération, ce qui commencera à baisser les rumeurs.

<div style="text-align:center;"> 

![Quelques version originelle de pokémon, avec un donphan (un éléphant avec aspect un peu armure de pierre), un Pharamp (une sorte de mouton bipede sans laine éléctrique), un roigada (un ramollos avec un cocquillage sur la tête), Ho-oh (un oiseau arc-en-ciel)](/img/betagold/155.png)

</div>

## L'onix de cristal

L'onix de cristal est un onix qui apparaissait dans l'anime, dans la saison des îles oranges. Cet Onix avait la particularité d'être entièrement fait de cristal, ce qui faisait qu'il avait un type différent (qui a souvent été décrit comme proche du type glace) puisqu'il était particulièrement sensible aux attaques feu.

## Pokégods inventés

En plus de cela, de nombreux pokédieux seront tout simplement inventés (liste provenant de l'article de bulbapedia sur les Pokégods) : 

- Millenium
- Anthrax
- Mysterio
- Apocalypse
- Ruin
- Psybir
- Psybird (son évolution)
- Doomsay
- Doomsday
- Tricket
- Hifishi
- Pokémaniac
- Tyranticus
- Primator
- Wizwar
- Le dragon non-identifié

<div style="text-align:center;"> 

![Doomsday un sprite retro… difficile à décrire. C'est insectoïde et ça part dans tout les sens.](/img/pokegods/doomsday.png)

*Un fan-sprite de doomsday, circulant depuis cette époque*

</div>

Il est à noter que cette liste contient beaucoup de noms visant à faire "sombre", ou "puissant". Millenium, Anthrax, Apocalypse, Doomsday… sont des bons examples de noms faisant référence à des trucs dangereux, puissant (ou "cool" pour Millenium). Cela fait partie de cette recherche de trouver le truc "cool", le truc qui attirerait l'attention. Pour le pokémon "Pokémaniac", on peut se dire qu'il peut avoir une origine venant du jeu : il existe en effet un type "pokémaniac" glitché dans le jeu, et sa présence parmis les pokédieux peut provenir de cela.

On peut également remarquer que pour les pokémon décrit comme les évolutions les uns des autres, il s'agit sans doute du même "pokédieu" de rumeur qui a été mal écrit à un endroit, et qu'ensuite d'autres fans ont considéré comme étant l'évolution l'un de l'autre en "fusionnant" les deux noms qui avaient divergé.

## Sources et crédits

- [Chanchimi](https://www.deviantart.com/chanchimi/) - Sprites de PokéDieux utilisés
- [Page bulbapedia sur les PokéGods](https://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9Gods)
- [Page RageCandyBar sur les PokéGods](https://www.angelfire.com/pokemon2/animerpg/pokegodsindex.html)