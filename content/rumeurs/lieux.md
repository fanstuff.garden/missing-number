---
layout: layouts/base.njk
eleventyNavigation:
  key: Lieux spéciaux
  parent: Rumeurs
  order: 4
---

# Lieux spéciaux

Divers lieux spéciaux étaient théorisés dans Pokémon Rouge et Bleu, et était réputés comme étant accessibles et pouvant offrir accès à des pokémon incroyables.

##  La Pokémon Factory

La Pokémon Factory (ou Pokégods Factory) est le nom que je donne à un type de rumeur particulière : il existe un lieu unique, une toute nouvelle ville/donjon ou l'on peut affronter des dresseurs extrèmement puissant et capturer les pokédieux et/ou des pokémons extrêmement rares et/ou puissants.

L'idée de ce lieu est souvent qu'il était un lieu "post-ligue", qu'on atteignait après avoir affronté un certain nombre de fois la ligue et/ou atteints des objectifs ou obtenus des objets secrets. La copypasta  originelle est la suivante : 

>  Il existe un truc nommés les Pokégods. Pour les obtenir, vous devez vaincre l'Elite 4 trente fois sans parler à qui que ce soit entre l'infirmière du Centre Pokémon du Plateau Indigo ou quoi que ce soit, et le riv (note : ici ça signifie rival, c'est une typo présente dans la version originelle). La trentième fois, le Professeur Chen dira "Ça commence à me fatiguer" et te laissera marcher dans le Hall of Fame qui amène à une nouvelle ville ou les gens échangent des Pokégods. 

Cette rumeurs existent en des tonnes de variations, parfois il faut obtenir des CS secretes, parfois il faut réussir à faire revenir l'Océane, etc. Ce lieu a eu de nombreux noms : Cloud City, Brick Town, la Pokégod City/Island… Parfois, on y trouvait aussi du coup des dresseurs d'exceptions, unique, des sortes de "boss secrets".

## Le Jardin de Léo

Le jardin de léo était une rumeur assez répandu dans la communauté Pokémon, qui disait qu'avec certains objectifs de rempli, il était possible de pouvoir accéder à un lieu secret nommé le "Jardin de Léo". D'une manière, cette rumeur fait partie de la même catégorie de rumeurs que la Pokemon Factory, mais souvent était un peu plus large, étant un lieu ou il était dit qu'on pouvait y trouver tout les pokémon possible, notamment les plus rares (et souvent Mew, des évolitions inconnues et/ou des pokégods). Les manières d'y accéder etait souvent diverses : 

- Amener toute les évolitions avant l'incident de téléportation de Bill
- Avoir un pokédex presque complet
- Amener divers pokémon extrèmement rares
- Avoir vaincu pleins de fois la ligue
- Entrer dans le téléporteur

Elles se basaient sur deux aspects : 
- Le côté légendaire de Bill, le "plus grand pokémaniac", inventeur du système de stockage de pokémon par PC
- La présence de tuiles d'herbe derrière la maison de Bill, à travers la montagne, ce qui donnait l'impression que c'était un endroit auquel on pouvait accéder d'une manière ou d'une autre.

D'autres que j'avais lue étaient a base de "capturer Bill quand il est transformé en Pokémon", mais ça c'était sans doute plus notre cruauté habituelle en tant qu'enfant, ahah.

## Bourg Palette

Les herbes autour du Bourg Palettes étaient souvent crue comme étant accessible d'une manière où d'une autre, avec souvent l'idée que c'était ici que le Professeur Chen y garde des pokémon extrêmement rares. Cependant, ce ne sont que des tuiles "visuelles", et on n'y trouve aucun pokémon (on risque juste de faire crash le jeu).

Cette rumeur est très similaire à la précédente.

## Sources et crédits

- https://www.angelfire.com/pokemon2/animerpg/rumors.html