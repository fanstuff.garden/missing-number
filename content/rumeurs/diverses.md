---
layout: layouts/base.njk
eleventyNavigation:
  key: Rumeurs diverses
  parent: Rumeurs
  order: 4
---


# Rumeurs diverses

Des tas de rumeurs moins "grandes" existaient, proposant des moyens d'avoir Mew, ou toutes autres manières détournées d'obtenir des trucs utiles au jeu, ou très puissants.

S'il est facile avec le recul de penser que certaines rumeurs étaient "ridicules", il faut se rappeller que beaucoup étaient cru par des enfants ou de jeunes adolescents, et surtout que les manières "légitimes" d'obtenir certains trucs, via des bugs, sont parfois tout aussi absurdes (tel que le bug du croupier, ou le bug des pierres élémentaires).

## Pokémon Vert

Pokémon Vert avait de nombreuses rumeurs, étant souvent supposé comme "la version unique au japon", une sorte de version supplémentaire/originelle qui aurait eut des choses supplémentaires qu'on n'avait pas eut en occident.

L'une des plus connue était que Mew y était accessible simplement en finissant le Pokédex : si on réussissait à y avoir tout les pokémon, le gérant de Game Freak à Celadopole nous offrait en plus du diplome une pokéball contenant Mew. Le manque de connaissance sur les versions japonaise de Pokémon a permis à celle-ci de persister plus longtemps que bien d'autre rumeur, semblant bien plus "plausible".

## Mew et le camion

Une des rumeurs les plus persistentes consistait à penser que si on réussissait à ne pas faire partir l'Océnae, le bateau que l'on peut trouver à Carmin-Sur-Mer et qu'on rejoint le *camion* qui se trouve sur les bords de la zone accessible à cet endroit, alors on poura utiliser Force sur le camion pour le déplacer et obtenir un Mew.

<div style="text-align:center;">

![Le camion sous lequelle les gens pensaient que mew se trouvait](/img/other/truck.png)

</div>

La rumeur est lié en grande partie à l'aspect unique et "mystérieux" de ce camion : en effet, il n'y en a aucun autre dans tout le jeu, ce qui confère une aura particulier au véhicule. Les rumeurs ont donc commencé à naitre sur l'idée qu'il y aurait quelque chose d'incroyable dedans. De plus, l'aspect difficile d'accès de l'endroit donnait l'espoir d'y trouver quelque chose.

Cependant, des objets seront bien trouvable dans le camion dans les générations suivantes :

- Un Cookie Magma dans Rouge Feu / Vert Feuille
- Un Rappel y spawn tout les jours dans Pokemon Let's Go

## Retour de l'Océane

Une autre rumeur autour de l'Océane était qu'il pourrait potentiellement revenir (ce qui n'est cependant pas possible dans le jeu). La théorie était que si un joueur entrait 365 fois dans le Hall of Fame, le bateau reviendrait avec des opposant plus puissant à bord.

Cette théorie venait du fait que le bateau était supposé revenir tout les ans à Carmin-Sur-Mer.

## Trempette

Une croyance persistente était que l'attaque trempette pouvait, de très rare fois, faire des dégats. Cette rumeurs se base sur la croyance qu'une attaque "ne servant à rien" n'était pas possible, et que beaucoup de gens pensaient qu'il devait y avoir une sorte de surprise, un truc en plus.

Mais non, c'est vraiemnt une attaque qui sert à rien.

## B+Bas

C'est peut-être la rumeurs d'école qui était la plus connue et tenté. L'idée derrière cette rumeur était que si on appuyait sur B+Bas, toute pokéball avait le pouvoir d'une Hyper Ball et devenait surpuissante (chez moi, on avait plus tendance à poser la gameboy et s'eloigner, ou rester le plus silencieux possible).

Dans les faits, cependant, il n'en était rien, et appuyer sur B+Bas n'avait pas d'effet positifs à la capture, ni d'effet négatif. Cette rumeur a été notamment pas mal amplifié par les biais cognitif : quand ça marchait, on se disait que c'était grâce à l'astuce qu'on avait fait.

Cela dit, cette croyance est au final pas forcément plus absurde que l'était tout le système de capture de Pokémon Rouge et Bleu.