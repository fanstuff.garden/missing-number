---
layout: layouts/base.njk
eleventyNavigation:
  key: Les fusions
  parent: Rumeurs
  order: 2
---

# Les fusions de pokémon

Les fusions de pokémon était un type très commun de rumeurs de pokémons "autres" capturables. Ces rumeurs racontaient qu'il était possible avec certains codes de faire fusionner des pokémons, permettant d'obtenir en résultat des pokémons plus puissants. 

Ces rumeurs contenaient parfois beaucoup de pokémon, et utilisaient souvent des amalgame des noms pour donner le noms du résultat. De nombreux de ces pokémons avaient des noms du coup trop long pour la limite de caractères de la première génération, ce qui n'empechait pas la dispersion de ces rumeurs, mais cela provient en grande partie des leur grande inspiration : celles de l'anime/du manga.

Il est amusant de noter que la fusion de pokémon arrivera spécifiquement pour le trio légendaires de la cinquième génération, puisque Kyurem aura la capacité dans Blanc 2 et Noir 2 de fusionner avec Reshiram et Zekrom.

## Un concept existant

A noter que le concept de fusion n'est pas un concept complètement inventé dans Pokémon. Si leur présence dans Digimon a sans doute influencé cela, plusieurs pokémons sont plus ou moins décrit comme étant des fusions dans l'anime ou même dans des descriptions des jeux :

- **Flagadoss** est souvent décrit comme la fusion d'un Ramolos et d'un Kokyas
- **Magneton** est souvent décrit comme la fusion de trois Magnéti
- **Triopikeur** est souvent décrit comme la fusion de trois Taupiqueur
- **Smogogo** est souvent décrit comme la fusion de deux Smogo

Donc, l'idée même que des pokémons puissent fusionner (même si ce n'était pas de la manière décrite par les "codes", ni quelque chose qui arrivait en gameplay) existait déjà dans l'univers collectif.

## Dans les autres média

Outre les "fusions" présente dans les jeux, l'anime et le manga Pokémon contiendront chacun une fusion qui influencera beaucoup ce que seront dans l'esprit de ces rumeurs les fusions : deux pokémon mélangeant des aspects de leur apparence, et les noms "amalgame"

- **Venustoise** est la version présente dans l'anime (mais qui n'est qu'une illusion), une fusion entre un florizarre et un tortank
- **Zapmolcuno** est la version présente dans le manga, une fusion des trois oiseaux légendaires 

Ces fusions seront à la source de la plupars des rumeurs de fusions qui existeront.

## Petite liste de fusion

Voici une petite liste de fusion, reprise du site [RageCandyBar](https://www.angelfire.com/pokemon2/animerpg/merged_pokegods.html). Ici, je vais surtout mettre une liste plutôt que de mettre pour chacun l'exemple utilisée pour comment les obtenir. En effet, les codes pouvaient régulièrement varier suivant les sites et qui écrivaient, pouvant venir du gameshark ou de codes plus complexes.

- **Articzapmewtres** contient les trois oiseaux légendaires et mew
- **Charmewsquirasuar** contient les trois starters et mew
- **Chartres** contient dracaufeu et sulfura
- **Charticuno** contient Artikoda et Salamèche
- **Pikamew** contient Pikachu et Mew
- **Pikish** contient Pikachu et Mystherbe
- **Ratichu** contient Ratatac et Raichu
- **Supercharstoise** contient Dracaufeu et Tortank
- **Stonebro** contient… *soupir* : Ramolos, Papilusion, Dardargnan, Artikodin, Lipoutou and Grolem

Ces rumeurs se recoupent parfois aussi avec celle habituelle des PokéGods, ou d'autres éléments du jeu :

- **Chrono Mew** ou **Mewthree** ont été parfois décrit comme des fusions de Mewtwo et Mew
- **Nidogod** a déjà été décrit comme des fusions de Nidoking et Nidoqueen (et parfois avec un Ramollos)
- Les **spectres** de Lavanville ont été décrit comme une fusion des trois membres de la lignée de fantominus

Et avec cela, on peut voir qu'en fait, sans doute presque toute combinaison de pokémon à eu sa fusion, et sa méthode souvent un peu aléatoire pour l'obtenir. C'est une des choses que l'on peut retrouver avec toute cette époque, cette effervescence de trouver des "secrets", souvent couplé à une envie que son site ai plus de visite que les autres sites de fans.

## Les méthodes

Les méthodes souvent recoupaient un certains nombre de paramètres, qui souvent se fusionnaient :
- Mettre un pokémon dans une certaine place, ou les pokémon dans un certain ordre dans l'équipe
- Avoir les pokémon niveau 100
- Faire un certain nombre de fois la ligue
- Avoir certaines attaques sur l'un des pokémons ou tous
- Aller à un endroit particulier et faire une action spécifique
- Avoir une Mist Stone
- Parler à Mr. Psy