---
layout: layouts/base.njk
eleventyNavigation:
  key: Rumeurs
  order: 11
---

# Les rumeurs pokémon

La série pokémon à eut beaucoup de rumeurs au fil des années, et les premières générations étaient en particulier pleines d'histoire et de rumeurs sur ce que pouvait contenir les jeux. La plupars de ces rumeurs ont été debunk, et le but est d'un peu les découvrir ici.

## Liste des pages