---
layout: layouts/base.njk
eleventyNavigation:
  key: Rive de Cramois'île
  parent: Les glitches
  order: 3
---

# Rive de Cramois'île

Un des glitches les plus simple à exploiter dans pokémon est le glitch sur *la rive droite de Cramois'île*. En effet, cette rive n'a pas de collision pokémon de codée, ce qui permet de faire plusieurs exploitation permettant de faire apparaitre des pokémons là ou ils ne devraient pas apparaitre.

Le bug fonctionne de la manière suivante : comme Cramois'île n'a pas de liste de pokémon, la rive droite fait apparaitre les derniers pokémons qui ont peuplé cette liste (et, dû à une erreur, celle des hautes herbes). Si on y arrive du coup avec vol, ou avec d'autres éléments pouvant rajouter des pokémon dans cette liste, des effets particuliers peuvent se produire. 

Ce bug est résolu dans Pokémon Jaune, en effaçant les données de pokémon sauvage avant de les ajouter, ce qui fait que la rive droite ne produit aucun pokémon.

## Bug du vieil homme

Un des plus connus, est que *le nom que vous entrez* est stockée à cet endroit dans Pokémon Rouge et Bleu lorsque le vieil homme vous montrera comment capturer un pokémon. Les pokémon que vous rencontrerez seront tous entre le niveau 127 et 245, du a la position des caractères.

Votre nom sera décomposée en donnée de pokémon sauvage de la manière suivante : La première lettre n'a pas d'effet, puis une lettre sur deux servira à déterminer le niveau, puis le pokémon à ce niveau. Certaines rencontres provoquent des combats contre des dresseurs, permettant par exemple d'affronter le Professeur Chen… cependant, sans une équipe spécifique, puisque les dresseurs que vous y rencontrerez cherchent l'équipe ayant un identifiant lié au niveau, donc la 127e variante d'équipe pour ça. Le Ditto Glitch est en cela plus efficace pour rencontrer ces dresseurs.

<details>
<summary>Cliquez ici pour voir la liste des pokémon et niveau créé par chaque lettre</summary>

| Caractère | Pokémon | Niveau |
|:---------:|:-------:|:------:|
|Espace|Scarabrute| 127 |
|A|Akwakwak| 128 |
|B|Hypnomade| 129 |
|C|Nosferalto| 130 |
|D|Mewtwo| 131 |
|E|Ronflex| 132 |
|F|Magicarpe| 133 |
|G|MissingNo.| 134 |
|H|MissingNo.| 135 |
|I|Grotadmorv| 136 |
|J|MissingNo.| 137 |
|K|Krabboss| 138 |
|L|Crustabri| 139 |
|M|MissingNo.| 140 |
|N|Électrode| 141 |
|O|Mélodelfe| 142 |
|P|Smogogo| 143 |
|Q|Persian| 144 |
|R|Ossatueur| 145 |
|S|MissingNo.| 146 |
|T|Spectrum| 147 |
|U|Abra| 148 |
|V|Alakazam| 149 |
|W|Roucoups| 150 |
|X|Roucarnage| 151 | 
|Y|Staross| 152 |
|Z|Bulbizarre| 153 |
| ( | Florizarre | 154 |
| ) | Tentacruel | 155
| : | MissingNo. | 156 |
| ; | Poissirène | 157 |
| [ | Poissoroy | 158 |
| ] | MissingNo. | 159 |
|a|MissingNo.| 160 |
|b|MissingNo.| 161 |
|c|MissingNo.| 162 |
|d|Ponyta| 163 |
|e|Galopa| 164 |
|f|Rattata| 164 |
|g|Rattatac| 166 |
|h|Nidorino| 167 |
|i|Nidorina| 169 |
|j|Racaillou| 169 |
|k|Porygon| 170 |
|l|Ptéra| 171 |
|m|MissingNo.| 172 |
|n|Magnéti| 173 |
|o|MissingNo.| 174 |
|p|MissingNo.| 175 |
|q|Salamèche| 176 |
|r|Carapuce| 177 |
|s|Reptincel| 178 |
|t|Carabaffe| 179 |
|u|Dracaufeu| 180 |
|v|MissingNo.| 181 |
|w|MissingNo. (fossile Kaputops)| 182 |
|x|MissingNo. (fossile Ptéra)| 183 |
|y|MissingNo. (spectre)| 185
|z|Mystherbe| 186 |
|'|Karatéka| 224 |
|Pk|Blue| 225 |
|Mn|Prof Chen | 226 |
|—|Chief| 227 |
|?|Sbire Rocket| 230 |
|!|Topdresseur♂| 231 |
|♂|Auguste| 239 |
|×|Gentleman| 241 |
|.|Rival Blue | 242 |
|/|Blue (Champion)| 243 |
|,| Olga | 244 |
|♀|Exorciste| 245 |

</details>

## Parc Safari

Comme la zone réplique aussi les pokémons sauvage du dernier lieu que vous avez visité, c'est un moyen bien plus efficace de capturer les pokémons du parc safari. Pour cela, il faut faire *Tunnel* ou *Téléport* pour sortir du Parc Safari, puis voler vers Cramois'île.

Les pokémon seront alors présent dans l'eau, et sans les contraintes du parc safari.