---
layout: layouts/pokedex-missingno.njk
eleventySubNavigation:
  key: MissingNo.
  parent: Pokémons glitchs
  order: 1
---

# MissingNo.

MissingNo. (Ketsuban en japonais ) est un pokémon glitch ayant le n°000 du pokedex de Kanto, de type *Oiseau* et Normal. Dans Pokémon Jaune, il est cependant de type Normal et 999. Il a une forme unique à chaque jeu (sa frome de base), et trois formes uniques communes aux trois jeux. Il est surnommé le "Pokémon 000".

Dans les version française, le jeu crash à sa rencontre sans un fix spécifique.

## Explication

MissingNo. n'est pas un glitch à proprement parler, mais un *placeholder*. Il s'agit de donnée vide servant à remplir la place de pokémon non-utilisé. Cependant, il n'a pas été complètement bien codée parce que son espèce (l'espèce 000) va quand même puiser dans des endroits de données imprévues, au lieu d'avoir des vrai donnée vides pour ça (d'où ses attaques, ses types changeants suivant les version, ses sprites, etc).

## Effets

MissingNo. peut avoir les effets suivants :
- **Glitch de duplication d'objet** : MissingNo. passera l'objet numéro 6 du sac à 128 exemplaire s'il n'y en a pas déjà 128 ou plus.
- Il peut corrompre les données du Panthéon
- Dans Pokémon jaune il peut faire crash le jeu et possède un cri très long.

## Généralité

### Statistiques (Rouge/Bleu)

|   | PV | ATK | DEF | SPD | SPE | 
|:-:|:--:|:---:|:---:|:---:|:---:|
| **Base :** | 33 | 136 | 0 | 29 | 6 |

### Statistiques (Jaune)

|   | PV | ATK | DEF | SPD | SPE | 
|:-:|:--:|:---:|:---:|:---:|:---:|
| **Base :** | 178 | 19 | 11 | 0 | 23 |

### Évolutions

<table>
  <thead>
    <tr>
      <th>Évolue en</th>
      <th>via</th>
    </tr>
  </thead>

  <tbody>
    <tr>
        <td colspan="2"> <em>Stade Final</em> </td>
    </tr>
  </tbody>
</table>

## Emplacements

<table class="pokedex">
  <thead>
    <tr>
      <th></th>
      <th>Emplacement</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <th>Rouge </th>
      <td>
        Rencontre via des glitchs
      </td>
    </tr>
    <tr>
      <th>Bleu </th>
      <td>
        Rencontre via des glitchs
      </td>
    </tr>
    <tr>
      <th>Jaune </th>
      <td>
        Rencontre via des glitchs
      </td>
    </tr>
  </tbody>
</table>

## Attaques (Rouge/Bleu)

<div class="grid" style="display:flex;">
  <div>
    <table>
        <thead>
            <tr>
              <th>Attaques de base</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Pistolet à O</td>
            </tr>
            <tr>
                <td>Pistolet à O</td>
            </tr>
            <tr>
                <td>Pique</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th>Niv</th>
                <th>Attaque</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"> <em>Aucune attaque apprise par niveau</em> </td>
            </tr>
        </tbody>
    </table>
    </div>
    <div>
    <table>
        <thead>
            <tr>
            <th>Par CT / CS</th>
            </tr>
        </thead>
        <tbody>
            <tr><td>Ultimapoing</td></tr>
            <tr><td>Coupe-Vent</td></tr>
            <tr><td>Danse Lames</td></tr>
            <tr><td>Ultimawhashi</td></tr>
            <tr><td>Toxik</td></tr>
            <tr><td>Bélier</td></tr>
            <tr><td>Damoclès</td></tr>
            <tr><td>Bulles D'O</td></tr>
            <tr><td>Laser Glace</td></tr>
            <tr><td>Blizzard</td></tr>
            <tr><td>Sacrifice</td></tr>
            <tr><td>Frappe Atlas</td></tr>
            <tr><td>Frénésie</td></tr>
            <tr><td>Fatal-Foudre</td></tr>
            <tr><td>Séisme</td></tr>
            <tr><td>Abîme</td></tr>
            <tr><td>Psyko</td></tr>
            <tr><td>Téléport</td></tr>
            <tr><td>Piqué</td></tr>
            <tr><td>Repos</td></tr>
            <tr><td>Cage-Éclair</td></tr>
            <tr><td>Triplattaque</td></tr>
            <tr><td>Clonage</td></tr>
            <tr><td>Coupe</td></tr>
            <tr><td>Vol</td></tr>
            </tr>
        </tbody>
    </table>
    </div>
</div>


## Attaques (Jaune)

<div class="grid" style="display:flex;">
  <div>
    <table>
        <thead>
            <tr>
              <th>Attaques de base</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Jackpot</td>
            </tr>
            <tr>
                <td>Etreinte</td>
            </tr>
            <tr>
                <td>Pistolet à O</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th>Niv</th>
                <th>Attaque</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"> <em>Aucune attaque apprise par niveau</em> </td>
            </tr>
        </tbody>
    </table>
  </div>
  <div>
    <table>
        <thead>
            <tr>
            <th>Par CT / CS</th>
            </tr>
        </thead>
        <tbody>
          <tr>
              <td> <em>Aucune attaque apprise par CT/CS</em> </td>
          </tr>
        </tbody>
    </table>
    </div>
</div>

## Crédits et sources

- Page MissingNo. de [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/MissingNo.)
- Page MissingNo. de [Glitch City Wiki](https://glitchcity.wiki/wiki/MissingNo.)