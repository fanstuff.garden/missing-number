---
layout: layouts/base.njk
eleventyNavigation:
  key: Pokémons glitchs
  parent: Les glitches
  order: 1
---

# Pokémon Glitch

Un pokémon glitch est un pokémon composée de données non prévues par le jeu. Le plus connu d'entre eux est missingno, mais de nombreux autres Pokémon glitch existent.

## Sous-pages

<ul>
  {%- for post in collections.all | getSubPages(eleventyNavigation.key) -%}
    <li><a href="{{ post.url }}">{{ post.data.eleventySubNavigation.key }}</a></li>
  {%- endfor -%}
</ul>

## Glitch hybrides

Les Glitch "hybrides" sont des pokémon glitches qui partagent leur "espèce" avec celle d'un vrai pokémon. En effet, une partie des informations des données (notamment les attaques de départ, les types, les ct/cs apprise et le sprite) sont chargée à partir du *numéro de pokedex* (ce qu'on appelle "l'espèce"), tandis que d'autres sont chargées à partir de l'identifiant internes (on peut voir cela dans [la décompilation notamment](https://github.com/pret/pokered/tree/master/data/pokemon)).

Cela fait que des pokémons partageant le même ID de pokédex auront de nombreuses caractéristiques communes (ce qui est impossible en jeu normal, mais possible sur les pokémon glitchs). Les pokémons glitch ayant le même id de pokédex qu'un pokémon normal seront nommé des *Glitch hybrides*.

Cependant, il est à noter que d'autres données peuvent affecter et faire que les glitches hybrides ont une apparence différente de celle du pokémon originel, notamment des glitches causé par la taille du nom, etc.

## Glitchs "purs"

D'autres pokémon glitch ont des espèces pokémon glitchées, et donc auront des données non-prévue aussi sur leur espèce, ils sont alors d'une espèce glitch.

Il a 22 espèces glitchs répartie sur Pokémon Rouge/Bleu et Pokémon Jaune, en plus de celle de MissingNo.

## Pokémon hybrides instables

En plus des Pokémon Glitch, il est possible de faire passé des pokémon légitimes dans un état buggué en en faisant des *Pokémon hybrides instables*. Le principe de ces pokémon hybrides instables est de provoquer une fusion de deux pokémon via un glitch, ce qui fait que le pokémon aura l'espèce d'un autre pokémon, mélangant des données des deux pokémons. 

L'hybride est composé d'un pokémon "donneur" (le pokémon d'où l'hybride prendra l'espèce), et un pokémon receveur (le véritable pokémon).

Ce pokémon aura les particularités suivantes :

| Version | Donneur | Receveur |
|:-------:|:-------:|:--------:|
| Rouge/Bleu/Jaune | Sprite (menu+combat), type affiché, évolution, movepool (CT et niveau), cap d'expérience | Palette, pokémon qui sera reçu une fois dans Stadium 2 ou la Pension, groupe d'expérience, stats de base |
| Or/Argent/Crystal | Sprite hors-combat + menu, évolution, moovepool (CT et niveau), cap d'expérience, genre hors-combat | Sprite en combat, genre en combat |

## Crédits et sources

- Page Pokemon Glitch de [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Glitch_Pok%C3%A9mon)
- Page Glitch [Pokémon Familly de Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Glitch_Pok%C3%A9mon_family)
- Page MissingNo. de [Glitch City Wiki](https://glitchcity.wiki/wiki/Glitch_Pok%C3%A9mon)