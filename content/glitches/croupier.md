---
layout: layouts/base.njk
eleventyNavigation:
  key: Bug du Croupier
  parent: Les glitches
  order: 4
---

# Le Bug du Croupier

Ce bug est l'un des bugs les plus connus, puisqu'il est celui qui permet de *capturer Mew*. En vérité, à partir de ce bug, il est possible de l'étendre et de capturer des tas de pokémons, ou de combattre des dresseurs.

## Capturer Mew

<div style="text-align:center;margin:auto;">

![Mew en style retro](/img/pret/pokered/front/mew.png)

</div>

Pour rencontrer Mew, la méthode à faire est la suivante :
- Ne pas combattre avant le Croupier de la Route 8 à côté du Sousterrain, ni le Gamin route 25 avec un Ramollos dans son équipe (il est tourné vers le nord)
- Avoir un pokémon ayant Vol dans son équipe
- Aller commencer le combat contre le Croupier MAIS appuyer sur start au moment de faire le pas, afin de faire apparaitre le menu start. Il faut ensuite voler avec Azuria
- Il faut aller affronter le Gamin route 25, en ayant *au moins un pas d'écard avec le Gamin avant la rencontre* (sinon le jeu freeze)
- Après le combat, il faut retourner vers la route 8, sans rencontrer le moindre combat ni sauvegarder. La méthode la plus pratique et d'utiliser Vol vers Lavanville
- Une fois arrivé sur la Route 8, le menu start apparaitra. En le quittant, un combat contre un Mew Sauvage niveau 7 se produira.

A noter qu'entre la "fuite" du combat contre le Croupier et le combat contre le Gamin, les boutons A, B et Start ne marcheront pas.

## Explication

Le jeu se base sur plusieurs erreurs de programmation pour provoquer le glitch. Les dresseurs voyant læ joueur⋅euse de loin mettent une frame avant de vraiment déclencher la rencontre, permettant d'afficher le menu start et de fuir le combat avec fly. Cela mettra le jeu dans un état instable ou il croit être en combat mais ne l'est pas, notamment le script de la carte ou le joueur était cherchera à lancer un combat. Seul certains dresseurs permettent de faire fonctionner cela. Lorsque le joueur retournera sur la route, il tentera de recharger le combat à partir de l'adresse mémoire ($CD2D) ou est stocké le dresseurs que l'on va combattre, puisque le script de la carte sera en mode "un combat va se lancer".

Ensuite, en allant faire un combat, cela va avoir deux effets : rétablir l'accès aux fonctions normales du jeu (mais pas le script de la map que vous avez quitté) et intégrer une autre variable dans la même adresse mémoire : le **spécial** non modifié du pokémon que vous avez affronté. Le Ramollos du Gamin ayant une valeur de 21, l'identifiant interne de Mew, c'est cette valeur qui se retrouvera dans l'emplacement mémoire partagé entre le **spécial** (en combat) et le combat qui va se lancer (sur la map), ce qui fait qu'arrivé sur la map, cela lancera le combat 21 : Mew. Vous pouvez voir les ID possible de pokémon légitime via le [Pokédex](/pokedex/g1/), et les différents ID de rencontre via la page Correspondance 1G de [PRAMA Initiative](https://www.prama-initiative.com/index.php?page=correspondance-1G)

Le niveau 7 sera lié au niveau de modification de la force/attaque (si elle a baissé ou augmenté, via rugissement), 7 étant le niveau de base, 1 celui après 6 attaque pour le baisser, etc. Cela peut permettre aussi de rencontrer des pokémon niveau 1.

## Le Ditto Glitch

<div style="text-align:center;margin:auto;">

![Metamorph en style retro](/img/pret/pokered/front/ditto.png)

</div>

Il est possible d'étendre ce combat avec une rencontre de n'importe quel pokémon, et son **spécial** sera prise comme identifiant de combat. Cependant, il est difficile de véritablement prévoir le combat qu'on va rencontrer, puisque le spécial est difficile à savoir sur un pokémon sauvage.

Cependant, il existe un moyen de prédire cela : Métamorphe. En effet, Métamorph prend les stats du pokémon en lequel il se transforme, ce qui vous permet d'utiliser votre équipe pour obtenir le special voulu. Ensuite, avec rugissement, vous pouvez baisser son attaque afin de pouvoir réussir à avoir le niveau que vous voulez, ou la variante de combat que vous souhaiterez.

Pour cela, après avoir combattu le second dresseur (il est préférable de perdre afin de pouvoir refaire le bug à l'infini), allez dans la Route 23 dans Rouge/Bleu ou le Manoir Pokémon dans Jaune, et combattez un Métamorphe. Faite le se transformer en le pokémon dont vous souhaitez utiliser le special, utilisez rugissement si vous souhaitez faire une rencontre spécifique, puis fuyez ou battez le métamorphe.

Une fois retourné sur la route initiale, vous aurez le combat que vous souhaitez.

## Les dresseurs

A partir de l'ID 200, comme visible dans [cette liste](https://bulbapedia.bulbagarden.net/wiki/Mew_glitch#Method_#1), vous rencontrerez des dresseurs, dont des dresseurs glitchs. Pour avoir leur combats normaux, vous devez pour la plupars utiliser 6 rugissement pour avoir leur party n°1.

Certaines équipes de dresseurs glitchés sont apprécie en tant que combat "ultime" surpuissant tel qu'Agatha Ultima, sa variante 7 de Jaune (donc celle que vous avez de base en faisant un Ditto Glitch avec sa valeur, 246) qui à comme équipe (dans la version anglaise contient tous niveau 205 un Dodrio, un Otaria, un Pikachu, un Caninos, Hypocean, un Caninos et le pokémon glitch "Z")

Au dela d'un special à 247, vous risque de provoquer le bug ZZAZZ, un bug qui écrit par dessus de nombreuses variables, pouvant corrompre les données du jeu. Il a un effet plutôt "amusant" qui est de remplir l'équipe du dresseur de Bulbizarre ayant l'attaque explosion.

### JACRED

JACRED est un dresseur glitché qui fait crash le jeu.

### Combattre le Prof Chen

Pour combattre le Prof Chen avec cette méthode, utilisez un pokémon avec un special de *226*, et utilisez le nombre de rugissement suivant pour obtenir la variante que vous souhaitez.

- 6 fois pour Tortank
- 5 fois pour Florizarre
- 4 fois pour Dracaufeu