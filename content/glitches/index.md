---
layout: layouts/base.njk
eleventyNavigation:
  key: Les glitches
  order: 13
---


# Glitches dans Pokémon

Pokémon est connu pour avoir beaucoup de glitch. Dans la génération 1, ils sont assez nombreux et font partie presque intégrante de l'expérience. le but de cette catégorie est de parler des différents glitch qu'on pouvait y trouver. Ces pages parlent surtout des glitchs présents dans la première génération.

Le but de cette page n'est pas d'aller loin dans l'execution de code arbitraire et le rabbit-hole de ce qu'il est possible de faire avec les glitchs de Pokémon Rouge et Bleu, mais plus de brosser un petit tableau général de ce qu'on peut trouver dans cet univers.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>

## Pourquoi la version EN ?

Ces pages utilise principalement pour sa glitchologie les version anglaise. C'est en partie du au fait que dans la version française, MissingNo à tendance à faire planter le jeu (ce qui rend la glitchologie plus complexe). Utiliser les version anglaises permet donc d'obtenir plus d'informations. 

Cependant, nous mettrons où c'est possible aussi l'accès à des informations sur ce que ça donne sur la version française.

## Crédits et sources

Une partie des informations sur ces pages proviennent des sites suivants :
- [Glitch City Wiki](https://glitchcity.wiki) - Un wiki anglophone de glitchologie
- [PRAMA Initiative](https://www.prama-initiative.com/) - Un site francophone de glitchologie