---
layout: layouts/base.njk
eleventyNavigation:
  key: Glitch City
  parent: Les glitches
  order: 5
---

# Glitch City

Glitch City est un des bugs iconiques de Pokémon Rouge et Bleu, consistant en une corruption de map la transformant en un espace à l'apparence bugguée.

Il existe plusieurs type de glitch city, le plus connu étant celle étant accédée depuis le Parc Safari.

## Via le Parc Safari

La variation originelle de Glitch City, et la plus connue, est celle obtenue par *le Glitch du Parc Safari*.

<div style="text-align:center;">

![Un écran de pokémon avec des tuiles glitchées](/img/glitches/glitch-city.png)

</div>

Pour déclencher le bug, il faut faire les étapes suivantes :
- Entrer dans le parc safari.
- En sortir, mais quand le gardien nous demande si on veut sortir, lui dire "non"
- Sauvegarder et redémarrer
- Retenter de sortir
- Ce sera alors le message pour entrer dans le parc qui sera actif. Répondre non et sortir
- Partir dans un endroit hors d'une grande ville et épuiser les 500 pas du parc safari

On aura alors le message de fin des pas autorisé du parc safari, et on sera transporté à la porte du parc safari… mais en sortant, on se retrouvera à Glitch City, face à un tileset tout buggué.

Pour en sortir, il est possible d'utiliser vol ou téléport.

### Explication

Glitch City est une corruption qui se produit parce que le jeu charge trop d'information sur la map. Le déclenchement dans ce cas se produit parce que la sortie du parc safari n'est pas codé pour amener à un endroit précis de Parmanie, mais *au quatrième point de téléportation de la dernière map visitée". Hors, si on est dans un espace ayant moins de quatre point de télportation (maison, etc), la map chargera mal le quatrième point de téléportation et corrompra la map d'où on venait.

Ce type de glitch city est nommé "Glitch City de Carte Valide", puisqu'il s'agit d'une glitch city chargée depuis une "vrai" carte. Certaines variations de cette Glitch City ont été nommé "Glitch League" (quand le dernier pas est dans la League Pokémon) ou "Glitch Mountain" (sur la piste cyclable).

### Autre moyens d'y accéder

Il existe d'autres moyen d'accéder à une glitch city de carte valide, et les moyens suivants ont été documentés :

- Sortir de la map sur l'axe de la verticale (via un bug de walk-through-wall, par exemple) lorsqu'il n'y a pas de connection
- Utilier l'objet glitch 10F
- En causant une corruption du Hall of Fame (via un 'M) et en se connectant au PC du dernier etage de la Sylph SARL.

## Phantom City

Les cités fantômes (Phantom City en anglais) sont un type de Glitch City représentant des copies exacte des map située sur la droite des map, en sortant de la map, 6 tuiles après les limites de la map. Les map formeront alors une boucle tout les "taille de la map + 6 tuiles". Pour y accéder, il faut réussir à passer à travers les limites de la map, sans déclencher un crash parce que le jeu tente de charger une carte invalide autour de la map.

Ces cités ont les particularités suivantes :
- Il n'y a pas de point de téléportation ni de PNJ
- Le joueur sera à chaque "boucle" quelques pixel décalé à la verticale.

## Sortir de la map

Deux des glitch city citée plus haut sont accessible depuis un out-of-bound, mais la plupars du temps cela provoque juste un crash. En effet, lorsque l'on sort d'une map, le jeu tente de charger la map connectée à celle où on est, via le point cardinal (nord, sud, est, ouest). Cependant, lorsqu'il n'y a pas de map, le jeu freeze en tentant de charger la map "255" qui n'existe pas.

Ces "tuiles de freeze" sont sur toute les limites de la map (-1 et la hauteur/largeur de la map).

### Glitch Hell

Glitch Hell est un bug se produisant dans les version japonaise de Pokémon (et dans certaines version de Rouge et Bleu, mais sur émulateur uniquement) qui se produit lorsqu'on sort de la map. L'écran devient instannément buggué, provoquant des sons et des graphismes aléatoires causées par les données présentes en mémoire.

<div style="text-align:center;">

![Un écran de pokémon complétement glitché](/img/glitches/glitch-hell.png)

</div>

Dans les versions plus récentes, cela ne provoque qu'un écran blanc (et quelques tuiles glitchées avant).

### Sortir de la map sans planter

Il est quand même possible de réussir à sortir de la map sans provoquer de freeze. Pour cela, il faut exploiter la taille des maps, et les connections qui existent vraiment entre les map. POur sortir d'une map sans provoquer de freeze, il faut sortir d'une map à l'emplacement ou une autre map existe (genre au sud d'une map qui a une map au sud, etc), mais de manière à être trop haut ou trop bas pour vraiment entrer à l'intérieur de la map.

<div style="text-align:center;">

![Une fleche sur une carte de vieux jeux pokémon, montrant un moyen de sortir d'une map en étant out of bound sur l'autre](/img/glitches/outofbound.png)

</div>

Sur la map en exemple, on peut voir que si on sort par la flèche rouge, on sera du côté qui se connecte à la Route 1 (donc une map valide, ce qui évite le freeze), mais HORS de cette map (ce qui permet d'être out of bound). Cela permet d'aller out of bound aussi de Bourg Palette, puisqu'on sera toujours sur une connexion valide, mais on arrivera dans le out of bound de Bourg Palette

En regardant sur cette [cette map](https://helixchamber.com/wp-content/uploads/2019/07/Kanto-B.png) d'Helix Chamber (qui est la base que j'ai utilisé pour l'image d'exemple) qu'il y a pas mal de ces maps qui s'alignent pas parfaitement.

## Dans la Gen 2

Il est possible de causer un glitch similaire en rencontrant des pokémon glitch dans Pokémon Or, Argent et Crystal.

## Sources et crédits

- [Vidéo "Pokémon R/B/Y: What's out of bounds?" de ZZAZZGlitch](https://www.youtube.com/watch?v=xuEsiyyYwVk)
- Articles [Glitch City](https://glitchcity.wiki/wiki/Glitch_City) de Glitch City Wiki
  - Les images viennent notamment de Glitch City Wiki, et d'un article d'[Helix Chamber sur les maps de Kanto](https://helixchamber.com/2019/07/22/proto-map-analysis/)