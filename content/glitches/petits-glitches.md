---
layout: layouts/base.njk
eleventyNavigation:
  key: Petits glitches
  parent: Les glitches
  order: 99
---

# Petits glitches

Il existe dans le premières génération pleins de plus petits glitchs qui peuvent affecter le jeu, et que connaitre peut être utiles. Certains peuvent aider le joueur, d'autre lui poser soucis.

## Bug 1/256

Toutes les attaques ont 1 chances sur 256 en plus d'échouer que prévue, même celle qui ont normalement 100% de précision. C'est du à une comparaison dans le code qui est basé sur un "inférieur/supérieur à" et non "inférieur/supérieur ou égal à". Patience et Météores ne sont pas affectée, puisqu'elles bypasse entièrement la vérification.

## Bug des badges

Dans Pokémon Rouge et Bleu, différents badges offrent différents boost aux statistiques, permettent de les rendre plus puissantes, apportant un aventage au joueur dans le jeu au fur et à mesure qu'il progresse. Les boost sont les suivants : Attaque pour le badge d'Azuria, Défense pour celui de Carmin-Sur-Mer, Special pour celui de Cramois'île et Vitesse pour celui de Parmanie. A noter qu'il y a une inversion entre l'effet et ce qui est dit, puisque le jeu déclare que celui de Carmin-Sur-Mer augmente la défense et ce lui de Parmanie la vitesse.

Il y a un bug dans ce comportement cela dit : à chaque modification de statistique, les modifications de stats des badges seront réappliqué, rendant le pokémon encore plus fort.

## Coups critiques

Puissance et le Muscle+ divisent par 4 les chances de coup critique au lieu de les quadrupler.

## Poképoupée

La Poképoupée permet de fuir le fantôme d'Ossatueur dans la tour pokémon, ce qui permet de skipper un donjon entier (le repaire de la Team Rocket de Celadopole).