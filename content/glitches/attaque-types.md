---
layout: layouts/base.njk
eleventyNavigation:
  key: Attaques et types
  parent: Les glitches
  order: 2
---

# Attaques et types glitch

Le but de cette page est de couvrir des informations sur les attaques et les types glitches qui peuvent exister dans pokémon. Ces attaques ont pas mal de redondances et d'effets projets, notamment au niveau des types, qui au final pour la plupars provoquent peu d'effets intéressant.

## Types glitches

Il existe plusieurs catégorie de types "glitch". Le [type bird](/beta/divers/#type-bird) est souvent vu comme un type "glitch" un type abandonné.

Parmis les différents types de 
- Onze copies du type "normal". Ces types sont des placeholder (comme MissingNo), sans doute parce que l'équipe préférait avoir de la place pour potentiellement rajouter d'autres types si besoin.
- Une copie de toute la liste des types à partir de l'ID 80 (ce qui comprend les 11 types normal placeholder)
- Des types qui contiennent des noms relié à d'autres éléments du jeu (CHIEF, COOLTRAINER, etc)
- Des types qui contient des listes de caractères aléatoires (par exemple "ゥPC ゥTM x'yゥ")

La plupars des listes de types n'ont pas de résistence, faiblesse, etc. Peu aussi ont de vrai effet d'attaque de même type parce qu'il est rare d'avoir un pokémon et une attaque de même type.

Il y a une liste des types glitchs possibles sur [Glitch City](https://glitchcity.wiki/wiki/TypeDex). Une liste similaire est présente sur [PRAMA Initiative](https://www.prama-initiative.com/index.php?page=glitchdex-types)

## Attaques glitches

Les attaques glitchées sont des attaques au dela des attaques prévues par le jeu, qui chargent souvent des données non-prévues par le jeu, provoquant des effets non-voulus. A noter que les effets pour varier entre les version française et anglaise du jeu. Beaucoup d'attaques ont des faux types. On peut classe en trois grande catégories d'attaques glitchs.

- L'attaque `-` (nom variable/aléatoire) situé à l'ID 00 est une attaque de type COOLTRAINER♀.
- Les attaques Super Glitch sont des attaques ayant un nom long et buggué allant de l'ID 166 à 195 (inclus).
- Des attaques aux noms de CS/CT allant de l'ID 196 à 255.

Les attaques ont des données d'attaques normales, en plus de leur potentiel effet. L'attaque "-" et les attaques "Super Glitch" ont la potentialité de provoquer l'effet éponyme (de manière partielle pour "-").

### Liste des attaques

*Note: Ce tableau est basé plus sur la version anglaise*

| Nom | ID | Effets |
|:---:|:--:|:------:|
| -- | 00 | Super Glitch Partiel |
| Super Glitch | 166-195  | Corruption Super Glitch |
| HM01 | 196 | Attaque avec les effets de Choc Mental. Si ne met pas KO, le message "Mais rien ne se passe" apparaitra. |
| HM01 | 196 | Attaque avec les effets de Choc Mental. Si ne met pas KO, le message "Mais rien ne se passe" apparaitra. |

Pour une liste plus complète et liée à la version française, vous pouvez voir celle-ci sur [PRAMA Initiative](https://www.prama-initiative.com/index.php?page=glitchdex-attaques)

## Super Glitch

L'effet Super Glitch est une corruption de mémoire provoqué par des attaques ayant un nom qui n'a pas de marqueur de fin. Plus précisément, cela vient du fait que les attaques dites "Super Glitch" et "-" n'ont pas de vrai "nom", mais une série de donnée de la ROM qui va être pris comme si c'était un nom, et qui n'a donc pas forcément le caractère de fin présent.

Le nom continue donc jusqu'à trouver un marqueur de fin, provoquant un remplacement de donnée située après la fin du nom dans la mémoire vive. Cela provoque beaucoup d'effets inattendu, et peut même être utilisé pour modifier la mémoire et faire pas mal de trucs avec.

Une grande corruption connue qu'il provoque est l'effet "TMTRAINER" se produit si on ouvre la liste des pokémons avant de voir l'attaque "super glitch". Il provoque un fade-out de la musique, son remplacement par des effets sonores aléatoires, et l'ennemi est dit gelé, mais se prenant des dégats d'une brulure. La brulure fera permettre tout ses PV, la barre revenant même à zéro plusieurs fois. Il a se nom parce que "TM TRAINER" apparait dans le message glitché qu'on y trouve. 

L'attaque va aussi corrompre divers partie de la mémoire du jeu et ces différents effets peuvent faire planter le jeu. Si le nom du joueur est corrompu, sauvegarder peu provoquer la perte de la sauvegarde.

<iframe width="560" height="315" style="display:block;margin:auto;" src="https://www.youtube.com/embed/W5Hp8kPJTqg?si=Axy4R5CyzBMqMHfi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Crédits et sources

Une partie des informations sur ces pages proviennent des sites suivants :
- Liste des attaques glitch sur [Glitch City Wiki](https://glitchcity.wiki/wiki/AttackDex)
- Liste des attaques glitch sur [PRAMA Initiative]()
- Page Super Glitch sur [Glitch City Wiki](https://glitchcity.wiki/wiki/Super_Glitch_(Generation_I)) et [PRAMA Initiative](https://www.prama-initiative.com/index.php?page=corruption)
