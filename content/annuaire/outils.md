---
layout: layouts/base.njk
eleventyNavigation:
  key: Liste d'outils
  parent: Annuaire
  order: 1
---

# Liste d'outils

Voici une liste d'outils qui peuvent être utiles pour vous amuser avec les générations 1 et 2

## Outils sauvegardes

- [PKHeX](https://github.com/kwsch/PKHeX/blob/master/.github/README-fr.md) - Editeur universel de sauvegarde
- [Pokered Save Editor](https://github.com/junebug12851/pokered-save-editor) - Editeur pour Rouge/Bleu
- [Pikasav Save Editor](https://projectpokemon.org/home/files/file/1598-pikasav/) - Editeur de sauvegarde G1/G2
- [Convertisseur de Sauvegarde GSC](https://bluemoonfalls.com/pages/tools/gsc-save-converter) - Pour convertir les sauvegardes des formats virtual console vers jeux originels.

## Outils analyse

- [Générateur de Passcode de changement d'heure](https://bluemoonfalls.com/pages/tools/time-passcode-generator) - Pour pouvoir reset la console.
- [Tracker d'expérience de stats](https://bluemoonfalls.com/pages/tools/stat-exp-tracker) - Pour savoir l'expérience de statistique (équivalent des EV plus tard) des pokémons.
- [Outils de Shiny Hunting G1](https://bluemoonfalls.com/pages/tools/gen-1-shiny-tool) - Pour savoir si un pokémon de la G1 sera shiny.
- [Calculateur de Cadeau Mystère](https://bluemoonfalls.com/pages/tools/mystery-gift-calc)

## Outils autres

- [Ultimate Pokémon Randomizer](https://pokehacks.dabomstew.com/randomizer/index.php) - Randomizeur de sauvegarde universel, permettant de modifier beaucoup d'aspect de votre jeu avant une partie.
- [Outils d'échange G1/G2](https://bluemoonfalls.com/pages/tools/trade-sim) - Pour échanger des pokémons entre G1 et G2 (permet même d'obtenir des pokémons glitch !)

## Outils romhacking

- Les [décompilations](https://github.com/pret) - Permettent de plus facilement modifier les jeux
- [Polished Map](https://github.com/Rangi42/polished-map) - Éditeur de map pour les romhack basé sur les décompilations
- [Polished Map++](https://flathub.org/fr/apps/io.github.Rangi42.polished-map-plusplus) - Éditeur de map pour certains romhack basé sur des versions plus avancées des décompilations (avec un attributes.bin).
- **Wikis** : [pokered](https://github.com/pret/pokered/wiki), [pokeyellow](https://github.com/pret/pokeyellow/wiki), [pokecrystal](https://github.com/pret/pokecrystal/wiki)