---
layout: layouts/base.njk
eleventyNavigation:
  key: Liste de hackroms
  parent: Annuaire
  order: 1
---

# Liste de hackroms

Une liste de hackroms basés sur les versions GBC de Pokémon (générations 1 et 2), pour les gens voulant soit d'avoir des nouvelles expériences dans le style des premières générations, soit de revivre ces jeux avec des amélirations ou des modifications.

## Transformation totale

- [Pokemon Brown](https://www.romhacking.net/hacks/134/)
- [Pokemon Orange](https://github.com/PiaCarrot/pokeorange) - Note : c'est l'ancienne version de Pokémon Orange, la nouvelle n'étant pas encore vraiment prête.
- [Pokemon Bronze](https://www.pokecommunity.com/showthread.php?t=280588)
- [Pokemon Bronze 2](https://www.pokecommunity.com/showthread.php?t=370991) - La suite de Pokemon Bronze
- [Pokemon Prism](https://rainbowdevs.com/title/prism/)
- [Pokemon Black and White 3 : Genesis](https://www.pokecommunity.com/showthread.php?t=444114) - Une continuation de Pokémon Blanc/Noir, mais cette fois en version gameboy color.

## Recréation beta G2

- [Pokemon Super Gold 97](https://www.pokecommunity.com/showthread.php?t=430526) - Une recréation de la béta de Pokémon Or/Argent
- [Pokemon Gold and Silver 97: Reforged](https://www.pokecommunity.com/showthread.php?t=437360) - Une autre recréation de la béta de Pokémon Or/Argent, avec les amélioration gbc des générations suivantes

## Modifications

### Première génération

- [Pokemon Anniversary Red](https://rainbowdevs.com/title/red/)
- Shin Pokemon [Red](https://www.romhacking.net/hacks/8087/), [Blue](https://www.romhacking.net/hacks/8190/) and [Green](https://www.romhacking.net/hacks/8189/) - Des modifications "repaster-like", visant à rester proche de la première génération tout en rajoutant des amélioration.
- [Pokemon Red++](https://github.com/JustRegularLuna/rpp-backup) - Une modernisation de Pokémon Rouge, rajoutant des éléments des générations suivantes.
- [Kanto Expansion Pack](https://github.com/PlagueVonKarma/kep-hack) - Un pack d'extensions ajoutant des nouveaux pokémon, lieux et éléments à RGB tout en restant dans l'esprit Gen1

### Seconde génération

- [Pokemon Anniversary Crystal](https://rainbowdevs.com/title/crystal/) - Une version modifiée de Pokémon Crystal, avec différente modification, dont le fait de commencer à Kanto
- [Pokemon Crystal Legacy](https://www.romhacking.net/hacks/8134/) - Une amélioration de Pokémon Crystal, visant à les améliorer tout en restant propre au gameplay "vanilla" de la version (pas de split special/physique, etc)
- [Pokemon Polished Crystal](https://github.com/Rangi42/polishedcrystal) - Une modernisation de Pokémon Crystal utilisant des éléments des générations suivantes

### Autres jeux

- [Pokemon Pinball Generations](https://rainbowdevs.com/title/pinball/) - Une version or/argent de Pokémon Pinball

## Divers

- [Pokemon Black](https://github.com/Ax461/pokeblack) - Un hack basé sur la creepypasta avec le fantôme qui tue des gens.