---
layout: layouts/base.njk
eleventyNavigation:
  key: Annuaire
  order: 50
---

# Annuaire Pokémon

Le but de cet annuaire est de présenter différents sites pour en découvrir plus, mais aussi des listes de contenus qui peuvent intéresser les fans pour s'amuser et étendre leur expérience des premiers jeux pokémon.

## Pages disponibles

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a></li>
    {%- endif -%}
  {%- endfor -%}
</ul>